fn main() {
    if_expr(3);
    if_expr(7);
    ifelse_expr(6);
    iflet(true);

    loop_loop();
    while_loop();
    for_loop();
    for_countdown();
}

// if expressions
fn if_expr(number: i32) {
    if number < 5 {
        println!("condition was true!");
    } else {
        println!("condition was false!");
    }
}

fn ifelse_expr(number: i32) {
    if number % 4 == 0 {
        println!("number is divisible by 4");
    } else if number % 3 == 0 {
        println!("number is divisible by 3");
    } else if number % 2 == 0 {
        println!("number is divisible by 2");
    } else {
        println!("number is not divisible by 4, 3, or 2");
    }
}

fn iflet(condition: bool) {
    let number = if condition {
        5
    } else {
        6
    };
    println!("The value of number is: {}", number);
}

// loops
fn loop_loop() {
    let mut counter = 0;

    let result = loop {
        counter += 1;

        if counter == 10 {
            break counter * 2;
        }
    };

    println!("The result is {}", result);
}

fn while_loop() {
    let mut number = 3;

    while number != 0 {
        println!("{}!", number);

        number -= 1;
    }

    println!("LIFTOFF!!!");
}

fn for_loop() {
    let arr = [10, 20, 30, 40, 50];

    for element in arr.iter() {
        println!("the value is {}", element);
    }
}

fn for_countdown() {
    for number in (1..4).rev() {
        println!("{}!", number);
    }
    println!("LIFTOFF!!!");
}
