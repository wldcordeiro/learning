fn main() {
    // The trick is `mut` without it this fails as variables are created immutable by default.
    let mut x = 5;
    println!("The value of x is: {}", x);
    x = 6;
    println!("The value of x is: {}", x);

    // Shadowing...
    let y = 5;

    let y = y + 1;

    let y = y * 2;
    println!("The value of y is: {}", y);

    // Shadowing allows for "changing" the type of a variable
    let spaces = "   ";
    let spaces = spaces.len();
    println!("The spaces length is: {}", spaces);
}
