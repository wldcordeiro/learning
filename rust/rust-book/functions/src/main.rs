fn main() {
    another_function(5, 6);
    func_with_expressions();

    let x = five();
    println!("Here's the value of five() {}", x);

    let y = plus_one(x);
    println!("Here's the value of plus_one(x) {}", y);
}

// parameters are required to be typed
fn another_function(x: i32, y: i32) {
    println!("The value of x is {}", x);
    println!("The value of y is {}", y);
}

// function bodies, statements & expressions
fn func_with_expressions() {
    let val_from_expr = {
        let x = 3;
        x + 1
    };

    println!("The value of val_from_expr is {}", val_from_expr);
}

// functions with return values
fn five() -> i32 {
    5
}

fn plus_one(x: i32) -> i32 {
    x + 1
}
