fn main() {
    // Scalar Types

    // Integers

    // Integer types
    // length | signed | unsigned
    // 8-bit | i8 | u8
    // 16-bit | i16 | u16
    // 32-bit | i32 | u32
    // 64-bit | i64 | u64
    // 128-bit | i128 | u128
    // arch | isize | usize

    // Integer Literals
    // Number literal | example
    // Decimal | 98_222
    // Hex | 0xff
    // Octal | 0o77
    // Binary | 0b1111_0000
    // Byte (u8 only) | b'A'

    // Floating-Point Numbers
    let x = 2.0; // f64
    let y: f32 = 3.0; // f32
    println!("Floating points! f64 - {} f32 - {}", x, y);

    // Numeric operations

    let sum = 5 + 10;
    println!("Addition: 5 + 10 = {}", sum);

    let difference = 95.5 - 4.3;
    println!("Subtraction: 95.5 - 4.3 = {}", difference);

    let product = 4 * 30;
    println!("Multiplication: 4 * 30 = {}", product);

    let quotient = 56.7 / 32.2;
    println!("Division: 56.7 / 32.2 = {}", quotient);

    let remainder = 43 % 5;
    println!("Remainder: 43 % 5 = {}", remainder);

    // Booleans
    let t = true;
    let f: bool = false; // explicit annotation
    println!("{} {}", t, f);

    // Characters
    let c = 'z';
    let z = 'ℤ';
    let heart_eyed_cat = '😻';
    println!("{}, {}, {}", c, z, heart_eyed_cat);

    // Compound Types

    // Tuples
    let tup = (500, 6.4, 1);
    // tuple unpack
    let (x, y, _) = tup;
    let one = tup.2;
    println!("whole tuple {:?}", tup);
    println!("tuple value x {}", x);
    println!("tuple value y {}", y);
    println!("tuple value z {}", one);

    // Arrays
    let arr = [1, 2, 3, 4, 5];
    println!("whole array {:?}", arr);

    let months = [
        "January",
        "February",
        "March",
        "April",
        "May",
        "June",
        "July",
        "August",
        "September",
        "October",
        "November",
        "December",
    ];
    println!("month array {:?}", months);
    println!("First month {}", months[0]);
    println!("Second month {}", months[1]);
    // Out of range index access causes a panic runtime error
    // println!("Thirtheenth month {}", months[12]);
}
