# Reactive Programming with RxJS Chapter 01: The Reactive Way - Notes

## The Reactive Way

The real world is messy, few applications are fully synchronous and writing asynchronous code is necessary for responsive applications. Usually this is painful, but it doesn't need to be. Modern development fast responses and the ability to process various sources of data without issue. Current methods are weak but they don't need to be, here you'll learn about reactive programming a better way to think of asynchronous code. You'll learn how streams of events (Observables) are great ways to work with async code. We'll create an Observable and see how RxJS and reactive thinking improve our techniques and make you more productive and happy.

### What's Reactive

Let's start with a simple example. Don't worry about understanding it yet. The program retrieves data from different sources upon a button click with these three requirements:

* unify data from two locations using different JSON structures
* no duplicates in the final result
* The user should be throttled so the button can't be clicked more than once a second.

It'd look like this with RxJS

```javascript
var button = document.getElementById('retrieveDataBtn');
var source1 = Rx.DOM.getJSON('/resource1').pluck('name');
var source2 = Rx.DOM.getJSON('/resource2').pluck('props', 'name');

function getResults(amount) {
    return source1.merge(source2)
        .pluck('names')
        .flatMap(function(array) { return Rx.Observable.from(array); })
        .distinct()
        .take(amount);
}

var clicks = Rx.Observable.fromEvent(button, 'click');
clicks.debounce(1000)
    .flatMap(getResults(5))
    .subscribe(
        function(value) { console.log('Received Value:', value); },
        function(err) { console.error(err); },
        function() { console.log('All values retrieved!'); }
    );
```

Don't worry about understanding it all. High level, let's go over this. First thing to note, more expressed in less code thanks to Observables.

Observables represent streams of data, and programs can be expressed largely as streams of data. In the example, both remote sources are Observables and so are the mouse clicks from the user. The entire program is practically an Observable made from a transformed click event.

Reactive programming is expressive. Imagine the pain in the ass all this would be using traditional callbacks or Promises. With this reactive approach we can ignore how the specifics happen and worry about *what* we want our code to do.

Next we cover how to make our programs more expressive and efficient using reactive programming!

### Spreadsheets are Reactive

The quintessential example of a reactive system is a spreadsheet. They're shockingly intuitive. If we have a value in the cell `A1` we can then reference it from other cells in the spreadsheet and whenever `A1` changes, all depending cells update.

This behavior feels natural, no human intervention needed for how to update, the cells *reacted* to the changes. In spreadsheets we declare our problems and don't worry about how the compute calculates the values.

This is the goal of reactive programming, declare relationships between players and the program evolves as entities change or create new values.

### The Mouse as a Stream of Values

Let's use the program from before as an example of streams. We were using mouse clicks as an infinite sequence of real time user generated events. In reactive programming we seem this as a stream of events that can be queried and manipulated. Thinking of streams instead of individual values opens up new ways to program where entire sequences can be manipulated before creation.

This is different from what we're used to, having values stored somewhere like a database or array and waiting for them to be available before using them. We can think of our event streams as as an array separated by *time* instead of memory. Seeing programs as flowing sequences of data is key to understanding RxJS programming. There is a learning curve but we'll get over that.

### Querying the Sequence

Let's implement the simple mouse stream program using traditional event listeners.

***EXAMPLE: THINKING IN SEQUENCES 01***

This code outputs the x and y coordinates like so

```
252 183
124 233
153 384
```

Looks like a sequence huh? Problem is manipulating the event isn't as easy as manipulating arrays. For example say we wanted to change the code to only log the first ten events on the right side of the screen.

```js
var clicks = 0;
document.body.addEventListener('click', function registerClicks(e) {
    if (clicks < 10) {
        if (e.clientX > window.innerWidth / 2) {
            console.log(e.clientX, e.clientY);
        }
    } else {
        document.body.removeEventListener('click', registerClicks);
    }
})
```

To do this though we had to introduce external state through the `clicks` var, and check for two different conditions in nested conditional blocks. Plus we also tidy up and unregister the listener in the else case.

For such a simple goal we got some complicated code. Plus it's prone to bugs due to the state we introduced. All we want here is to "query" the database of clicks, if this were SQL:

```sql
SELECT x, y FROM clicks LIMIT 10;
```

What if we could treat the click stream as a transformable, queriable, data source? It's no different from a database, but only emits real time values. We just need a data type abstraction.

Enter RxJS's Observable type.

```javascript
Rx.Observable.fromEvent(document, 'click')
    .filter(function(c) { return c.clientX > window.innerWidth / 2; })
    .take(10)
    .subscribe(function(c) { console.log(c.clientX, c.clientY); });
```

This does the same as the code before. It reads like this:

> Create an Observable of click events and filter out the clicks that happen on the left side of the screen. Then print the coordinates of only the first 10 clicks to the console as they happen.

Notice how easy to read the code is even when unfamiliar with it. It's also free of external state, making this safer, less buggy, self-contained code. We created the Observable from the DOM event. It provides us with a sequence of events we can manipulate. This gives us tons of power. We can merge, transform and pass around Observables easily. We've turned events that were hard to work with into simple, flexible, array-like data structures.

## Of Observers and Iterators

To understand Observables we need to look at their foundations the Observer and Iterator software patterns.

### The Observer Pattern

In the Observer pattern we have a Producer object that keeps an internal list of Listeners subscribed to it. Listeners are notified by calling `update()` on them whenever Producer state changes.

A simple example of the pattern can be implemented:

***EXAMPLE: OBSERVER PATTERN***

The Producer object keeps a dynamic list of listeners in the instance's `listeners` array that's updated whenever the Producer calls its `notify` method.

The output of the example looks like this:

```
Listener 1 Received: Hello there!
Listener 2 Received: Hello there!
```

The listeners are notified whenever the Producer's internal state updates, without having to check.

### The Iterator Pattern

An Iterator is an object that providers consumers an easy way to traverse its contents, hiding the implementation details. The interface is simple, only two methods are needed, `next()` to get the next item and `hasNext()` to find out if there is a next item.

A simple example of an iterator that operates on arrays of numbers yielding only elements that are multiples of the divisor parameter.

***EXAMPLE: ITERATOR PATTERN***

Iterators are a great way to encapsulate traversing logic for any data structure. Iterators get interesting though when they're made generic to handle different data types or when they're configurable at runtime like our example with the `divisor` parameter.

## The Rx Pattern and the Observable

The Observer and Iterator patterns are both powerful, but the combination is even more powerful. We call this the Rx pattern, named after the Reactive Extensions libraries.

Observables are central to the Rx pattern, they emit their values in order like an iterator, but instead of its consumers requesting values, the Observable pushes values as they become available. Similar in a way to the Producer in the Observer pattern. Observables are sequences whose items become available over time. The consumer of Observables is an Oberserver and the equivalent of a listener in the Observer pattern. When the Oberserver is subscribed to an Observable it receives the values in the sequence as they become available.

The two key differences between the Rx pattern and the Observer pattern are:

* An Observable doesn't start streaming items until it has at least on subscriber.
* Like iterators an Observable can signal a sequence end.

We can declare how to react to a sequence of elements emitted by an Observable, instead of dealing with single items. We can apply various transformations to the entire sequence in an efficient way.

## Creating Observables

You can create Observables in several ways, the `create` method is the most rudimentary. For example, here's how we create a simple Observable.

```javascript
var observable = Rx.Observable.create(function(observer) {
    observer.onNext('Simon');
    observer.onNext('Jen');
    observer.onNext('Sergi');
    observer.onCompleted();
});
```

When this Observable is subscribed to it'll emit three values by calling the `onNext` method on its listeners and `onCompleted` when it's done. Now to cover how to subscribe to an Observable with an Observer.

### First Contact with Observers

Observers listen to Observables, when events happen in Observables they call the related methods on their Observers. There are three methods on Observers:

* `onNext` - like `update` in the Observer pattern, called when a new value is emitted from the Observable.
* `onCompleted` - Signals that there's no more data available, after this is called, calling `onNext` will have no effect.
* `onError` - called when an error occurs in an Observable. After it's called, calling `onNext` will have no effect.

How to create an Oberserver:

```javascript
var observer = Rx.Observer.create(
    function onNext(x) { console.log('Next: ', x); },
    function onError(err) { console.log('Error: ', err); },
    function onCompleted() { console.log('Completed!'); }
);
```

The `create` method in the `Rx.Observer` takes three function parameters, one for onNext, onCompleted, and onError and gives you an Observer instance. The functions are optional and you can choose to send only the ones you use. Like say you have an endless stream like button clicks, no need for an `onCompleted` there or say you know it won't error, drop the `onError`.

### Making Ajax Calls with an Observable

Let's do something interesting, create an Observable that retrieves remote content. This will wrap the `XMLHttpRequest` object with `Rx.Observable.create()`:

```javascript
function get(url) {
    return Rx.Observable.create(function(observer) {
        // Make Ajax request
        var req = new XMLHttpRequest();
        req.open('GET', url);

        req.onload = function() {
            if (req.status === 200) {
                observer.onNext(req.response);
                observer.onCompleted();
            } else {
                observer.onError(new Error(req.statusText));
            }
        };

        req.onerror = function() {
            observer.onError(new Error('Unknown error!'));
        };

        req.send();
    });
}

// Create an Ajax Observable
var test = get('/api/contents.json');
```

Here the `get()` function uses `create()` to wrap `XMLHttpRequest`. If it's successful we emit the contents and end the sequence. Otherwise if there's an error we emit it with `onError()`

We call the function with a URL, this creates the Observable, but it has no subscriptions yet, so it won't make any requests. Observables don't do anything until there's at least one subscriber, let's add one:

```javascript
test.subscribe(
    function onNext(x) { console.log('Result: ', x); },
    function onError(err) { console.log('Error: ', err); },
    function onCompleted() { console.log('Completed!'); }
);
```

Now we're not making an Observer explicitly like before, we'll use this shorthand most of the time with the 3 argument functions. `subscribe()` sets off everything, before it's called nothing happens.

### There is (Almost) Always an Operator

In RxJS methods that transform or query sequences are operators. There are both object and instance methods on Observables, `create()` is one such operator.

`create()` is a good idea when we have a very specific Observable type in mind, otherwise there's many operators you can use to create Observables from common sources.

In the RxJS DOM library there are several HTTP helper operators. For Example:

```javascript
Rx.DOM.get('/api/contents.json').subscribe(
    function onNext(data) { console.log(data.response); },
    function onError(err) { console.log(err); },
);
```

This little snippet does the same work as the previous. Only it's been vetted to work by many others so we know it will be more robust. Notice that we didn't pass `onCompleted` that's because we didn't need to do anything on completion.

We'll use lots of built in operators. Think of them as the RxJS standard library. Let's go over some common operators for creating observables.

#### One Data Type to Rule Them All

In RxJS programs we want to deal with just Observables, not just asynchronous data. This makes it a easier to combine data from different origins, like existing arrays with callbacks.

### Creating Observables from Arrays

We can make any iterable object into an Observable by using the `from` operator. It takes an iterable as an argument and returns an Observable that emits each of its elements.

```javascript
Rx.Observable
    .from(['Adria', 'Jen', 'Sergi'])
    .subscribe(
        function(x) { console.log(`Next: ${x}`); },
        function(err) { console.log(`Error: ${err}`); },
        function() { console.log('Completed!'); },
    );
```

`from` along with `fromEvent` are some of the most convenient and frequently used operators in RxJS.

### Creating Observables from JavaScript Events

When we convert events into Observables, it becomes a first class value that you can pass around. Here's an example Observable that emits the mouse pointer coordinates whenever it moves.

```javascript
var allMoves = Rx.Observable.fromEvent(document, 'mousemove');

allMoves.subscribe(function(e) {
    console.log(e.clientX, e.clientY);
});
```

Changing the event into an Observable gives you a ton of freedom, but most importantly you can now build other Observables from the original that can be used independently.

```javascript
var movesOnTheRight = allMoves
    .filter(function(e) {
        return e.clientX > window.innerWidth / 2;
    });

var movesOnTheLeft = allMoves
    .filter(function(e) {
        return e.clientX < window.innerWidth / 2;
    });

movesOnTheRight.subscribe(function(e) {
    console.log('Mouse is on the right:', e.clientX);
});

movesOnTheLeft.subscribe(function(e) {
    console.log('Mouse is on the left:', e.clientX);
});
```

So we made two Observables from the preceding `allMoves` Observable. These two only contain filtered items from the original Observable. One the movements on the right and the other the left. Neither of these modify the original, it can still be subscribed to and will emit events, Observables are immutable and all operations create new Observables.

### Creating Observables from Callback Functions

We can transform callbacks into Observables with `fromCallback` or `fromNodeCallback`. Node.js follows a convention of always invoking the callback with an error argument first to signal there was an error.

```javascript
var Rx = require('rx');
var fs = require('fs');

// Create Observable from `readdir`
var readdir = Rx.Observable.fromNodeCallback(fs.readdir);

var source = readdir('/home/wldcordeiro');

var subscription = source.subscribe(
    function(res) { console.log('List of directories: ' + res); },
    function(err) { console.log('Error: ' + err); },
    function() { console.log('Done!'); }
);
```

We made an Observable from `fs.readdir` and then use it with the same arguments the original would take, save for the callback. Giving us a proper Observable that we can use as expected.

## Wrapping Up

We covered reactive programming with RxJS and how it can solve common problems with callbacks and other methods with Observables.
