'use strict';
// From Callback
var Rx = require('rx');
var fs = require('fs');

// Create Observable from `readdir`
var readdir = Rx.Observable.fromNodeCallback(fs.readdir);

var source = readdir('/home/wldcordeiro');

var subscription = source.subscribe(
    function(res) { console.log('List of directories: ' + res); },
    function(err) { console.log('Error: ' + err); },
    function() { console.log('Done!'); }
);
