'use strict';

// THINKING IN SEQUENCES 01

// document.body.addEventListener('mousemove', function(e) {
//   console.log(e.clientX, e.clientY);
// });


// OBSERVER PATTERN
function Producer() {
  this.listeners = [];
}

Producer.prototype.add = function(listener) {
  this.listeners.push(listener);
};

Producer.prototype.remove = function(listener) {
  var index = this.listeners.indexOf(listener);
  this.listeners.splice(index, 1);
};

Producer.prototype.notify = function(message) {
  this.listeners.forEach(function(listener) {
    listener.update(message);
  });
};

var listener1 = {
  update(message) {
    console.log('Listener 1 Received:', message);
  }
};

var listener2 = {
  update(message) {
    console.log('Listener 2 Received:', message);
  }
};

var notifier = new Producer();

notifier.add(listener1);
notifier.add(listener2);
notifier.notify('Hello there');

// ITERATOR PATTERN

function iterateOnMultiples(arr, divisor) {
  this.cursor = 0;
  this.array = arr;
  this.divisor = divisor || 1;
}

iterateOnMultiples.prototype.next = function() {
  while (this.cursor < this.array.length) {
    var value = this.array[this.cursor++];
    if (value % this.divisor === 0) {
      return value;
    }
  }
}

iterateOnMultiples.prototype.hasNext = function() {
  var cur = this.cursor;
  while (cur < this.array.length) {
    if (this.array[cur++] % this.divisor === 0) {
      return true;
    }
  }

  return false;
}

var consumer = new iterateOnMultiples([1, 2, 3, 4, 5, 6, 7, 8, 9, 10], 3);

console.log(consumer.next(), consumer.hasNext());
console.log(consumer.next(), consumer.hasNext());
console.log(consumer.next(), consumer.hasNext());


// Example Observables


// From array

Rx.Observable
    .from(['Adria', 'Jen', 'Sergi'])
    .subscribe(
        function(x) { console.log(`Next: ${x}`); },
        function(err) { console.log(`Error: ${err}`); },
        function() { console.log('Completed!'); }
    );

// From Event

var allMoves = Rx.Observable.fromEvent(document, 'mousemove');

var movesOnTheRight = allMoves
    .filter(function(e) {
        return e.clientX > window.innerWidth / 2;
    });

var movesOnTheLeft = allMoves
    .filter(function(e) {
        return e.clientX < window.innerWidth / 2;
    });

movesOnTheRight.subscribe(function(e) {
    console.log('Mouse is on the right:', e.clientX);
});

movesOnTheLeft.subscribe(function(e) {
    console.log('Mouse is on the left:', e.clientX);
});

// From Callback - see `node_example.js`
