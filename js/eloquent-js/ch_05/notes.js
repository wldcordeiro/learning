"use strict";
/// Eloquent Javascript Chapter 05: Higher Order Functions - Notes

// Large programs are costly, they introduce complexity and confuse programmers.
// This can lead to bugs in the code and the larger the program the more places
// bugs can hide. Let's go back to the example from the last chapter.
// We have this version.
var total = 0, count = 1;

while (count <= 10) {
  total += count;
  count++;
}
console.log(total);
// Or this second version that relies on two functions.
function range(start, stop, step) {
  let arr = [];
  if (step == undefined) {
    step = 1;
  }

  for (let i = start; step > 0 ? i <= stop : i >= stop; i += step) {
    arr.push(i);
  }

  return arr;
}

function sum(arr) {
  let sum = 0;
  for (let i = 0; i < arr.length; i++) {
    sum += arr[i];
  }

  return sum;
}

console.log(sum(range(1, 10)));
// Which is less prone to bugs? If we count the size of the functions, the second
// is larger, however it's likely to be more correct. This is because the solution
// is expressed in a vocabulary that maps to the problem being solved.
// Summing a range of numbers isn't about loops and counters, it's about ranges
// and sums. The definition of this vocabulary could have loops and counters, but
// because they're expressing simpler concepts it's easier to get it right.

/// Abstraction

// In programming terms, these vocabularies are called abstractions. They hide
// details and allow us to talk about problems at a higher (more abstract) level.
// Here's a cooking analogy:

// Compare this recipe

// Put 1 cup of dried peas per person into a container. Add water until the peas
// are well covered. Leave the peas in water for at least 12 hours. Take the peas
// out of the water and put them in a cooking pan. Add 4 cups of water per person.
// Cover the pan and keep the peas simmering for two hours. Take half an onion
// per person. Cut it into pieces with a knife. Add it to the peas. Take a stalk
// of celery per person. Cut it into pieces with a knife. Add it to the peas.
// Take a carrot per person. Cut it into pieces. With a knife! Add it to the peas.
// Cook for 10 more minutes.

// To this recipe

// Per person: 1 cup dried split peas, half a chopped onion, a stalk of celery,
// and a carrot.

// Soak peas for 12 hours. Simmer for 2 hours in 4 cups of water (per person).
// Chop and add vegetables. Cook for 10 more minutes.

// The second is shorter, and easier to interpret but you'll need more knowledge
// of cooking vocabulary. In programming we can't rely on the vocabulary having all
// "words" we need so we can have a tendency to fall into the first pattern. You
// need to make it second nature to notice when a concept needs to be abstracted
// into a new "word".

/// Abstracting Array Traversal

// Plain functions can be a good way to build abstractions, but they also have
// shortcomings. You've seen for loops like:
var array = [1, 2, 3];

for (let i = 0; i < array.length; i++) {
  let current = array[i];
  console.log(current);
}
// This is trying to say "for each element in an array, log it to the console",
// but it does it in a roundabout way with counters, checking the array length and
// a variable declaration. This provides lots of potential for error, let's try
// abstracting this function. It's easy to write a function that loops over an
// array and logs each element.
function logEach(array) {
  for(let i = 0; i < array.length; i++) {
    console.log(array[i]);
  }
}
// But what if you wanted to do something other than log the element? We could
// pass the "something" (a function) as a value called action.
function forEach(array, action) {
  for(let i = 0; i < array.length; i++) {
    action(array[i]);
  }
}
forEach(['Wampeter', 'Foma', 'Granfallon'], console.log);
// Often you don't pass a predefined function to forEach but instead use a function
// expression.
var numbers = [1, 2, 3, 4, 5], sum = 0;
forEach(numbers, function(number) {
  sum += number;
});
console.log(sum);
// This looks a lot like a normal for-loop, with the body as a block below it.
// However now the body is within the function and the parentheses of forEach so
// you need to terminate with `});` Using this pattern you can specify a variable
// name for the current element rather than picking out of the array manually.

// But we don't need to write forEach ourselves, it's a standard method on arrays.
// Since the array is already provided, the only argument needed is the function
// to be executed on the elements.

// To illustrate this, lets use the gatherCorrelations function from the last chapter.
// it has two array traversing for-loops.
// function gatherCorrelations(journal) {
//   var phis = {};
//   for (var entry = 0; entry < journal.length; entry++) {
//     var events = journal[entry].events;
//     for (var i = 0; i < events.length; i++) {
//       var event = events[i];
//       if (!(event in phis))
//         phis[event] = phi(tableFor(event, journal));
//     }
//   }
//   return phis;
// }

// Reworked with forEach, it's a bit shorter and cleaner.
// function gatherCorrelations(journal) {
//   let phis = {};

//   journal.forEach(function(entry) {
//     entry.events.forEach(function(event) {
//       if (!event in phis) {
//         phis[event] = phi(tableFor(event, journal));
//       }
//     });
//   });
// }

/// Higher Order Functions

// Functions that operate on other functions, either by taking them as arguments
// or by returning them are called "higher order functions". Higher order functions
// allow us to abstract over actions, not just values. They can come in serveral
// forms. Here's some examples.

// You can have functions that create new functions
function greaterThan(n) {
  return function(m) { return m > n; }
}

var greaterThanTen = greaterThan(10);
console.log(greaterThanTen(11));

// or functions that change other functions
function noisy(f) {
  return function(arg) {
    console.log(`Calling with: ${arg}`);
    let val = f(arg);
    console.log(`Called with: ${arg} - got ${val}`);
    return val;
  }
}
noisy(Boolean)(0);

// Or functions that provide new types of control flow.
function unless(test, then) {
  if (!test) {
    then();
  }
}

function repeat(times, body) {
  for (let i = 0; i < times; i++) {
    body(i);
  }
}

repeat(3, function(n) {
  unless(n % 2, function() {
    console.log(`${n} is even`);
  });
});

// The lexical scoping rules work to our advantage when using functions this way.
// In the previous example, the `n` parameter is local to the outer function, but
// because inner functions have access to the scope of enclosing functions it can
// use `n`, but note, variables in inner functions can't be accessed by outer
// functions.

/// Passing Along Arguments

// The noisy function from earlier has a deficit, it can only take one argument.
// We could add more arguments but how would we know we've added enough? For these
// situations, JavaScript has the `apply` method you pass it an array (or other
// array like object) of arguments and it calls the function with those arguments.
function transparentWrapping(f) {
  return function() {
    return f.apply(null, arguments);
  };
}
// This example is useless, but it shows the pattern, the returned function passes
// all arguments to f, it does this by passing the arguments object to apply. The
// first argument to apply can be used to simulate a method call and we pass `null`

/// JSON

// Higher order functions that apply functions to array elements are used often
// in JavaScript. The forEach method is the most primitive of these. To get started
// lets play with a data set. The data set is a collection of the author's ancestors.
// It's in the JSON format (JavaScript Object Notation) a widely used data-storage
// format. It's similar to how you write objects and arrays but with some limitations.
// All property names must be wrapped in double quotes.
// Simple data expressions only, no functions, variables or computation
// No comments allowed.

// In JavaScript we have the `JSON.stringify` and `JSON.parse` methods that convert
// data to and from the JSON format, the first takes a JavaScript value and returns
// a JSON string. The second takes a JSON string and converts it to a JavaScript value.
var json_string = JSON.stringify({name: "X", born: 1980});
console.log(json_string);
console.log(JSON.parse(json_string).born);

// Adding the ancestry data to use in the chapter.
var ancestry = JSON.parse(require('./ancestry.js'));
console.log(ancestry.length);

/// Filtering An Array

// To find people in the ancestry who were young in 1924, you could make a filter
// function like so that filters out elements that don't pass a test.
function filter(array, test) {
  let passed = [];

  for (let i = 0; i < array.length; i++) {
    if (test(array[i])) {
      passed.push(array[i]);
    }
  }

  return passed;
}

console.log(filter(ancestry, function(person) {
  return person.born > 1900 && person.born < 1925;
}));
// This uses the argument named test which is a function value that's called for
// each element and its return value determines whether the element is included
// in the returned array. Note that this creates a new array rather than modify
// the existing array, it's a pure function. Like forEach though, filter is built
// into JavaScript arrays. For example:
console.log(ancestry.filter(function(person) {
  return person.father == 'Carel Haverbeke';
}));

/// Transforming With Map

// Say we want to take our array of objects representing people and turn it into
// an array of names, the map method is our savior. It transforms an array by applying
// a function to all of its elements and returns a new array, the new array will
// be the same length of the input but it's contents will have been "mapped" by
// the function.
function map(array, transform) {
  let mapped = [];

  for (let i = 0; i < array.length; i++) {
    mapped.push(transform(array[i]));
  }

  return mapped;
}

var overNinety = ancestry.filter(function(person) {
  return person.died - person.born > 90;
});

console.log(map(overNinety, function(person) {
  return person.name;
}));
// Like forEach and filter, this is also a standard array method.

/// Summarizing With Reduce

// A common pattern of array computation is getting a single value from them.
// A simple example is summing the values of an array of numbers, or finding the
// lowest birth year in the ancestry set. This operation is called reduce (or fold)
// think of it as folding up an array, one element at a time. When summing numbers,
// you start at zero and combine by adding the two.

// The reduce function takes an extra argument aside from the array and the combining
// function, it takes a start value, this is a little less straightforward than
// filter or map. For example:
function reduce(array, combine, start) {
  let current = start;

  for (let i = 0; i < array.length; i++) {
    current = combine(current, array[i]);
  }

  return current;
}

console.log(reduce([1, 2, 3, 4], function(a, b) {
  return a + b;
}, 0));

// The standard array method reduce has an added convenience, if the array has at
// least one value, the start argument can be left off and the first element will
// be used. For example:
console.log(ancestry.reduce(function(min, cur) {
  if (cur.born < min.born)
    return cur;
  else return min;
}));

/// Composability

// If you consider how you would have written the last example without higher-order
// functions, it wouldn't be much different.
// var min = ancestry[0];
// for (let i = 1; i < ancestry.length; i++) {
//   let cur = ancestry[i];
//   if (cur.born < min.born)
//     min = cur;
// }
// console.log(min);
// The higher-order functions really shine when you need to compose functions.
// For example here's code that finds the average age for men and women in the
// ancestry
function average(array) {
  function plus(a, b) {
    return a + b;
  }

  return array.reduce(plus) / array.length;
}

function age(p) { return p.died - p.born; }
function male(p) { return p.sex == 'm'; }
function female(p) { return p.sex == 'f'; }

console.log(average(ancestry.filter(male).map(age)));
console.log(average(ancestry.filter(female).map(age)));
// Instead of having a big loop we've neatly composed the concepts we're interested
// in, applying each one by one to get the value we're interested in. This is
// great for writing clear code but comes with a cost.

/// The Cost

// Unfortunately while higher-order functions make code more elegant, they're also
// more inefficient. A program that processes arrays is most elegantly expressed
// as a series of separated steps that each do something to the array and return
// a new array, but building those steps may be a tad expensive.

// Passing a function to forEach and letting it handle array iteration is convenient
// and easy to read, but function calls in JavaScript are less efficient than loops.
// This is true of many techniques that improve code clarity. Abstractions add layers
// between the computer's actions and the concepts we're working with, making the
// machine do more work. This isn't 100% true, there are languages that have better
// support for abstractions, and even experienced JavaScript developers can find
// ways to write efficient abstract code.

// However, computers have gotten insanely fast. If the data you're processing isn't
// massive or the timeframe of what you're doing is on a human scale then it won't
// matter if you wrote an elegant solution or efficient solution.

// It's helpful to keep a rough idea of how often a piece of code will run. A loop
// inside a loop the code in the innner loop will run M*N times, with N being the
// number of times the outer loop runs and M being the numbers of times the inner
// loop repeats per outer loop iteration. If you have another nested loop it's now
// M*N*P and so on for each additional nested loop. This can grow exponentially
// and when a program is slow its wise to look for loop nesting.

/// Great-Great-Great-Great...

// Finding the percentage of DNA the author shares with his oldest ancestor in the
// dataset. Starting off with a helper object that has people's names as keys.
var byName = {};

ancestry.forEach(function(person){
  byName[person.name] = person;
});

console.log(byName['Philibert Haverbeke']);
// We can't simply count the fathers from Philbert to Pauwels, there's several
// cases where people married their second cousin, meaning the tree rejoins in
// certain places, meaning the author shares more than 1/2^G of their genes with
// this person where G is the number of generations between Pauwels and the author.
// The formula comes from the idea that each generation splits the gene pool in two.

// A way to think of the problem is analogous to reduce, which condenses an array
// to a single value by repeatedly combining values left to right. In this case
// we want to reduce the data structure in a way that follows the family lines to
// a single value, the shape of the data is a family tree, not a flat list.

// The way to reduce this shape is to compute a value from a single person by
// combining values for their ancestors, this can be done recursively: for person
// A we need to compute the values for their parents, and so on. In principle this
// would be infinite but the data set is finite. We'll have a default value for people
// not in the dataset, a value of 0, assuming that if they're not in the list
// they don't share DNA. Given a person, a function to combine values from the parents
// and a default value, reduceAncestors condenses a value from a family tree.
function reduceAncestors(person, f, defaultValue) {
  function valueFor(person) {
    if (person == null)
      return defaultValue;
    else
      return f(person, valueFor(byName[person.mother]),
                       valueFor(byName[person.father]));
  }

  return valueFor(person);
}
// valueFor handles a single person, and with recursion it calls itself to handle
// the parents of the person, the result and the person are passed to f which returns
// the value for the person. We can use this to compute the amount of DNA the author's
// grandfather shared with Pauwels van Haverbeke and divide that by 4 to get the
// author's percentage.
function sharedDNA(person, fromMother, fromFather) {
  if (person.name == 'Pauwels van Haverbeke')
    return 1;
  else
    return (fromMother + fromFather) / 2;
}

var ph = byName['Philibert Haverbeke'];
console.log(reduceAncestors(ph, sharedDNA, 0) / 4);
// Pauwels Haverbeke, obviously shared 100% of his DNA with himself, so we return
// 1 for him. Everyone else shares the average of the amount their parents share.
// The author shares roughtly 0.05% of his DNA with Pauwels

// We could have computed this without relying on reduceAncestors, but separating
// the general approach from the specific case improves code clarity and allows us
// to reuse the abstract part of the code for other uses. The next example finds
// the percentage of a person's known ancestors who lived past 70 (by lineage, so
// people maybe counted multiple times)
function countAncestors(person, test) {
  function combine(current, fromMother, fromFather) {
    let thisOneCounts = current != person && test(current);
    return fromMother + fromFather + (thisOneCounts ? 1 : 0);
  }

  return reduceAncestors(person, combine, 0);
}

function longLivingPercentage(person) {
  let all = countAncestors(person, function(person) {
    return true;
  });

  let longLiving = countAncestors(person, function(person) {
    return (person.died - person.born) >= 70;
  });

  return longLiving / all;
}

console.log(longLivingPercentage(byName['Emile Haverbeke']));
// These numbers shouldn't be taken too seriously though because the data set is
// rather arbitrary but the code shows that reduceAncestors gives us a useful piece
// of vocabulary for working with family tree structures.

/// Binding

// The bind method which all functions have will create a new function that calls
// the original with some arguments already set.

// Here's an example of bind in use. We define a function isInSet that tells us if
// a person is in a given set of strings. To call filter in order to collect the
// people data we either write a function expression that calls isInSet or partially
// apply the isInSet function.

var theSet = ['Carel Haverbeke', 'Maria van Brussel', 'Donald Duck'];

function isInSet(set, person) {
  return set.indexOf(person.name) > -1;
}

console.log(ancestry.filter(function(person) {
  return isInSet(theSet, person);
}));

console.log(ancestry.filter(isInSet.bind(null, theSet)));
// The call to bind returns a function that calls isInSet with theSet as the first
// argument, followed by any arguments given to the bound function. The first argument
// where we pass null is used for method calls similar to the first argument for
// apply, this will be covered in the next chapter.

/// Summary

// The ability to pass function values to other functions is a deeply useful aspect
// of JavaScript. It allows us to write computations with "gaps" in them that we
// can then provide functions to fill the gaps with the missing computation.

// Arrays provide useful methods, forEach, filter, map, and reduce.
// Functions also have an apply and bind method which be used to either call the
// function with an array of arguments or return a partially applied function.
