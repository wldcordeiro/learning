"use strict";
// Eloquent Javascript Chapter 11: Project -- A Programming Language - Notes

// Building a programming language is surprisingly easy and enlightening.
// There's no magic involved in building a programming language, we're building
// a language called Egg, it's a tiny, simple language but powerful enough to express
// any computation you can think of with simple function based abstractions.

/// Parsing

// The most immediately noticed aspect of a lanuage is its syntax. A parser is a program
// that reads a piece of text and produces a data structure that reflects the structure
// of the program contained in the text. If the text isn't a valid program, the parser
// should errror out.

// Our language will have a simple, uniform syntax. Everything is an expression in Egg.
// An expression can be a variable, number, string, or application. Applications are
// used for function calls and constructs like if and while.

// For simplicity strings in Egg don't support escapes. A string is simply a sequence
// of characters that aren't double quotes wrapped in double quotes. Numbers are
// sequences of digits. Variable names are anything except whitespace and reserved
// names.

// Applications are written like they are in JavaScript, by putting parantheses after
// an expression, having any number of arguments in those parantheses, comma separated.

// do(define(x, 10),
//   if(>(x, 5),
//     print("large"),
//     print("small")))

// Due to Egg's uniformity, operators like ">" are normal variables and applied like
// other functions. The syntax has no concept of a block so we need a `do` construct
// to represent things in sequence.

// The data structure the parser uses to describe a program will consist of expression
// objects with a type property and others to describe its content.

// "value" expressions represent literal strings or numbers. Their value property
// contains the string or number value they represent. Expressions of type "word"
// are used for identifiers, they have a name property that holds the identifier
// name as a string. Then there's "apply" expressions that represent applications.
// They have an operator property that refers to the expression being applied, and
// an args property containing their argument expressions as an array.

// ">(x, 5)" would be represented like.
{
  let exSynTree = {
    type: 'apply',
    operator: {type: 'word', name: '>'},
    args: [
      {type: 'word', name: 'x'},
      {type: 'value', value: 5}
    ]
  };

  console.log(`${exSynTree.operator.name}(${exSynTree.args[0].name}, ${exSynTree.args[1].value})`);
}
// Data structures like this are called syntax trees. Imagine objects as dots and
// the links between them are lines. It has a tree-like shape. The fact that expressions
// can nest is similar to how branches split.

// * do
// |
// |- * define
// |  |
// |  |- * x
// |  |- * 10
// |
// |- * if
//    |
//    |- * >
//    |  |
//    |  |- * x
//    |  |- * 5
//    |- * print
//    |  |
//    |  |- * "large"
//    |- * print
//    |  |
//    |  |- * "small"

// Compared to the simple parser from chapter 9 we need another approach because
// we're dealing with recursive structures. We can solve this by writing a recursive
// parsing function.

// The function parseExpression(..) takes a string for input and returns an object
// containing the data structure for the expression at the start of the string,
// along with the leftover string. Then to parse subexpressions the function calls
// itself, yielding the argument expression and the leftover text. This can continue
// or maybe the terminating paranthesis.
function parseExpression(program) {
  program = skipSpace(program);
  var match, expr;

  if (match = /^"([^"]*)"/.exec(program)) {
    expr = {type: "value", value: match[1]};
  } else if (match = /^\d+\b/.exec(program)) {
    expr = {type: "value", value: Number(match[0])};
  } else if (match = /^[^\s(),"]+/.exec(program)) {
    expr = {type: "word", name: match[0]};
  } else {
    throw new SyntaxError(`Unexpect syntax: ${program}`);
  }

  return parseApply(expr, program.slice(match[0].length));
}

function skipSpace(string) {
  var first = string.search(/\S/);
  if (first == -1) {
    return '';
  }

  return string.slice(first);
}
// Since Egg doesn't care about whitespace we need to trim it, that's what
// the skipSpace(..) function is for.

// After that parseExpression(..) then uses three regular expressions to parse
// the atomic elements Egg supports, strings, numbers, and words. The data structure
// constructed is different depending on the match. If there isn't a match at all
// then we throw a syntax error.

// We can then cut off the part we matched from the string and pass it along with
// the object for the expression to parseApply(..) that checks whether the expression
// is an application, and if so parses its arguments.
function parseApply(expr, program) {
  program = skipSpace(program);

  if (program[0] != '(') {
    return {expr: expr, rest: program};
  }

  program = skipSpace(program.slice(1));
  expr = {type: 'apply', operator: expr, args: []};

  while (program[0] != ')') {
    var arg = parseExpression(program);
    expr.args.push(arg.expr);
    program = skipSpace(arg.rest);

    if (program[0] == ',') {
      program = skipSpace(program.slice(1));
    } else if (program[0] != ')') {
      throw new SyntaxError("Expected ',' or ')'");
    }
  }

  return parseApply(expr, program.slice(1));
}
// If the next character isn't an opening parenthesis, it's not an application and
// parseApply just returns the expression object.

// Otherwise it skips the opening parenthesis and creates the syntax tree object
// for the application expression. Then it calls parseExpression recursively to parse
// each argument till a closing parenthesis is found. This is indirect recursion
// through parseApply(..) and parseExpression(..) calling each other.

// Because applications can be applied (like multiplier(1)(2)), parseApply must call
// itself to check for nesting.

// That's what we need to parse Egg. We'll wrap it all in a wrapper parse(..) function
// that verifies we reached the end of input after parsing the expression, (Egg
// programs are single expressions) and gives us the program data structure.
function parse(program) {
  var result = parseExpression(program);

  if (skipSpace(result.rest).length > 0) {
    throw new SyntaxError('Unexpected text after program');
  }

  return result.expr;
}

console.log(parse('+(a, 10)'));
// It's working, no real helpful info on fail and it doesn't store line and column
// where each expression starts which would be helpful for error reports but good
// enough.

/// The Evaluator

// Lets write an evaluate(..) function that will run a syntax tree given an environment.
// The environment object associates names and values and evaluate(..) will return
// the value that the syntax tree expression produces.
function evaluate(expr, env) {
  switch(expr.type) {
    case 'value':
      return expr.value;

    case 'word':
      if (expr.name in env) {
        return env[expr.name];
      } else {
        throw new ReferenceError(`Undefined variable: ${expr.name}`);
      }

    case 'apply':
      if (expr.operator.type == 'word' && expr.operator.name in specialForms) {
        return specialForms[expr.operator.name](expr.args, env);
      }

      var op = evaluate(expr.operator, env);
      if (typeof op != "function") {
        throw new TypeError('Applying a non-function');
      }

      return op.apply(null, expr.args.map(function(arg) {
        return evaluate(arg, env);
      }));
  }
}

var specialForms = Object.create(null);
// We have a case for each expression type. Literal values produce the values,
// for variables we first check that the exist in the environment before returning it.

// Applications are more involved, if they're a special form like "if", we don't evaluate
// and just pass the argument expression and environment to the function that handles
// this form. If it's a normal call, we evaluate the operator (verifying it's a
// function) and call it with the result of evaluating the arguments.

// We'll use JavaScript functions to represent the functions in Egg. The recursive
// structure of the evaluator is similar to the parser, both mirroring the language
// structure. It's also possible to integrate the parser and evaluator so that you
// can do both in sequence but this is more readable.

// That's all we really need to interpret Egg, but we need to define some special
// forms to make the language more useful.

/// Special Forms

// The specialForms object is used to define custom syntax, associating words with
// functions that evaluate such forms. Let's add some.
specialForms['if'] = function(args, env) {
  if (args.length != 3) {
    throw new SyntaxError('Bad number of args to if');
  }

  if (evaluate(args[0], env) !== false) {
    return evaluate(args[1], env);
  } else {
    return evaluate(args[2], env);
  }
}
// Egg's 'if' expects exactly 3 arguments, it evaluates the first and if it isn't
// false it evaluates the second, otherwise the third is evaluated. The if form
// is more similar to the Javascript ternary operator. It's an expression that produces
// a value, either the result of the second or third argument.

// Egg differs in how it handles the 'if' condition, it won't treat 0 or '' as false
// like Javascript.

// The reason 'if' is a special form instead of a function is that all arguments
// to functions are evaluated before the function is called, but 'if' should only
// evaluate either it's second or third argument depending on the first.

// The 'while' form is similar.
specialForms['while'] = function(args, env) {
  if (args.length != 2) {
    throw new SyntaxError('Bad number of args to while');
  }

  while(evaluate(args[0], env) !== false) {
    evaluate(args[1], env);
  }
  // Egg has no undefined so we return false when there's no meaningful result.
  return false;
}
// The 'do' form executes all its arguments from top to bottom, making its value
// the value produced by the last argument.
specialForms['do'] = function(args, env) {
  var value = false;

  args.forEach(arg => value = evaluate(arg, env));

  return value;
}
// To create variables and give them names we'll create a 'define' form. It expects
// a word as its first argument and an expression that produces a value as its second.
// We'll make it return the value assigned (like Javascript's '=' operator).
specialForms['define'] = function(args, env) {
  if (args.length != 2 || args[0].type != 'word') {
    throw new SyntaxError('Bad use of define');
  }

  var value = evaluate(args[1], env);
  env[args[0].name] = value;
  return value;
}

/// The Environment

// The environment accepted by evaluate is an object with properties whose names
// map to variable names and values map to the values bound to the variables.

// To work with conditional we'll need boolean values so we'll define true and false
// now.
var topEnv = Object.create(null);
topEnv['true'] = true;
topEnv['false'] = false;
// This lets us evaluate simple expressions negating Boolean values
var prog = parse('if(true, false, true)');
console.log(evaluate(prog, topEnv));
// To give us basic arithmetic and comparisons we'll also add some functions to the
// environment. For the sake of brevity we use a forEach loop and the function constructor
// to create each operator.
['+', '-', '*', '/', '==', '<', '>'].forEach(op => {
  topEnv[op] = new Function('a, b', `return a ${op} b;`);
});
// We need a way to print values too so we'll wrap console.log in a function called
// print.
topEnv['print'] = function(value) {
  console.log(value);
  return value;
};
// Those are the elementary tools we need to write simple programs. This run function
// provides a simple way to write and run programs. It creates a new environment
// and parses and evaluates the strings given to it as a single program..
function run() {
  var env = Object.create(topEnv);
  var program = Array.prototype.slice.call(arguments, 0).join('\n');
  return evaluate(parse(program), env);
}
// We use Array.prototype.slice.call as a trick to convert the arguments object
// into an array.
run("do(define(total, 0),",
    "   define(count, 1),",
    "   while(<(count, 11),",
    "         do(define(total, +(total, count)),",
    "            define(count, +(count, 1)))),",
    "   print(total))");
// Here's a sample Egg program that computes the sum of 1-10.

/// Functions

// Let's add functions to our programming language, we'll add a 'fun' construct that
// treats its last argument as the function body and all preceeding arguments as
// the function's arguments.
specialForms['fun'] = function(args, env) {
  if (!args.length) {
    throw new SyntaxError('Functions need a body.')
  }

  function name(expr) {
    if (expr.type != 'word') {
      throw new SyntaxError('Arg names must be words');
    }

    return expr.name;
  }

  var argNames = args.slice(0, args.length - 1).map(name);
  var body = args[args.length - 1];

  return function() {
    if (arguments.length != argNames.length) {
      throw new TypeError('Wrong number of arguments');
    }

    var localEnv = Object.create(env);
    for (var i = 0; i < arguments.length; i++) {
      localEnv[argNames[i]] = arguments[i];
    }

    return evaluate(body, localEnv);
  };
}
// Egg functions have their own environment just like in JavaScript and we use
// Object.create to create a copy of the outer environment so that we can have
// access to enclosing variables without modifying the outer scope. The function
// created by 'fun' creates this environment and adds the argument variables to it
// and then evaluates the function body in this environment and returns it.
run("do(define(plusOne, fun(a, +(a, 1))),",
    "   print(plusOne(10)))");

run("do(define(pow, fun(base, exp,",
    "     if(==(exp, 0),",
    "        1,",
    "        *(base, pow(base, -(exp, 1)))))),",
    "   print(pow(2, 10)))");

/// Compilation

// We've built an interpretter, during evaluation it acts directly on the representation
// of the program produced by the parser.

// Compilation is another step between the parsing and running of a program, that
// transforms the program into something that can be evaluated more efficiently
// by doing more work ahead of time. For example in well-designed languages it's
// possible to know for each use of a variable which variable is being referred to
// without running the program. This can be used to avoid having to look up the variable
// every time it's accessed.

// Traditionally compilation involves converting a program to machine code, a raw
// format computers can process. But any process that converts a program from one
// representation to another can be thought of as compilation.

// It's possible to write a different evaluation strategy for Egg that first converts
// the program into a JavaScript program, and then runs the results. If done right
// this would make Egg very fast while still simple to implement.

/// Cheating

// You notice that our if and while were basically wrappers around the JavaScript
// constructs. If we compared the implementation of Egg on JavaScript with other
// languages the difference is huge. The amount of difference in effort and complexity
// is massive. Regardless this example gave you an idea of how programming languages
// work.

// When it comes to getting things done, cheating is more effective than doing
// everything yourself. Though this language doesn't do anything JavaScript can't
// there's situations where writing simple languages helps. Such languages don't
// have to resemble typical programming languages. If JavaScript didn't come with
// Regular Expressions you could write your own parser and evaluator for such languages.

// Imagine you're building a giant robot and need to program its behavior. Instead
// of JavaScript you might want a language that looks like this.

// behavior walk
//   perform when
//     destination ahead
//   actions
//     move left-foot
//     move right-foot

// behavior attack
//   perform when
//     Godzilla in-view
//   actions
//     fire laser-eyes
//     launch arm-rockets

// This is called a Domain Specific Language (DSL) a language made to express a
// narrow domain of knowledge. Such languages can be more expressive than general
// purpose languages because they're designed exactly to express the things in that
// domain.
