// Eloquent Javascript Chapter 08: Bugs and Error Handling - Notes

/// Programmer Mistakes

// Javascript is a language that doesn't have a ton of facilities to catch mistakes.
// Improper code will often continue on just producing NaN or undefined and make
// difficult to correct problems.

/// Strict Mode

// You can make Javascript a little more strict by using strict mode. You put a string
// "use strict" at the top of a function or file. For example:
// function canYouSpotTheProblem() {
//   "use strict";
//   for (counter = 0; counter < 10; counter++) {
//     console.log("Happy happy");
//   }
// }

// canYouSpotTheProblem(); -> ReferenceError: counter is not defined
// Normally when you don't put var in front of a variable, Javascript implicitly
// creates a global variable to use. In strict mode, this isn't the case and an error
// is thrown. Though the variable won't be created if it exists as a global variable.

// Another effect of strict mode is that `this` is only bound for methods, otherwise
// it has undefined for a value. Outside of strict mode, `this` will refer to the
// global object. So if you accidentally call a method or constructor incorrectly
// in strict mode, Javascript raises an error instead of using the global object.

// Consider this example:
function Person(name) { this.name = name; }
var ferdinand = Person("Ferdinand"); // no "new" keyword
console.log(name);
// So this bad call to Person "succeeds" but returns undefined and added a global
// name variable. In strict mode however it's different.
// "use strict";
// function Person(name) { this.name = name; }
// Forget `new`
// var ferdinand = Person("Ferdinand"); -> TypeError: Cannot set property 'name' of undefined

// The error provides immediate feedback which is helpful. Strict mode does some other
// stuff. It doesn't allow functions to have multiple arguments of the same name
// and removes problematic language features (like the `with` statement).
// Put shortly, it rarely hurts to put "use strict" into your program.

/// Testing

// Since Javascript doesn't do much to help us find mistakes we'll have to test our code.
// Here's an example with a Vector type
function Vector(x, y) {
  this.x = x;
  this.y = y;
}

Vector.prototype.plus = function(other) {
  return new Vector(this.x + other.x, this.y + other.y);
};
// We'll write a program that tests our Vector implementation for proper bevior.
// Then we run the program to be reasonably confident that it's not broken.
// When we add additional functionality we also add tests for the feature.
function testVector() {
  var p1 = new Vector(10, 20);
  var p2 = new Vector(-10, 5);
  var p3 = p1.plus(p2);

  if (p1.x !== 10) {
    return "fail: x property";
  }
  if (p1.y !== 20) {
    return "fail: y property";
  }
  if (p2.x !== -10) {
    return "fail: negative x property";
  }
  if (p3.x !== 0) {
    return "fail: x from plus";
  }
  if (p3.y !== 25) {
    return "fail: y from plus";
  }

  return "everything ok";
}

console.log(testVector());
// Writing tests like this tends to be tedious. Thankfully there are pre-existing
// solutions that help you build and run test suites by providing a language (in
// the form of functins and methods) suited to expressing tests and outputting
// info upon failure. These are called testing frameworks.

/// Debugging

// Once you notice something wrong with your program because it's misbehaving or
// producing errors, the next step is to figure out what the problem is.

// Sometimes it's obvious. The error message will point at the specific line
// of code, and if you look at the description and the line of code you'll often
// see the problem.

// But other times the line that triggered the problem is simply the first place
// where a bad value produced elsewhere gets used in an invalid way. Sometimes there
// no error message at all, just an invalid result.

// The following example tries to convert a whole number to a string in any base
// by repeatedly picking out the last digit and the dividing the number to get
// rid of the digit. But the output it produces suggests a bug is present.
function numberToString(n, base) {
  var result = "", sign = "";
  if (n < 0) {
    sign = "-";
    n = -n;
  }

  do {
    result = String(n % base) + result;
    n /= base;
  } while (n > 0);

  return sign + result;
}

console.log(numberToString(13, 10));
function numberToStringFixed(n, base) {
  var result = "", sign = "";
  if (n < 0) {
    sign = "-";
    n = -n;
  }

  do {
    result = String(n % base) + result;
    n = Math.floor(n / base);
  } while (n > 0);

  return sign + result;
}
console.log(numberToStringFixed(13, 10));
// Think, analyze what's happening and come up with a theory of why it might be
// happening. Then make observations to test the theory. Putting strategic
// console.log calls in the code is a good way to get additional info, in this
// case we want 'n' to take the values of 13, 1 and then 0. Here are the values
// from the start of the loop
// 13
// 1.3
// 0.13
// 0.013
// ...
// 1.5e-323
// Dividing 13 by 10 doesn't return a whole number so instead of n /= base, we need
// the floor of n / base with m = Math.floor(n / base) so it's properly shifted right.

// Alternatively you could use the debugger in a broweser instead of console.log.
// You can set a breakpoint in modern browsers on a specific line of code. This
// causes execution to stop when your breakpoint is reached and allows you to inspect
// the values of variables at the time. Another way to set a breakpoint is to put the
// 'debugger' keyword in the code and the developer tools will pause execution when
// they reach that statement.

/// Error Propagation

// Not all problems are preventable, if your program communicates with the outside
// world there's a chance the input it gets will be invalid or other systems it talks
// to are broken.

// Simple programs can choose to ignore errors and just crash, real programs though
// must sometimes take bad data and try to use it anyways. Other times you need to
// report to the user what went wrong and then give up, either way the program does
// something in response to the problem.

// Say there's a function that prompts a user for a number and returns, it. What
// happens when they enter "orange"? You could return special values, like null, or
// undefined.
// function promptNumber(question) {
//   var result = Number(prompt(question, ''));
//
//   if (isNaN(result)) {
//     return null;
//   } else {
//     return result;
//   }
// }
//
// console.log(promptNumber("How many trees do you see?"));

// Special values can make cluttered code and can be hard to distinguish from other
// data. We need a way to stop and jump back to where the problem can be handled.

/// Exceptions

// Exceptions are a way for programs to do this. A program can raise (or throw) an
// exception that will jump up its call stack (called unwinding the call-stack)
// until it reaches the first call that started the stack. You can "catch" exceptions
// and do something with them and keep the program running. Here's an example:
// function promptDirection(question) {
//   var result = Number(prompt(question, ''));
//
//   if (result.toLowerCase() == 'left') {
//     return 'L';
//   }
//   if (result.toLowerCase() == 'right') {
//     return 'R';
//   }
//
//   throw new Error("Invalid direction: " + result);
// }
//
// function look() {
//   if (promptDirection('Which way?') == 'L') {
//     return "a house";
//   } else {
//     return "two bears";
//   }
// }
//
// try {
//   console.log("You see: ", look());
// } catch (error) {
//   console.log("Something went wrong: " + error);
// }

// The throw keyword is used to raise an exception. You can catch one by wrapping
// code in a try block followed by the keyword catch. When the code in the try block
// causes an exception, the catch block will be evaluated. The variable in the parantheses
// after catch is bound to the exception value. After the catch block finishes, cotrol
// proceeds beneath the try/catch block.

// We used the Error constructor in this case, this is a standard Javascript constructor
// that creates an object with a message property. In modern Javascript environments
// it also gathers additional information about the call stack (stacktrace) stored
// in the stack property that can be helpful for debugging. Now the function look
// can ignore if a value isn't valid from promptDirection. This is the advantage of
// exceptions, error handling code is only necessary at the point where the error
// occur and where it's handled.

/// Cleaning Up After Exceptions

// Consider this, a function withContext, wants to make sure that during its execution
// the variable context holds a specific context value and restores it when it finishes
// to its old value.
var context = null;

function withContext(newContext, body) {
  var oldContext = context;
  context = newContext;
  var result = body();
  context = oldContext;
  return result;
}
// If body() raises an exception the context will never be reset, possibly causing
// other issues. There's another feature in try blocks, the finally block. It will
// be run no matter whether the code in the try block succeeds or fails. This is
// where clean-up code can be run.
function withContext(newContext, body) {
  var oldContext = context;
  context = newContext;
  try {
    return result = body();
  } finally {
    context = oldContext;
  }
}
// Now we don't need to store the result of body, we can do this now and be safe.
try {
  withContext(5, function() {
    if (context < 10) {
      throw new Error("Not enough context!");
    }
  });
} catch (e) {
  console.log("Ignoring: " + e);
}

console.log(context);
// Even though the code failed, withContext cleaned up after itself.

/// Selective Catching

// When an exception makes it to the bottom of the stack without being handled, the environment
// handles it. This differs based on environments but the browser a description of the error
// is printed to the console. For programming mistakes that the program can't possibly handle
// this is fine. An exception is a reasonable way to signal a broken program and the console
// will provide you with information about the function calls leading to the problem.

// For problems that are expected though, crashing won't do. Invalid use of the language,
// like referencing non-existent variables will result in exceptions being raised which you
// can catch. When the catch body is entered, all we know is that *something* in the try
// caused an exception, but we don't know *what* caused it. Javascript doesn't provide
// support for selectively catching exceptions, it's either all or nothing, making it easy
// to assume the exception that's caught is the one you expected. But that's not always the
// case. Some other part of code could have caused the exception, here's an example that
// attempts to keep calling promptDirection until a valid answer.
// for (;;) {
//   try {
//     var dir = prompDirection("Where?"); // <- Typo
//     console.log("You chose: " , dir);
//     break
//   } catch (e) {
//     console.log("Not a valid direction, try again");
//   }
// }

// The for (;;) construct is a way to create a loop that doesn't terminate on its own. We
// break out only explicitly from the loop. Unfortunately the function name is misspelled
// resulting in a ReferenceError and since the catch block ignores the value we have an infinite
// loop with a buried exception. Rule of thumb, don't blanket catch exceptions unless you're
// routing them somewhere like over a network to inform a system of an error.

// So how do we catch specific exceptions? We do it by checking in the catch block that the
// exception is the one we want or throw it again. We could check the message property of
// an exception, but that's brittle, instead we'll use the instanceof keyword and define
// a new exception type. For example:
function InputError(message) {
  this.message = message;
  this.stack = (new Error()).stack;
}

InputError.prototype = Object.create(Error.prototype);
InputError.prototype.name = "InputError";
// The prototype derives from Error so that instanceof Error will also return true and we
// give it a name property to match the other exception types. The assignment to the stack
// property tries to give this exception a useful stack trace by creating an Error object
// and using its stack property.

// Now promptDirection can use this error.
// function promptDirection(question) {
//   var result = prompt(question, "");
//
//   if (result.toLowerCase() == 'left') {
//     return 'L';
//   }
//   if (result.toLowerCase() == 'right') {
//     return 'R';
//   }
//
//   throw new InputError("Invalid direction: " + result);
// }

// and our loop can selectively catch our exception.

// for (;;) {
//   try {
//     var dir = promptDirection("Where?");
//     console.log("You chose: " , dir);
//     break
//   } catch (e) {
//     if (e instanceof InputError) {
//       console.log("Not a valid direction, try again");
//     } else {
//       throw e;
//     }
//   }
// }
// This catches only instances of InputError and leaves unrelated errors alone, so if the
// typo is introduced again, it'll be properly reported instead of being swallowed.

/// Assertions

// Assertions are a tool for basic sanity checking for programming errors. Consider
// this example:
function AssertionFailed(message) {
  this.message = message;
}

AssertionFailed.prototype = Object.create(Error.prototype);

function assert(test, message) {
  if (!test) {
    throw new AssertionFailed(message);
  }
}

function lastElement(array) {
  assert(array.length > 0, "empty array in lastElement");
  return array[array.length - 1];
}

console.log(lastElement([1, 2, 3]));
// console.log(lastElement([])); -> AssertionFailed

// This provides a nice way to enfore expectations, helpfully blowing up the program
// if the stated condition doesn't hold. For instance the lastElement function, which
// gets the last element from an array, would return undefined on empty arrays if the
// assertion was omitted. Since this doesn't make sense to fetch the last element
// on an empty array it's almost certainly a programmer error.

// Assertions are a way to make sure mistakes cause failures at the point of the mistake,
// instead of silently returning nonsense values that can cause trouble in unrelated
// parts of a system.

/// Summary

// Mistakes and bad input are facts of life. Bugs need to found and fixed.
// It becomes easier to notice with test suites, and assertions in your program.
// Problems caused by outside factors to the program control should be handled gracefully.
// Sometimes when the problem can be handled locally, special return values can
// help track them. Otherwise exceptions are preferable.

// Throwing an exception causes the call stack to be unwound until the next enclosing
// try/catch block or until we hit the bottom of the stack. The exception value
// will be given to the catch block that catches it, which should verify it's the
// expected exception type and do something with it. To deal with unpredictable
// control flow caused by exceptions the finally block can be used for clean-up
// code that always needs to run.
