/// Eloquent Javascript Chapter 03: Functions - Notes

// Functions are the bread and butter of JavaScript Programming.
// Functions allow a programmer to wrap up code into an executable, reusable
// value. A Function is the main way a programmer introduces new vocabulary into
// JavaScript.

/// Defining A Function

// A function definition is just a variable definition where the value the variable
// is assigned to is a function. For example:
var square = function(x) {
  return x * x;
};

console.log(square(12));
// A function is created by an expression that begins with the `function` keyword,
// Functions have a set of parameters (in the example, `x`) and a body, which
// is the set of statements to run when the function is called. Function bodies
// must be wrapped in braces. Functions can be parameterless, for example:
var makeNoise = function() {
  console.log("Pling!");
}

makeNoise();
// Or multiple parameters...
var power = function(base, exponent) {
  var result = 1;
  for (var count = 0; count < exponent; count++) {
    result *= base;
  }

  return result
}

console.log(power(2, 10));
// Functions can either return values or just have side-effects.
// power and square both return values (thus the use of the return keyword),
// but makeNoise only has a side-effect. When the engine encounters the return
// keyword the value is returned and immediately returns to the code the executed
// the function. The return keyword without any value or expression after it will
// return undefined.

/// Parameters and Scope

// Function parameters behave like regular variables, but their initial values are
// given by the caller of the function, not the function code itself.
// An important property of functions is that they're parameters and variables
// defined within them are scoped to the function and local to it.
// Anything defined outside of the function could be globally scoped or scoped to
// an enclosing function, it's possible to access these variables from within a
// function as long as the name has not been shadowed.
// The following example demonstrates this:
var x = "outside";

var f1 = function() {
  var x = "inside f1";
};
f1();
console.log(x);

var f2 = function() {
  x = "inside f2";
}
f2();
console.log(x);
// In the first function, we declare a variable x, thus shadowing the x value.
// However in the second, we simply reassign x thus changing its value.
// This kind of behavior helps prevent accidental name collisions, if all variables
// shared a single scope it'd be much harder to make sure no names collide.
// This allows you to worry about variables as they relate to the scope you're in
// rather than having to remember all names and where they change values.

/// Nested Scope

// In Javascript scopes can be nested as well. Meaning this example works.
var landscape = function() {
  var result = "";
  var flat = function(size) {
    for (var count = 0; count < size; count++) {
      result += "_";
    }
  };
  var mountain = function(size) {
    result += "/";
    for (var count = 0; count < size; count++) {
      result += "'";
    }
    result += "\\";
  };

  flat(3);
  mountain(4);
  flat(6);
  mountain(1);
  flat(1);
  return result;
}

console.log(landscape());
// Since the flat and mountain functions exist within the landscape function's scope
// They have access to the result variable. But they also can't access each other's
// count variable because of their own local scopes. Finally the enviroment outside
// the landscape function has no access to any of the variables within landscape's
// scope.

// In short, a scope can "see" any enclosing scopes. The set of variables available
// to a function are determined by where in the code the function is. All variables
// from blocks around a function definition are accessible.
// Coming from other languages people expect this example to produce a separate local
// scope. In JavaScript this doesn't work, functions are the only things that create
// new scopes.
var something = 1;
{
  // Gets redefined.
  var something = 2;
}
console.log(something);
// However, JavaScript will add the `let` keyword that can create variables that are
// local to blocks rather than functions.

/// Functions as Values

// Function variables usually act as name for a specific piece of the program.
// Such a variable is defined once and not changed, this makes it easy to confuse
// the function and its name. But they are different, a function value can do the
// same things other values can, and it's variable can do all the things other
// variables can, like reassignment, take this hypothetical example.
// var launchMissles = function(value) {
//   missleSystem.launch("now");
// };

// if (safeMode) {
//   launchMissles = function(value) {};
// }

/// Declaration Notation

// There's a shorter, better way to declare functions, it's the preferred method too.
// Example:
function square(x) {
  return x * x;
}

console.log(square(4));
// There's one major but subtle difference between this form of function definition
// and the var approach. This example illustrates it.
console.log(`The future says: ${future()}`);

function future() {
  return "We STILL have no flying cars.";
}
// The code works even though the function is defined after its use. This is because
// function declarations aren't part of the normal top-to-bottom execution.
// Conceptually they are moved to the top of the scope and can be used by all the code
// in that scope.

// Note, don't declare functions inside a conditional or loop. In the past different
// browsers had different behaviour for how this was handled and the latest standard
// forbids it.
// function example() {
//   function a() {} // Okay
//   if (something) {
//     function b() {} // Danger!
//   }
// }

/// The Call Stack

// Let's look at how control flows through functions. Here's a simple example.
function greet(who) {
  console.log(`Hello ${who}`);
}

greet("Harry");
console.log("Bye");

// The run-down of the control flow goes like this.
// call to greet causes the flow to jump to the start of the function.
// console.log is called, does its work, then returns control to where it was called.
// greet ends, so the control is returned to where it was called (line 4),
// then the last line calls console.log again.

// We can think of the control flow like this.
// top
//    greet
//         console.log
//    greet
// top
//    console.log
// top

// Because functions must jump back where they were called from, the computer must
// remember the context of the function call. The computer stores this context is called
// the call stack. Every time a function is called it is placed on top of the stack
// When it's done it is removed off the top of the stack.

// Storing the stack requires computer memory, when the stack grows too large an error
// will occur (such as a stack overflow), here's an example that would cause that.
// function chicken() {
//   return egg();
// }

// function egg() {
//   return chicken();
// }
// console.log(`${chicken()} came first.`);

/// Optional Arguments

// This works... `alert('Hello', 'Good Evening', 'How do you do?');
// But officially the alert function only takes one argument, so why?
// JavaScript is very lax with parameters. If you add more arguments than necessary
// it simply ignores them and if you don't add enough it'll just use `undefined`
// in the place of the argument. The downside of course being that you'll likely
// pass the wrong number of arguments to a function without any indication that
// it is wrong.

// The upside is that you can use this behavior to have "optional" arguments.
// Here's an example of the power function with an optional argument.
// renamed due to shadowing by the var power = function... version.
function power_2(base, exponent) {
  if (exponent == undefined) {
    exponent = 2;
  }

  var result = 1;
  for (var count = 0; count < exponent; count++) {
    result *= base;
  }

  return result
}

console.log(power_2(4));
console.log(power_2(4, 3));
// In this case if you don't provide an exponent power acts like a squaring function

/// Closure

// The ability to treat functions as values and the fact that their
// local scope is recreated on every invocation brings up an interesting question.
// What happens to the local scope when the function call is no longer active?
// This example demonstrates what occurs.
function wrapValue(n) {
  var localVariable = n;
  return function() { return localVariable; };
}

var wrap1 = wrapValue(1);
var wrap2 = wrapValue(2);
console.log(wrap1());
console.log(wrap2());
// This works as expected, you can still access the local variable and there can
// be more than one instance of it, this shows that the local variables are recreated
// each time and can't trample one another. This feature is called closure, it
// refers to being able to reference specific instances of local variables in an
// enclosing function. This behaviour frees you from worrying about variable lifetimes
// and allows for some creative uses of function values. For example:
function multiplier(factor) {
  return function(number) {
    return number * factor;
  };
}

var twice = multiplier(2);
console.log(twice(5));
// Thinking about functions this way takes some getting used to, a way to think of it
// is to think of the function keyword as freezing the code within its body and
// wrapping it in a package. So when you read `return function()...` think of it
// as returning a handle to a piece of computation for later use. In the example
// multiplier returns the frozen code that gets wrapped in the variable twice for
// later use, then when it's used it still has access to the factor parameter from
// the multiplier call for its use along with the number parameter passed to it.

/// Recursion

// It's alright for a function to call itself provided it doesn't overflow the stack.
// When a function calls itself it is called recursion. Recursion allows you to
// implement things in a different style, for example here's power done with recursion
function power_recur(base, exponent) {
  if (exponent == 0)
    return 1;
  else
    return base * power_recur(base, exponent - 1);
}

console.log(power_recur(2, 3));
// This is close to how Mathematicians define exponentiation and is arguably more
// elegant, the function calls itself multiple times till it reaches the answer.
// However, in JavaScript, recursion is more expensive than looping, about 10 times
// slower than looping. This dilemma of speed v. elegance is it's kind of a battle
// of human friendliness vs machine friendliness. Often a program deals with such
// complexity that giving up some efficiency to make the program easier to understand
// becomes worth it. The rule of thumb is that you shouldn't worry about efficiency
// until you know for sure that the program is too slow. If so, find out which parts
// are slow and refactor them for efficiency. This doesn't mean you should ignore
// efficiency altogether until it matters though, just that it should't be the only
// goal.

// Recursion is not always just a less-efficient way of doing things, sometimes
// it is easier to do something with recursion than looping. Most often this is when
// you must explore out or process multiple branches which might in turn branch out
// as well.

// Here is an example: by starting from 1 and repeatedly either adding 5 or multiplying
// by 3 an infinite number of new numbers can be produced. How would you write a function
// that given a number, tries to find a sequence of additions and multiplications
// that produce that number? Here is the recursive solution.
function findSolution(target) {
  function find(start, history) {
    if (start == target)
      return history;
    else if (start > target)
      return null;
    else
      return find(start + 5, `(${history} + 5)`) ||
             find(start * 3, `(${history} * 3)`);
  }
  return find(1, "1");
}

console.log(findSolution(24));
// Note this doesn't find the shortest solution, just any solution.
// Let's work through how this function works.

// The inner function find does the actual recursion. It takes two arguments,
// the current number, and a string that records how we reached this number.
// It then either returns a string for how to get to this target, or it returns
// null if it's unreachable. The function performs one of three things to do this.
// If the current number is the target, we return the current history.
// If the current number is greater than the target we return null since we can't
// reach the number via addition/multiplication of 3 or 5.
// Finally if we're still below the target we try both possible paths of either adding
// 5 or multiplying by 3 and if the first returns anything other than null, it's
// returned otherwise the second is returned regardless of its value.
// To better under stand this, here is the call stack for calling findSolution(13).
// find(1, "1")
//   find(6, "(1 + 5)")
//     find(11, "((1 + 5) + 5)")
//       find(16, "(((1 + 5) + 5) + 5)")
//         too big
//       find(33, "(((1 + 5) + 5) * 3)")
//         too big
//     find(18, "((1 + 5) * 3)")
//       too big
//   find(3, "(1 * 3)")
//     find(8, "((1 * 3) + 5)")
//       find(13, "(((1 * 3) + 5) + 5)")
//         found!
// The first time find is called it creates two paths, one for 1 + 5 and another
// for 1 * 3, the first call tries to find the solution using 1 + 5 as the start
// point and explores all possible solutions that yield a number less than or equal
// to the target. Since it can't, it returns null. The || operator then returns
// the result of the second call to find with 1 * 3 as its start point, this
// path has more luck and with two recursive calls finds the result and returns,
// passing its result up the call stack to the outermost function call of findSolution.

/// Growing Functions

// There's two ways functions get introduced into code. The first is that you find
// yourself repeating yourself in your code so you take that repeated code, find
// a good name for the operation and wrap it into a function. The second way is
// that you find that you need some functionality you haven't written yet that
// sounds like it deserves its own function. You start by naming the function then
// writing the body, you may even start writing code that uses this function before
// you've written it. The difficulty in finding a name for a function reflects how
// clear the concept you are trying to wrap is. Let's cover an example.
// We're writing a program that prints two numbers, the number of cows and chickens
// a farmer has with the words `Cow` and `Chicken` after them and zero padding so
// they're always 3 digits.
// Example output
// 007 Cows
// 011 Chickens
// function printFarmInventory(cows, chickens) {
//   var cowString = String(cows);
//   while (cowString.length < 3)
//     cowString = '0' + cowString;
//   console.log(`${cowString} Cows`);
//   var chickenString = String(chickens);
//   while (chickenString.length < 3)
//     chickenString = '0' + chickenString;
//   console.log(`${chickenString} Chickens`);
// }
// printFarmInventory(7, 11);
// Cool, but now when we're about ready the farmer says he needs to count his pigs
// too. We stop and consider this and decide to refactor to save on typing. So we
// try this.
// function printZeroPaddedWithLabel(number, label) {
//   var numberString = String(number);
//   while (numberString.length < 3)
//     numberString = '0' + numberString;
//   console.log(`${numberString} ${label}`);
// }

// function printFarmInventory(cows, chickens, pigs) {
//   printZeroPaddedWithLabel(cows, 'Cows');
//   printZeroPaddedWithLabel(chickens, 'Chickens');
//   printZeroPaddedWithLabel(pigs, 'Pigs');
// }
// printFarmInventory(7, 11, 3);
// This works, but the function printZeroPaddedWithLabel is awkward, it bunches
// up too many things. Instead of lifting all the repeated code, let's refactor
// a single concept.
function zeroPad(number, width) {
  var string = String(number);
  while (string.length < 3)
    string = '0' + string;
  return string;
}

function printFarmInventory(cows, chickens, pigs) {
  console.log(`${zeroPad(cows, 3)} Cows`);
  console.log(`${zeroPad(chickens, 3)} Chickens`);
  console.log(`${zeroPad(pigs, 3)} Pigs`);
}
printFarmInventory(7, 16, 3);
// A function like zeroPad has a nice obvious name and only wraps up a single
// concept into it. This makes it easier to understand for someone reading the
// code. It's also useful in more situations than just this specific program.
// How versatile of a function you write depends on your goals. However, a useful
// principle is to not add cleverness unless you know you will need it. It can
// be tempting to write generalized "frameworks" however this can get in the way
// of work being done.

/// Functions and Side-Effects

// Functions can be divided into two types, those with side-effects and those
// that return values (though it's possible for a function to do both.)
// The first helper we wrote printZeroPaddedWithLabel, is a function with a side-effect
// it prints a line. The second helper, zeroPad has a return value. It's not a
// coincidence that the second is more useful in different situations.

// A pure function is a function that has no side-effects and that doesn't rely
// on any side-effects of other functions, for example global variables that are
// changed by other functions. A pure function has the property that when called
// with the same arguments it always produces the same value. This makes it easy
// to reason about. Nonpure functions however do not have this nice property and
// may produce other values base on various factors, making them harder to reason
// about. Still, there's no reason to dislike nonpure functions, they can be useful
// and side-effects can be useful. Some operations are easier to do with a side-effect
// rather than a return value.

/// Summary

// This chapter taught you how to write your own functions. There are two ways to
// do this. Either by assigning a variable to a function.
// var f = function(a) {
//   console.log(a + 2);
// }
// or by use the function declaration syntax.
// function g(a, b) {
//   return a * b * 3.5;
// }
// A key aspect of understanding functions is understanding scope. Parameters and
// variables declared in a function are local to that function and recreated on
// each invocation, nested functions have access to enclosing scopes.
// Separating tasks into functions is useful to reduce repetition and can make code
// more readable by grouping code into conceptual chunks.
