"use strict";
// Eloquent Javascript Chapter 07: Project -- Electronic Life - Notes

// In this chapter we're building a virtual ecosystem, populated with critters
// that move around and fight to survive.

/// Definition

// To make this task managable we're going to simplify the concept of a world.
// A world will be a 2D grid where each entity takes up a square of the grid, every
// turn the critters get a chance to take an action.

// So time and space are chopped into units with a fixed size, squares and turns.
// We define a world with a `plan`, an array of strings that layout the world's grid.
var plan = [
  "############################",
  "#      #    #      o      ##",
  "#                          #",
  "#          #####           #",
  "##         #   #    ##     #",
  "###           ##     #     #",
  "#           ###      #     #",
  "#   ####                   #",
  "#   ##       o             #",
  "# o  #         o       ### #",
  "#    #                     #",
  "############################"
];
// The '#' characters are walls and rocks, the 'o' characters represent critters.
// Spaces are empty space. The plan array can be used to create a world object,
// which will keep track of the size and content of the world. It'll have a toString
// method, which will convert it back to a printable string, to be able see what's going
// on inside. There is also a turn method that allows critters to take a turn and
// updates the world to reflect their actions.

/// Representing Space

// The world grid has a fixed width and height, squares are identified by x and y
// coordinates. We'll use a simple Vector type to represent coordinate pairs.
function Vector(x, y) {
  this.x = x;
  this.y = y;
}

Vector.prototype.plus = function(other) {
  return new Vector(this.x + other.x, this.y + other.y);
};
// Next we need an object type to model the grid. A grid is a part of a world but
// we're making it a separate object (that will be a property of the world object)
// to keep the world object simple. Separation of concerns. We have several options
// for storing grids of values, we can use an array of row arrays and two property
// access to get a specific square.
var grid = [["top left", "top middle", "top right"],
            ["bottom left", "bottom middle", "bottom right"]];
console.log(grid[1][2]);
// Or we use a single array with size width * height and decide that the element at
// (x, y) is at position x + (y x width) in the array.
grid = ["top left", "top middle", "top right",
        "bottom left", "bottom middle", "bottom right"];
console.log(grid[2 + (1 * 3)]);
// Since the actual access is wrapped in methods it won't matter which approach is taken
// let's go with the second because it's easier to create the array. Calling The
// Array constructor with a single number as an argument, it creates an array of
// given length. Let's define the Grid object and methods.
function Grid(width, height) {
  this.space = new Array(width * height);
  this.width = width;
  this.height = height;
}

Grid.prototype.isInside = function(vector) {
  return vector.x >= 0 && vector.x < this.width &&
         vector.y >= 0 && vector.y < this.height;
};

Grid.prototype.get = function(vector) {
  return this.space[vector.x + this.width * vector.y];
};

Grid.prototype.set = function(vector, value) {
  this.space[vector.x + this.width * vector.y] = value;
};
// and a test
grid = new Grid(5, 5);
console.log(grid.get(new Vector(1, 1)));
grid.set(new Vector(1, 1), "X");
console.log(grid.get(new Vector(1, 1)));

/// A Critter's Programming Interface

// Before starting on the World constructor, we need to get more specific about
// critter objects that will live in it. Each critter has an act method that returns
// an action. An action is an object with a type property, naming the type of action
// the critter wants to take (e.g. "move"), the action may also contain extra info
// such as direction it wants to move in.

// Critters can see only the squares around them, this can still be useful when deciding
// which action to take. When act is called it is given a `view` object that allows
// the critter to inspect its surroundings. We use compass directions. Here's the
// object we will use to map from direction names to coordinate offsets.
var directions = {
  "n": new Vector(0, -1),
  "ne": new Vector(1, -1),
  "e": new Vector(1, 0),
  "se": new Vector(1, 1),
  "s": new Vector(0, 1),
  "sw": new Vector(-1, 1),
  "w": new Vector(-1, 0),
  "nw": new Vector(-1, -1)
};
// The view object has a method look which takes a direction and returns a character
// like "#" or " ". This object provides the convenient methods find and findAll
// both take a map character as an argument, first returns a direction in which
// a character can be found next to a critter or null if no direction exists.
// Second returns an array containing all directions with the character.

// A simple critter that just goes one way till it runs into an obstacle and goes
// off in a random direction.
function randomElement(array) {
  return array[Math.floor(Math.random() * array.length)];
}

var directionNames = "n ne e se s sw w nw".split(" ");

function BouncingCritter() {
  this.direction = randomElement(directionNames);
}

BouncingCritter.prototype.act = function(view) {
  if (view.look(this.direction) !== " ") {
    this.direction = view.find(" ") || "s";
  }

  return {type: "move", direction: this.direction};
};
// The randomElement helper simply picks a random element from an array, Using
// Math.random to get a random index. We'll use it again as randomness is good
// for simulations.

// To pick a random direction, the bouncingCritter calls randomElement on an Array
// of directions. We could have used Object.keys to get this but it wouldn't guarantee
// the order of the properties.

// The `|| "s"` is the act method is there to prevent the direction being set to null.

/// The World object

// Let's start the World object, the constructor takes a plan and a legend as arguments
// A legend is an object that tells us what each character in the map means. It
// has a constructor for every character minus space which is null.
function elementFromChar(legend, ch) {
  if (ch === " ") {
    return null;
  }

  var element = new legend[ch]();
  element.originChar = ch;
  return element;
}

function World(map, legend) {
  var grid = new Grid(map[0].length, map.length);
  this.grid = grid;
  this.legend = legend;

  map.forEach(function(line, y) {
    for (var x = 0; x < line.length; x++) {
      grid.set(new Vector(x, y), elementFromChar(legend, line[x]));
    }
  });
}
// in elementFromChar we first create an instance of the right type by looking
// up the character's constructor and applying new to it. Then we add the originChar
// property to make it easy to find what character it was created from.

// We need this property when implementing the world's toString method. The method
// builds up a maplike string from the world's state by performing a 2D loop over
// the grid.
function charFromElement(element) {
  if (element === null) {
    return " ";
  } else {
    return element.originChar;
  }
}

World.prototype.toString = function() {
  var output = "";

  for (var y = 0; y < this.grid.height; y++) {
    for (var x = 0; x < this.grid.width; x++) {
      var element = this.grid.get(new Vector(x, y));
      output += charFromElement(element);
    }
    output += "\n";
  }

  return output;
};
// Walls are empty objects just used to take up space.
function Wall() {}

// Creating a World instance based on the plan from earlier and call toString
// on it we get a string very similar to the plan put in.
var world = new World(plan, {"#": Wall, "o": BouncingCritter});
console.log(world.toString());

/// This and Its Scope

// The World constructor calls forEach and it's interesting to note that in the function
// we're no longer directly in the function scope of the constructor. Each function
// call gets its own `this` value, so `this` in the inner function isn't the object
// constructed in the constructor, functions that aren't methods have `this` be
// the global object.

// This means we can't refer to this.grid inside the loop so we have the variable
// in the outer function called grid which the inner function can access through
// lexical scope. This is a quirk of JavaScript, and in ES6 there's a solution. But
// a common workaround is to bind `var self = this;` and refer to `self` Since
// it's visible to inner functions. The better choice is the mind method, letting
// us provide an explicit `this` value.
var test = {
  prop: 10,
  addPropTo: function(array) {
    return array.map(function(elt) {
      return this.prop + elt;
    }.bind(this));
  }
};
console.log(test.addPropTo([5]));
// The function map uses is the result of the bind call and has its this bound to
// the first argument of bind, the outer function's this value.

// Most standard higher-order array functions (forEach, map, etc) take an optional second
// argument that can be used to provide a `this` for the calls to the predicate
// Another way to write the previous example:
test = {
  prop: 10,
  addPropTo: function(array) {
    return array.map(function(elt) {
      return this.prop + elt;
    }, this);
  }
};
console.log(test.addPropTo([5]));
// This only works with higher-order functions that have a context parameter, if not
// you'll need to use a different approach. We can support a context parameter in
// our higher-order functions by using the call method to call the function argument.
// Here's forEach on our Grid type, it calls a function for each element that isn't
// null or undefined.
Grid.prototype.forEach = function(f, context) {
  for (var y = 0; y < this.height; y++) {
    for (var x = 0; x < this.width; x++) {
      var value = this.space[x + y * this.width];
      if (value != null) {
        f.call(context, value, new Vector(x, y));
      }
    }
  }
};

/// Animating Life

// Now we write a turn method for the World object that gives critters a chance to act.
// It'll go over the grid using forEach we just defined on Grid looking for objects
// with an act method, when it finds one it calls that method to get an Action object
// and does the action, for now that's "move" only.

// There's a problem here though, we need to track which critters have already had
// their turn.
World.prototype.turn = function () {
  var acted = [];

  this.grid.forEach(function(critter, vector) {
    if (critter.act && acted.indexOf(critter) === -1) {
      acted.push(critter);
      this.letAct(critter, vector);
    }
  }, this);
};
// We use the second parameter to the grid's forEach to be able to access the correct
// `this` value. The letAct contains the logic that allows critters to move.
World.prototype.letAct = function (critter, vector) {
  var action = critter.act(new View(this, vector));

  if (action && action.type === "move") {
    var dest = this.checkDestination(action, vector);
    if (dest && this.grid.get(dest) == null) {
      this.grid.set(vector, null);
      this.grid.set(dest, critter);
    }
  }
};

World.prototype.checkDestination = function(action, vector) {
  if (directions.hasOwnProperty(action.direction)) {
    var dest = vector.plus(directions[action.direction]);
    if (this.grid.isInside(dest)) {
      return dest;
    }
  }
};
// First we ask the critter to act, passing it a view object that knows about
// the world and the critter's position. The act method returns an action of some
// kind.

// If the action type isn't "move" we ignore it, if it is and has a direction property
// that's valid and the square is empty (null), we set the square the critter used to
// be to null and store the critter in the destination.

// Note that letAct takes care to ignore nonsense input, not assuming that the action's
// direction property is valid or that the type property makes sense. This is defensive
// programming and makes sense in some situations. Mainly to validate inputs coming
// from sources you don't control (like users or file input), but also useful to
// isolate subsystems from each other. Here it's so Critters can be programmed sloppily
// they don't need to verify their actions make sense. They can request an action,
// and the world figures it out.

// These two methods are private details of a World object, not the public API.
// Some languages provide ways to explicitly declare things as private, but not
// JavaScript so you have to rely on some other form of communication to describe
// the API. Some people use an underscore prefix to denote internal properties and
// methods.

// Now to add the View type:
function View(world, vector) {
  this.world = world;
  this.vector = vector;
}

View.prototype.look = function(dir) {
  var target = this.vector.plus(directions[dir]);

  if (this.world.grid.isInside(target)) {
    return charFromElement(this.world.grid.get(target));
  } else {
    return "#";
  }
};

View.prototype.findAll = function(ch) {
  var found = [];

  for (var dir in directions) {
    if (this.look(dir) === ch) {
      found.push(dir);
    }
  }

  return found;
};

View.prototype.find = function(ch) {
  var found = this.findAll(ch);

  if (found.length === 0) {
    return null;
  }

  return randomElement(found);
};
// The look method figures out the coordinates we're trying to look at and if they're
// inside the grid, finds the corresponding character to the element that sits there.
// For coordinates outside the grid look pretends that there's a wall there.

/// It Moves

// Now that we've added the necessary parts it should be possible to make the world
// move.
for (var i = 0; i < 5; i++) {
  world.turn();
  console.log(world.toString());
}
// Just printing many copies of the map is a lame way to observe the world. The
// author added an animateWorld function that runs it as an animation, moving 3
// turns per second until you hit stop. How it's implemented doesn't matter till
// later.

/// More Life Forms

// Let's add another critter, this one moves along walls, it keeps its left "hand"
// to the wall and follows.

// We need to be able to "compute" with compass directions. Since directions are
// modeled as strings, we need our own operation dirPlus to calculate them. So
// dirPlus("n", 1) means a 45 degree turn clockwise to "ne" and dirPlus("s", -2)
// is 90 degrees counterclockwise to "e".
function dirPlus(dir, n) {
  var index = directionNames.indexOf(dir);
  return directionNames[(index + n + 8) % 8];
}

function WallFollower() {
  this.dir = "s";
}

WallFollower.prototype.act = function(view) {
  var start = this.dir;

  if (view.look(dirPlus(this.dir, -3)) != " ") {
    start = this.dir = dirPlus(this.dir, -2);
  }

  while (view.look(this.dir) != " ") {
    this.dir = dirPlus(this.dir, 1);
    if (this.dir == start) break;
  }

  return {type: "move", direction: this.dir};
};
// The act method scans the critter's surroundings, starting from left and going
// clockwise till it finds an empty square moving in that direction.

// What complicates this is that the critter may end up in the middle of empty space
// and it will keep turning left running in circles. So there's an extra check to
// start scanning to the left oly if the critter has passed some obstacle, otherwise
// it scans directly ahead and walk straight in empty space.

// Also there's a test comparing this.dir to start after every pass in the loop to
// make sure the loop doesn't run forever when the critter gets walled in or can't
// find an empty square.

/// A More Lifelike Simulation

// Let's make this more interesting by adding food and reproduction. Every living
// thing now gets an energy property which is reduced by performing actions and
// increased by eating things. With enough energy critters reproduce, for simplicity
// we'll make them asexual.

// If critters just eat one another the world will become a lifeless wasteland.
// Let's add plants to prevent this. We'll need a different world with a different
// letAct method. We'll use inheritence to create the new lifelike world. The new
// letAct delegates work of performing actions to the functions stored in actionTypes.
function LifelikeWorld(map, legend) {
  World.call(this, map, legend);
}

LifelikeWorld.prototype = Object.create(World.prototype);

var actionTypes = Object.create(null);

LifelikeWorld.prototype.letAct = function(critter, vector) {
  var action = critter.act(new View(this, vector));
  var handled = action &&
    action.type in actionTypes &&
    actionTypes[action.type].call(this, critter, vector, action);

  if (!handled) {
    critter.energy -= 0.2;
    if (critter.energy <= 0) {
      this.grid.set(vector, null);
    }
  }
};
// letAct checks whether an action was returned at all, then whether a handler function
// for the action exists, and finally whether the handler returned true, indicating
// success. Note the use of call to give it access to the world.

// If the action didn't work the default is for the creature to wait, it loses 1/5th
// of a point of energy and if it hits zero energy it dies and is removed from the grid.

/// Action Handlers

// The simplest action type is growing which is used by plants, when an action
// like {type: "grow"} is returned the following handler is called.
actionTypes.grow = function(critter) {
  critter.energy += 0.5;
  return true;
};
// Growing adds half a point of energy to the plant and always succeeds.
// Moving is a bit more work.
actionTypes.move = function(critter, vector, action) {
  var dest = this.checkDestination(action, vector);
  if (dest == null || critter.energy <= 1 || this.grid.get(dest) != null) {
    return false;
  }

  critter.energy -= 1;
  this.grid.set(vector, null);
  this.grid.set(dest, critter);
  return true;
};
// The action checks whether it provides a valid destination using checkDestination
// If not or if it's an empty direction, or the critters lack enough energy, move
// returns false, otherwise it moves, subtracts energy cost and returns true.

// Let's add eating.
actionTypes.eat = function(critter, vector, action) {
  var dest = this.checkDestination(action,vector);
  var atDest = dest != null && this.grid.get(dest);

  if (!atDest || atDest.energy == null) {
    return false;
  }

  critter.energy += atDest.energy;
  this.grid.set(dest, null);
  return true;
};
// Eating a critter involves providing a valid destination, though this time it must
// not be empty and have something with energy, like a crittery, but not a wall. If
// so the energy from the eaten is transferred to the eater and the eaten is removed.

// Let's add reproduction.
actionTypes.reproduce = function(critter, vector, action) {
  var baby = elementFromChar(this.legend, critter.originChar);
  var dest = this.checkDestination(action, vector);

  if (dest == null || critter.energy <= 2 * baby.energy ||
                                this.grid.get(dest) != null) {
    return false;
  }

  critter.energy -= 2 * baby.energy;
  this.grid.set(dest, baby);
  return true;
};
// Reproducing costs twice the energy of the baby critter. So we create a baby using
// elementFromChar on the critter's own origin character. We then find the baby's
// energy level and test whether the parent has enough energy to reproduce and that
// there's a valid destination. If everything is okay, the baby is put on the grid
// and the energy is spent.

/// Populating the New World

// We've got a setup for simulating a more lifelike world. Let's create some new
// creatures to fill it. Let's start with plants.
function Plant() {
  this.energy = 3 + Math.random() * 4;
}

Plant.prototype.act = function(view) {
  if (this.energy > 15) {
    var space = view.find(" ");
    if (space) {
      return {type: "reproduce", direction: space};
    }
  }

  if (this.energy < 20) {
    return {type: "grow"};
  }
};
// Plants start with energy between 3 and 7, randomized to mix up reproduction timing
// when they hit 15 energy and there's an empty spot they reproduce there, if it can't
// reproduce it waits till it hits energy level 20.

// Now for a plant eater.
function PlantEater() {
  this.energy = 20;
}

PlantEater.prototype.act = function(view) {
  var space = view.find(" ");

  if (this.energy > 60 && space) {
    return {type: "reproduce", direction: space};
  }

  var plant = view.find("*");

  if (plant) {
    return {type: "eat", direction: plant};
  }

  if (space) {
    return {type: "move", direction: space};
  }
};
// We'll use the "*" character for plants so that's what this creature will look
// for when it searches for food.

/// Bringing it to Life

// We now have enough elements to try the new world. This map is a grassy valley
// with herbivores, and plant life.
var valley = new LifelikeWorld(
  ["############################",
   "#####                 ######",
   "##   ***                **##",
   "#   *##**         **  O  *##",
   "#    ***     O    ##**    *#",
   "#       O         ##***    #",
   "#                 ##**     #",
   "#   O       #*             #",
   "#*          #**       O    #",
   "#***        ##**    O    **#",
   "##****     ###***       *###",
   "############################"],
   {
     "#": Wall,
     "O": PlantEater,
     "*": Plant
   }
);
// So most of the time the plants multiply quickly, then the herbivore population
// explodes and they usually wipe themselves out by eating everything. Sometimes
// though the herbivores die out and the plants take over everything.

////////////////////////////////////////////////////////////////////////////////
/// Exercises
////////////////////////////////////////////////////////////////////////////////

/// Artificial stupidity

// Having the inhabitants of our world go extinct after a few minutes is kind of
// depressing. To deal with this, we could try to create a smarter plant eater.

// There are several obvious problems with our herbivores. First, they are
// terribly greedy, stuffing themselves with every plant they see until they
// have wiped out the local plant life. Second, their randomized movement
// (recall that the view.find method returns a random direction when multiple
// directions match) causes them to stumble around ineffectively and starve if
// there don’t happen to be any plants nearby. And finally, they breed very
// fast, which makes the cycles between abundance and famine quite intense.

// Write a new critter type that tries to address one or more of these points
// and substitute it for the old PlantEater type in the valley world. See how
// it fares. Tweak it some more if necessary.
function SmartPlantEater() {
  this.energy = 20;
  this.hunger = 0;
  this.dir = randomElement(directionNames)[0];
}

SmartPlantEater.prototype.act = function(view) {
  var space = view.find(" ");

  if (this.energy > 80 && space) {
    this.hunger += 1;
    return {type: "reproduce", direction: space};
  }

  var plant = view.find("*");

  if (plant && this.hunger > 1) {
    this.hunger = 0;
    return {type: "eat", direction: plant};
  }

  if (view.look(this.dir) == ' ') {
    this.hunger += 1;
    return {type: "move", direction: this.dir};
  } else {
    this.hunger += 1;
    this.dir = space
    return {type: "move", direction: space};
  }
};

/// Predators

// Any serious ecosystem has a food chain longer than a single link. Write
// another critter that survives by eating the herbivore critter. You’ll notice
// that stability is even harder to achieve now that there are cycles at multiple
// levels. Try to find a strategy to make the ecosystem run smoothly for at
// least a little while.

// One thing that will help is to make the world bigger. This way, local
// population booms or busts are less likely to wipe out a species entirely,
// and there is space for the relatively large prey population needed to sustain
// a small predator population.

function Tiger() {
  this.energy = 80;
  this.hunger = 0;
  this.dir = randomElement(directionNames)[0];
}

Tiger.prototype.act = function(view) {
  var space = view.find(" ");

  if (this.energy > 180 && space) {
    this.hunger += 1;
    return {type: "reproduce", direction: space};
  }

  var herbivore = view.find("O");

  if (herbivore && this.hunger > 5) {
    this.hunger = 0;
    return {type: "eat", direction: herbivore};
  } else if (herbivore && 0 < this.hunger < 5) {

    return {type: "move", direction: herbivore};
  }

  if (view.look(this.dir) == ' ') {
    this.hunger += 1;
    return {type: "move", direction: this.dir};
  } else {
    this.hunger += 1;
    this.dir = space;
    return {type: "move", direction: space};
  }
};
