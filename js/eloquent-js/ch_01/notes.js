/// Eloquent Javascript Chapter 01: Values, Types, and Operators - Notes

/// Values

// Six basic data types
// Numbers
// Strings
// Booleans
// Objects (not covered in chapter 1)
// Functions (not covered in chapter 1)
// Undefined

/// Numbers

// Javascript numbers are fixed to 64 bit max
// so you can have 2^64 digits (or 18 quintillion) but due to negatives
// and floats you can really have 9 quadrillion.
// Examples
var x = 13;
var y = 9.13;
var z = 2998e8;
console.log(x);
console.log(y);
console.log(z);

console.log(`100 + 4 * 11 = ${100 + 4 * 11}`);
console.log(`100 + 4 * 11 = ${(100 + 4) * 11}`);
// Available operators
// +, -, *, /, %
console.log(`314 % 100 = ${314 / 100}`);
console.log(`314 % 100 = ${314 % 100}`);
// Number values that aren't really numbers...
// Infinity and negative Infinity just represent an infinite or negatively infinite amount.
// NaN is Not a Number and is the result of any calculation that yields incalculable values.
// This like 0 / 0 or Infinity - Infinity
console.log(Infinity);
console.log(-Infinity);
console.log(NaN);

/// Strings

// Strings are written and enclosed in quotes (single '' or double "" [and in the case of template strings
// in backticks ``])
// Standard escape characters are also possible, like newlines and tabs.
console.log('This is a multiline\n\tstring with a back\\slash in it.');

console.log('wooo' + '!!!\n\t\tconcatenation.');

/// Unary operators

// Some operators are words and not just characters, like the typeof
// it will return the data type of a value.
// Unary operators mean the operate on one value, while binary operators (like + or *)
// operate on two.
console.log(typeof 4.5);

console.log(typeof 'x');

// The - operator can be both unary and binary.
console.log(-1);
console.log(`4 - 1 = ${4 - 1}`);

/// Booleans

// Javascript has the values 'true' and 'false' that represent between two possible
// values, like on or off.
// You can produce these with comparisons.

console.log(`3 > 2 = ${3 > 2}`);
console.log(`3 < 2 = ${3 < 2}`);
// Strings can be compared and their unicode value is used to determine their value per character.
// Other possible comparisons are == (equal to), != (not equal to),  <=, >=
// In addition there is also the strict version of === and !==

/// Then there are the logical operators, && (and), || (or) and ! (not)

// These can short circuit. Meaning in the case of and that if the first value evaluates
// to false it will stop are return false, and in the case of or if the first value
// is true it will return true.
console.log(`true && false = ${true && false}`);
console.log(`true && true = ${true && true}`);
console.log(`!true && true = ${!true && true}`);
console.log(`true || true = ${true || true}`);

console.log(`1 + 1 == 2 && 10 * 10 > 50 = ${1 + 1 == 2 && 10 * 10 > 50}`);

/// ternary operator

// this is the conditional operator. Written like condition ? true result : false result
console.log(`true ? 1 : 2 = ${true ? 1 : 2}`);
console.log(`false ? 1 : 2 = ${false ? 1 : 2}`);

/// Undefined

// There are two types of undefined values, `null` and `undefined` they are used
// to denote the absence of a meaningful value.
// The difference between the two is minimal and they can be mostly thought of as interchangable.

/// Automatic type conversion

// Javascript will try to automatically coerce values into useable data types. This may not
// work how you expect.

console.log(`8 * null = ${8 * null}`)
console.log(`"5" - 1 = ${"5" - 1}`)
console.log(`"5" + 1 = ${"5" + 1}`)
console.log(`"five" * 2 = ${"five" * 2}`)
console.log(`false == 0 = ${false == 0}`)
console.log(`null == undefined = ${null == undefined}`);
console.log(`null === undefined = ${null === undefined}`);
console.log(`null == 0 = ${null == 0}`);

/// Short-Circuiting of Logical Operators

// The logical operators && and || handle of different types in peculiar ways
// They convert the left hand item into a boolean but either return the original
// left-hand value or the right-hand value.
console.log(null || "user")
console.log("Karl" || "user")
