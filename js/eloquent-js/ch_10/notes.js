"use strict";
var fs = require('fs');
var defineCache = Object.create(null);
var currentMod = null;
// Eloquent Javascript Chapter 10: Modules - Notes

// Every program has a shape, on the small end this is division into functions
// and blocks. Shape follows more from programmer taste than the program's intended
// functionality.

// When looking at larger programs individual functions blend into the background
// but can be made more readable if we have a larger unit of organization.

// Modules divide programs into clusters of code that belong together by some criterion.

/// Why Modules Help

// With books there's a number of reasons to break them up into chapters. To help
// the reader see how the book is built and to help the author by adding a focus
// per section.

// For code the benefits are similar, it lets someone who isn't familiar with the
// code find things easily and makes it easy to keep related things together.

// Some programs are organized like text, this is called literate programming.
// This means there's a defined order the reader is encouraged to read the code
// with lots comments documenting the code. This makes the code less intimidating
// to read but more tightly coupled.

// Rule of thumb, structure costs energy. Start with no structure until the code
// stabilizes. Then organize based on what needs to stay together.

//// Namespacing

// Many programming languages have scope levels between global and local, JavaScript
// isn't one of them. Namespace polution can become a problem, and though there is
// no module construct yet in the language, you can use objects to create public
// subnamespaces and functions to create private namespaces in modules.

//// Reuse

// In a "flat" structure it's not apparent which parts of the code are needed for
// a particular function. If you want to reuse a function in another project you
// will need to copy and paste it to the other and duplicate work fixing issues.
// Once you reach this point you'll find a lot of time wasted trying to keep parts
// up to date. Putting pieces that stand on their own into separate files and modules
// makes them easier to track, update, and share because all the pieces that want
// to use the code load it from the same source.

// This idea gets even more powerful when you consider relations between modules
// and what modules can depend on other modules and explicitly state them. You
// can automate the process of installing and updating dependencies.

// Taking this idea further there's the online service NPM, which tracks and distributes
// modules, allowing you to search for the one you want, install it and keep it updated.

//// Decoupling

// An important role of modules is to isolate pieces of code from each other. A
// well designed module provides an interface for external code use. As the module
// gets updated the interface stays the same (it's stable) so other modules can use
// the new, improved version without changes.

// A stable interface though doesn't mean no new functions, methods, or variables
// are added. It just means that existing functionality isn't removed or changed.

// A good interface should allow the module to grow without breaking the old interface.
// This means exposing as few of the module's internals as possible while making
// the API powerful and flexible enough to be applicable in a wide range of situations.

// For some interfaces this comes easily, for others it becomes more complicated
// and requires careful design.

/// Using Functions as Namespaces

// Functions are the only thing in JavaScript that create new scopes, so for our
// modules to have their own scope we need to use functions. Here's an example
// that associates days of the week with numbers.
// var names= ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday",
//   "Saturday"];
//
// function dayName(number) {
//   return names[number];
// }
// console.log(dayName(1)); -> Monday
//
// The dayName function is part of the interface, but the names array isn't, it's
// best if it isn't global though so this is better:
//
//
var dayName = (function() {
  var names = [
    "Sunday",
    "Monday",
    "Tuesday",
    "Wednesday",
    "Thursday",
    "Friday",
    "Saturday"];

  return function(number) {
    return names[number];
  };
}());

console.log(dayName(3)); // -> Wednesday

// Now names is a local variable in an anonymous function, the function is created
// and called immediately and its return value the actual dayName function is stored
// in the variable. This code could be much more complex, but it'd all be internal
// to the module.

// We can use a similar pattern to isolate code entirely. This module logs something
// to console but doesn't provide any values for other modules to use.
(function() {
  function square(x) {return x * x };
  var hundred = 100;

  console.log(square(hundred));
}());// -> 10000

// This code simply prints 100 squared, but it could be the setup for a UI widget,
// or it could add a method to some prototype. It's wrapped in a function to prevent
// namespace polution.

// Why is it wrapped in parentheses? In JavaScript if an expression starts with
// the keyword function it's a function expression and can be immediately invoked,
// but if a statement begins with the keyword function it's a function declaration
// that requires a name and can't be immediately invoked. The extra wrapping
// parentheses are used to force it to be a function expression.

/// Objects as Interfaces

// Say we want to add another function that lets you retrieve the number given the
// weekDay to our API? We need to return an object now instead of the function.
var weekDay = (function() {
  var names = [
    "Sunday",
    "Monday",
    "Tuesday",
    "Wednesday",
    "Thursday",
    "Friday",
    "Saturday"];

  return {
    name(number) {
      return names[number];
    },
    number(name) {
      return names.indexOf(name);
    },
  };
}());

console.log(weekDay.name(weekDay.number("Sunday"))); // -> Sunday
// for bigger modules gathering all the exported values into an object at the
// end becomes awkward, an alternative is to declare an object (conventionally
// named exports) and add properties to that whenever we are defining something
// that needs to be exported. The following example the module function takes the
// interface object as an argument. allowing code outside the function to create
// and store it (outside functions "this" refers to the global scope object).
(function(exports) {
  var names = [
    "Sunday",
    "Monday",
    "Tuesday",
    "Wednesday",
    "Thursday",
    "Friday",
    "Saturday"];

  exports.name = function(name) {
    return names[number];
  };

  exports.number = function() {
    return names.indexOf(name);
  };
}(this.weekDay = {}));

console.log(weekDay.name(weekDay.number("Saturday"))); // -> Saturday

/// Detaching from the Global Scope

// The previous pattern is commonly used in browser code. The module claims a single
// global variable and wrap its code in a function to have its own namespace.
// This pattern still causes problems with name collision or multiple versions of
// the same module.

// We can create a system that allows one module to ask for the interface object of
// another without going through the global scope. Our goal is a `require(..)`
// function that when given a module name will load that module's file (from disk
// or web) and return the interface object.

// This solves the problem mentioned earlier with the added bonus of making your
// program's dependencies explicit, making it harder to accidentally use a module
// without stating you need it.

// We need two things for `require`, a readFile function, which returns the contents
// of a file as a string and second we need to be able to execute this string of code.

/// Evaluating Data as Code

// There's several ways to take data and execute it as part of the current program.
// The most obvious being the `eval` function, which executes strings of code in
// the current scope. This is usually bad because it breaks sane properties that
// scopes have like being isolated from the outer code.
//
// Example doesn't work in Node.js
//
// function evalAndReturnX(code) {
//   eval(code);
//   return x;
// }

// console.log(evalAndReturnX("var x = 2")); // -> 2
// A better way to interpret data as code is the Function constructor, it takes
// 2 strings as arguments, the first a string of comma separated arguments and the
// second containing the function body.
var plusOne = new Function('n', 'return n + 1;');
console.log(plusOne(4)); // -> 5
// This is what we need for our modules, we'll wrap module's code into a function,
// and the function scope will act as the module scope.

/// Require
function readFile(fileName) {
  try {
    return fs.readFileSync(`${fileName}.js`, 'utf8');
  } catch(e) {
    throw e;
  }
}
// Here's a minimal `require(..)`
function requireCustom(name) {
  var code = new Function('exports', readFile(name));
  var exports = {};
  code(exports);
  return exports;
}

console.log(requireCustom('weekDay').name(1)); // -> Monday
// The Function constructor wraps the module in a function, so it doesn't need a
// wrapping namespace function in the module file, and since exports is an argument
// to the module, it doesn't need to declare it, this declutters the example.

// When using this pattern, modules usually begin with variable declarations that
// load modules it depends on.
// var weekDay = require('weekDay');
// var today = require('today');

// The simple `require(..)` implementation has several problems. It will run and load
// modules every time its called, even if the module has been loaded before.

// This is solved by storing the loaded modules in an object and returning existing
// ones when it's loaded multiple times.

// Problem two is that its not possible for a module to directly export a value
// other than the exports object, like a module might want to only expose a constructor.
// But that isn't possible with require in its current state. The traditional
// solution to this is to provide modules another variable `module` an object with
// a property `exports`, initially set to the empty object created by require but
// can be overriden with a different value.
function requireCustomCache(name) {
  if (name in require.cache) {
    return require.cache[name];
  }

  var code = new Function('exports, module', readFile(name));
  var exports = {}, module = {exports};
  code(exports, module);

  require.cache[name] = module.exports;
  return module.exports;
}
requireCustomCache.cache = Object.create(null);

console.log(requireCustomCache('weekDay').name(2)); // -> Tuesday
// Now we have a module system with a single global variable `require` that allows
// modules to find and use each other without using global scope.

// This style is called CommonJS Modules based on the pseudostandard its based on
// It's built into the Node.js system and the real systems do a lot more like better
// path traversal and module relative traversal.

/// Slow-Loading Modules

// It's possible to use CommonJS in the browser, but it's not optimal. Reading a
// disk from the web is more expensive than on hard disk. When a script is running
// nothing else can happen on a website, so if every require call fetched from high
// ping servers the page would freeze for a long time to load the scripts.

// A workaround is projects like Browserify which take all your require calls and
// collect the code beforehand in a large bundled file that the site can load.

// Another solution is wrapping up your module code into a function so a module
// loader could load its dependencies in the background, call the function, initializing
// the module when the dependencies have loaded. This is what the Asynchronous Module
// Definition (AMD) system does.

// our Program would look like this.
define(['weekDayAmd', 'today'], function(weekDay, today) {
  console.log(weekDay.name(today.dayNumber()));
});
// The define function is central to this system, it takes an array of module names
// and a function with an argument for each module, it loads the dependencies (if they aren't
// already loaded) in the background, letting us keep working. Once they're all loaded
// define calls the function it was given with the interfaces of those dependencies
// as arguments.

// Modules loaded this way must also utilize define, the value used for their interface
// is whatever was returned by the function passed to define.

// To keep track of modules while they're loaded, define will use objects that
// describe the state of modules, telling us their availability status, and providing
// their interface when ready.

// getModule(..) when given a name will return an object and ensure the module is scheduled
// to be loaded, using a cache object to avoid loading the same module twice.
defineCache = Object.create(null);
currentMod = null;

function backgroundReadFile(fileName, callback) {
  fs.readFile(`${fileName}.js`, 'utf8', function(err, content) {
    if (!err)
      return callback(content);

    console.log(err);
    throw err;
  });
}

function getModule(name) {
  if (name in defineCache) {
    return defineCache[name];
  }

  var module = {
    exports: null,
    loaded: false,
    onLoad: [],
  };

  defineCache[name] = module;
  backgroundReadFile(name, function(code) {
    currentMod = module;
    new Function('define', code)(define);
  });
  return module;
}
// Assuming the loaded file contains a single call to define. The currentMod variable
// is used to tell this call about the module object being loaded so it can update
// the object when loaded.

// define(..) uses getModule(..) to fetch or create module objects for the current
// module's dependencies. It schedules the moduleFunction to be run whenever those
// dependencies are loaded. To do this it defines a function whenDepsLoaded(..)
// that's added to the onLoad array of all dependencies that aren't loaded yet.
// The function immediately returns if there's still unloaded dependencies, so it
// only does the work once when the last dependency has loaded, and is called immediately
// within define(..) in case no dependencies need to be loaded.
function define(depNames, moduleFunction) {
  var myMod = currentMod;
  var deps = depNames.map(getModule);

  deps.forEach(function(mod) {
    if (!mod.loaded) {
      mod.onLoad.push(whenDepsLoaded)
    }
  });

  function whenDepsLoaded() {
    if (!deps.every(m => m.loaded)) {
      return;
    }

    var args = deps.map(m => m.exports);
    var exports = moduleFunction.apply(null, args);
    if (myMod) {
      myMod.exports = exports;
      myMod.loaded = true;
      myMod.onLoad.forEach(f => f());
    }
  }

  whenDepsLoaded();
}
// when all dependencies are loaded whenDepsLoaded(..) calls the function holding
// the module giving it the dependencies' interfaces as arguments.

// define(..) first stores the value that currentMod had when it was called in a
// variable myMod. With getModule(..) storing the corresponding module object in
// currentMod. This lets whenDepsLoaded(..) to store the return value of the module
// function in that module's exports property, set its loaded property to true, and
// call all functions waiting for the module to load.

// This code is harder to follow than require(..), its execution isn't straightforward
// and instead operations are set to happen at unspecified times in the future,
// obscuring the way the code executes.

// A real AMD implementation would do more clever things for resolving URLs and is
// more robust and bug proof. The RequireJS project provides a popular implementation.

/// Interface Design

// Designing interfaces for modules and object types is one of the subtler aspects
// of programming. Anything nontrivial can be modeled in multiple ways, finding a
// way that works well requires insight and foresight.

// The best way to learn good interface design is to use lots of interfaces, both
// good and bad. Experience will teach you what does and doesn't work. Never assume
// a bad interface is just the way it is, fix it or wrap it in a new interface that
// works better for you.

//// Predictablity

// If you can predict the way your interface works, you won't have to look up how
// to use it as often. Follow conventions. When another module or part of Javascript
// does something similar to what you're implementing, it's a good idea to make your
// interface resemble the existing interface so that it's familiar to users of the
// existing interface.

// Predictability is also important in the actual behavior of your program. It's
// tempting to make an unnecessarily clever interface with the justification that
// it's more convenient. You could accept all kinds of data types and do the
// "right thing" for all of them. Or you could provide many specialized functions
// that provide different flavors of functionality. These might make code that
// builds on your interface shorter but harder for people to reason about its behavior.

//// Composability

// Try using the simplest data structures possible in your interfaces and make
// functions do a single clear thing. Make them pure functions wherever practical.

// It's not uncommon for modules to provide their own array like collection object,
// with their own interfaces for counting and extracting elements. These objects won't
// have map(..) or forEach(..) methods, and any function that expects an array won't
// be able to work with them. This is an example of poor composability, the module
// can't be easily composed with other code.

//// Layered Interfaces

// When designing a complex interface, sending email for example, there is a dilemma.
// On one hand you don't want to overload users with details. They shouldn't need
// to study the interface for 20 mins to send an email, but on the other hand you
// don't want to hide all the details either, when people need to do complicated
// things with your module it should be doable.

// A common solution is to provide two interfaces, a high level interface and a low
// level interface. The high level can be simple and the low level detailed. Usually
// the high level can be built with the low level.

/// Summary

// Modules give structure to bigger programs by breaking code up into files and
// namespaces. Adding well defined interfaces to modules makes them easily reusable.

// JavaScript isn't too helpful at creating module systems, but functions and objects
// make it possible to define module systems. Function scopes can be used as internal
// namespaces for the module and objects can be used for storing sets of exported
// values.

// There's two popular module approaches in JavaScript, CommonJS modules, which
// revolve around a require function that fetches modules. The other is called
// AMD and uses a define function that takes an array of module names and a function
// then loads the modules and runs the functions with their interfaces as arguments.
