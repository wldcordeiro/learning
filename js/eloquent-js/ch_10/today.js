define([], function() {
  var today = new Date();

  return {
    dayNumber() { return today.getDay(); },
  }
});
