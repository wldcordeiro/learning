/// Eloquent Javascript Chapter 02: Program Structure - Notes

/// Expressions and Statements

// A fragment of code that produces a value is an expression.
// Expressions can nest and chain together to form longer expressions.
// An expression is to a sentence fragment, what a statement is to a sentence.
// A program is a list of statements.
// Simplest kinds of statements are expressions with a semicolon.
// Examples:
// 1;
// !false;
// An expression can be content to produce a value, a statement stands on its own
// and changes something, either directly or as a side-effect.
// Semi-colons aren't always needed at the end of statements, but the rules for when
// they aren't needed are error-prone.

/// Variables

// To create and hold values you use variables.
// In Javascript you create a variable with
// var caught = 5* 5;
console.log("Variables Example");
var caught = 5* 5;
console.log(caught);
// The keyword `var` indicates that we are declaring a variable.
// You follow it with the name you are assigning.
// If you want to give it a value you follow that with an equal sign and the value.
// Otherwise you can just do: `var x;` to have a undefined variable.
var ten = 10;
console.log(ten * ten);
// Variable names can be any word that isn't a reserved keyword.
// In addition they can not contain spaces, may not begin with a number
// can't contain punctuation with the exception of $ or _
// Variables can be rebound, meaning that if they're bound to one value now.
// They can be bound to another later.
// Example:
var mood = "light";
console.log(mood);
mood = "dark";
console.log(mood);
// Variables don't contain values, they only point to them, so you can rebind
// variables without destroying values (unless nothing else points to that value)
// then the garbage collector will do its thing.
// Example:
var luigisDebt = 140;
luigisDebt = luigisDebt - 35;
console.log(luigisDebt);
// var can be used to define multiple variables at once, they must be comma-separated though.
var one = 1, two = 2;
console.log(one + two);

/// Keywords and Reserved Words

// Words with special meaning to Javascript are called keywords there are also
// reserved words which currently don't have special meaning but may be used in
// the future by Javascript.
// List of Keywords:
// break
// case
// catch
// class
// const
// continue
// debugger
// default
// delete
// do
// else
// enum
// export
// extends
// false
// finally
// for
// function
// if
// implements
// import
// in
// instanceof
// interface
// let
// new
// null
// package
// private
// protected
// public
// return
// static
// super
// switch
// this
// throw
// true
// try
// typeof
// var
// void
// while
// with
// yield

/// The Environment

// the collection of variables and values that exist is called an environment
// at startup the environment isn't empty, it always contains variables that
// are part of the language standard and most of the time has variables that allow
// you to interact with the system. For example, in a browser, there are variables
// and functions to inspect and influence the currently loaded website and to
// read mouse and keyboard input.

/// Functions

// A function is a piece of program wrapped in a value, such values can be applied
// in order to run the wrapped code. In the browser for example there is the
// alert function that shows a dialog box with a message.
// alert("Good morning?!");
// ^ Doesn't work in node/io since it isn't the browser.
// Executing a function is called invoking, calling or applying
// You can call a function by putting parantheses after an expression that produces
// a function value. Usually that will be the variable name of the function.
// The values between the parantheses will be passed to the function and are called
// arguments and functions can have any number of arguments with different data types.

/// Console.log

// console.log is present in most Javascript systems, such as browsers or Node.js
// It is a better way to output some value than alert because it is not a focus stealing
// operation and is not system specific (alert isn't available in Node.js).
// Though variable names can't have periods in them, console.log isn't just a variable
// it's a reference to the log property on the console object.

/// Return Values

// Showing a dialog box or writing to console is a side effect. Lots of functions
// are useful because of their side-effects, but functions can also produce (or
// in programmer terminology return) values rather than have side-effects.
// For example, Math.max will take a set of number values and return the largest
console.log(Math.max(2, 4));
// Anything that produces a value is an expression in Javascript which means
// function calls can be used within larger expressions. For example:
console.log(Math.min(2, 4) + 100);

/// Prompt and Confirm

// In browsers there are other functions that produce dialog boxes.
// You can ask a user for an "Ok" or "Cancel" using confirm which returns
// a Boolean, true for OK and false for Cancel.
// confirm("Shall we, then?");

// The prompt function can be used for "open" questions. It takes two arguments
// the question, and the starting value. The use can then fill in the text entry
// field and the result of that will be returned by the function.
// prompt('Tell me everything you know.', '...');

// These functions are rarely used in modern development but may be useful
// during development or for toy apps.

/// Control Flow

// When a program contains more than one statement, statements are executed sequentially
// line by line. A simple Example:
// var theNumber = Number(prompt("Pick a number", ""));
// alert(`Your number is the square root of ${theNumber * theNumber}`);
// The first line asks for a number and converts it from a String to a Number type
// and binds it to the theNumber variable.
// The next line uses that variable to output a string with its square.
// This is an example of a linear control flow.

/// Conditional Execution

// But linear control flow isn't the only option we have, there is also conditional
// execution. This is where we choose from different routes based on boolean values.
// We have conditional execution through the if/else if/else keyword set.
// For example:
// var theNumber = Number(prompt("Pick a number", ""));
// if (!isNaN(theNumber))
//   alert(`Your number is the square root of ${theNumber * theNumber}`);
// Here we only print out the square of theNumber if the value is a number,
// we use the built-in isNaN function that returns true if something is not a number
// and we use the not operator to flip the result, thus our statement reads
// if theNumber is a number -> print square.
// Using else:
// var theNumber = Number(prompt("Pick a number", ""));
// if (!isNaN(theNumber))
//   alert(`Your number is the square root of ${theNumber * theNumber}`);
// else
//   alert("Hey why don't you give me a number!?");
// Using else if
// var num = Number(prompt("Pick a number" "0"));

// if (num < 10)
//   alert("Small");
// else if (num < 100)
//   alert("Medium");
// else
//   alert("Large");

/// While and Do Loops

// Consider a program like
// console.log(0);
// console.log(2);
// console.log(4);
// console.log(6);
// console.log(8);
// console.log(10);
// console.log(12);
// You could do something like this, but programming is about automating tedium
// This is where loops come in.
// Looping allows us to return to a point in the program and repeat work with our
// current program state. Rewriting the previous example to use a while loop, it
// looks like
var number = 0;
console.log("While Loop Example 1");
while (number <= 12) {
  console.log(number);
  number = number + 2;
}
// A statement that starts with while is a while loop. You follow the while keyword
// with a conditional that evaluates to true and then you can wrap
// the body in braces (if the body is a single line this isn't necessary) that should
// be executed each time the loop is run.
// Once the conditional stops evaluating to true, the loop completes.
// Here's another example that calculates and shows the result of 2^10
var result = 1;
var counter = 0;
console.log("While Loop Example 2");
while (counter < 10) {
  result = result * 2;
  counter = counter + 1;
}
console.log(result);
// Another similar loop to while is the do/while loop. It executes its body at least once
// and then begins checking its conditional.
// do {
//   var name = prompt("Who are you?");
// } while (!name);
// console.log(name);
// This program will ask you for a name, and will continue to prompt until something
// other than an empty string is entered.

/// Indenting Code

// Javascript does not have meaningful whitespace, to the JS Engine everything could
// be one long line, however you can use indentation to make your code more readable
// for yourself and others... and you should. Seriously!
// The indentation will make the structure of the code more obvious to someone reading
// it, so there are obvious benefits when code becomes complex.

/// For Loops

// Many loops follow the pattern shown, where you use a counter variable and test
// that it hasn't reached a threshold, because of this. There is another loop type
// called the for loop.
// Example:
console.log("For loop example");
for (var i = 0; i <= 24; i = i + 2) {
  console.log(i);
}
// The parantheses after the for keyword must contain two semicolons. The part
// before the first semicolon initializes the for loop, usually by defining a counter
// variable. The second part is the expression that checks whether the loop should continue.
// The final part updates the state of the loop after each iteration.

/// Breaking out of a Loop

// Having a loop's condition produce false is not the only way to end a loop.
// There is a special keyword called break that will immediately terminate the
// enclosing loop.
// Example:
console.log("Break Statement Example");
for (var current = 20; ; current++) {
  if (current % 7 == 0)
    break
}
console.log(current);
// The modulus operator is an easy way to test number divisibility by testing for
// if the result is 0.
// The for loop doesn't check for an end of loop, this means it will loop indefinitely
// if the break statement isn't present. If you left it out or had a condition that
// was always true in a loop, you would produce an infinite loop, which is usually a bad thing.
// The continue keyword is similar to break, in that they both influence loop progress.
// When continue is used in a loop, the loop jumps out of the body and to the next iteration.

/// Updating Variables Succintly

// There are ways to update a variable's value in a shorthand manner.
// This
// counter = counter + 1;
// Can be
// counter += 1;
// You can also do this with other operators.
// *= (multiply equal)
// -= (subtract equal)
// /= (divide equal)
// %= (modulus equal)
// ++ (increment by one)
// -- (decrement by one)
// Examples:
console.log("Assignment Shortcut operator examples");
var x = 10, y = 20, z = 30, a = 40;

console.log(y += 2);
console.log(x -= 2);
console.log(z *= 2);
console.log(a /= 2);
console.log(a %= 2);

/// Dispatching on a Value with Switch

// A common occurence is code like this.
// if (variable == "value1") action1();
// else if (variable == "value2") action2();
// else if (variable == "value3") action3();
// else defaultAction();
// There's a construct in Javascript called switch for this kind of pattern.
// However, it looks kind of awkward since it inherited this from C/Java
// Example:
// switch (prompt("What is the weather like?")) {
//   case "rainy":
//     console.log("Remember to bring an umbrella.");
//     break;
//   case "sunny":
//     console.log("Dress lightly.");
//   case "cloudy":
//     console.log("Go outside.");
//     break;
//   default:
//     console.log("Unknown weather type!");
//     break;
// }
// You may have any number of case statements and the program will jump to the matching
// one, if it exhausts all its options, it will use the default case. Note, though
// it will start executing the statements there and any after it if it doesn't
// run into a break statement. This can be useful, if multiple cases can be
// true, but it's worth being aware of since code can be executed that you didn't
// intend to execute.

/// Capitalization

// When it comes to naming variables with multiple words there are a few different styles.
// fuzzylittleturtle
// fuzzy_little_turtle
// FuzzyLittleTurtle
// fuzzyLittleTurtle
// The bottom of these is the standard way to do things in Javascript so
// it is best to follow the custom and name things in this style.

/// Comments

// Code alone does not always convey all the information necessary to understand it.
// This is where comments come in.
// Javascript has two types of comments. Inline (which this file uses) that is
// done by placing // at some point of a line or block comments.
/*
  Which look like this. You prefix them with /*
  then to end you use * / (with no space between.)
*/

/// Summary

// Programs are built out of statements, which can contain other statements.
// Statements usually containt expressions, which can also contain other expressions.
// Putting statements one after another produces a linear control flow.
// You can change this control flow by introducing conditionals or loops.
// Variables are useful for attaching a name to a value and can be used to track
// program state. The environment is the set of variables currently defined, including
// all built-in names that come with JavaScript.
// Fuctions are special values that encapsulate a piece of program. You can invoke them
// like so `myFunction(argument1, argument2);` A function call is an expression and
// may produce values.
