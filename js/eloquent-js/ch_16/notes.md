# Eloquent JavaScript Chapter 16: Drawing On Canvas - Notes

There are several ways to display graphics in a browser, the first and simplest is styling DOM elements. This can get you pretty far like the game from last chapter but we're using the DOM for something it wasn't really intended for and some tasks can be very awkward to perform this way such as drawing a line between two points.

There are two main alternatives. The first is DOM based but utilizes SVGs (Scalable Vector Graphics) rather than HTML elements. SVG is a dialect for describing documents focused on shapes instead of text. You can embed SVG documents in HTML or use it in `<img>` tags.

The next alternative are `canvas` elements. `canvas` elements are single DOM nodes that encapsulate a picture and provides an API for drawing on the space taken by the node. The main difference between SVG and `canvas` is that in SVG the original description is preserved so they can be moved or resized whenever. `canvas` on the other hand converts the shapes to pixels as soon as they're drawn and doesn't remember what the pixels represent. The only way to move a shape on a `canvas` is to clear the `canvas` or area the shape takes up and redraw it elsewhere.

## SVG

This book ins't going to cover SVG in depth, we'll cover the basics and the trade-offs of using it and other drawing mechanisms in the end of the chapter.

Here's an HTML document with a simple SVG image within:

```html
<p>Normal HTML Here.</p>
<svg xmlns="http://www.w3.org/2000/svg">
    <circle r="50" cx="50" cy="50" fill="red"/>
    <rect x="150" y="5" width="90" height="90" stroke="blue" fill="none"/>
</svg>
```

The `xmlns` attribute changes the XML namespace of an element and its children. The name space (identified by the URL) specifies the XML dialect to use. The `<circle>` and `<rect>` tags that don't exist in HTML have meaning in SVG and draw shapes based on their attributes.

These tags create DOM elements just like HTML elements do. Here we change our `<circle>` to cyan instead.

```javascript
const circle = document.querySelector('circle');
circle.setAttribute('fill', 'cyan')
```

## The Canvas Element

Canvas graphics go in `<canvas>` elements that you can set a height and width on. They start out empty and transparent taking up empty space. The `<canvas>` element supports different drawing styles. There are two types currently, 2d and 3d (webgl) graphics. To access either we must create a context object that provides the drawing API.

We won't cover WebGL and will stick the two dimensions here. Look into WebGL it provides a a direct interface to graphics hardware that lets you render complicated scenes efficiently in JavaScript.

Create contexts by calling `getContext` on a `<canvas>`

```html
<p>Before canvas.</p>
<canvas width="120" height="60"/>
<p>After canvas.</p>
<script>
  const canvas = document.querySelector('canvas');
  const context = canvas.getContext('2d');
  context.fillStyle = "red";
  context.fillRect(10, 10, 100, 50);
</script>
```

We create a context context object and the we create a 100x50 red rectangle with its top left corner in the (10, 10) coordinates.

## Filling and Stroking



```html
<canvas></canvas>
<script>
  const cx = document.querySelector('canvas').getContext('2d');
  cx.strokeStyle = "blue";
  cx.strokeRect(5, 5, 50, 50);
  cx.lineWidth = 5;
  cx.strokeRect(135, 5, 50, 50);
</script>
```



## Paths



```html
<canvas></canvas>
<script>
  const cx = document.querySelector('canvas').getContext('2d');
  cx.beginPath();
  for (let y = 10; y < 100; y += 10) {
    cx.moveTo(10, y);
    cx.lineTo(90, y);
  }
  cx.stroke();
</script>
```



```html
<canvas></canvas>
<script>
  const cx = document.querySelector('canvas').getContext('2d');
  cx.beginPath();
  cx.moveTo(50, 10);
  cx.lineTo(10, 70);
  cx.lineTo(90, 70);
  cx.fill();
</script>
```



## Curves



```html
<canvas></canvas>
<script>
  const cx = document.querySelector('canvas').getContext('2d');
  cx.beginPath();
  cx.moveTo(10, 90);
  // control = (60, 10) goal = (90, 90)
  cx.quadraticCurveTo(60, 10, 90, 90);
  cx.lineTo(60, 10);
  cx.closePath();
  cx.stroke();
</script>
```



```html
<canvas></canvas>
<script>
  const cx = document.querySelector('canvas').getContext('2d');
  cx.beginPath();
  cx.moveTo(10, 90);
  // control1 = (10, 10) control2 = (90, 10) goal = (50, 90)
  cx.bezierCurveTo(10, 10, 90, 10, 50, 90);
  cx.lineTo(90, 10);
  cx.lineTo(10, 10);
  cx.closePath();
  cx.stroke();
</script>
```



```html
<canvas></canvas>
<script>
  const cx = document.querySelector('canvas').getContext('2d');
  cx.beginPath();
  cx.moveTo(10, 10);
  // control = (90, 10) goal = (90, 90) radius = 20
  cx.arcTo(90, 10, 90, 90, 20);
  cx.moveTo(10, 10);
  // control = (90, 10) goal = (90, 90) radius = 80
  cx.arcTo(90, 10, 90, 90, 80);
  cx.stroke();
</script>
```



```html
<canvas></canvas>
<script>
  const cx = document.querySelector('canvas').getContext('2d');
  cx.beginPath();
  // center = (50, 50) radius = 40 angle = 0 to 7
  cx.arc(50, 50, 40, 0, 7);
  // center = (150, 50) radius = 40 angle = 0 to 1/2PI
  cx.arc(150, 50, 40, 0, 0.5 * Math.PI);
  cx.stroke();
</script>
```



## Drawing a Pie Chart



```javascript
const results = [
  { name: 'Satisfied', count: 1043, color: 'lightblue' },
  { name: 'Neutral', count: 563, color: 'lightgreen' },
  { name: 'Unsatisfied', count: 510, color: 'pink' },
  { name: 'No comment', count: 175, color: 'silver' },
];
```



```html
<canvas width="200" height="200"></canvas>
<script>
  const cx = document.querySelector('canvas').getContext('2d');
  const total = results.reduce((sum, choice) => sum + choice.count, 0);

  // Start at the top
  let currentAngle = -0.5 * Math.PI;
  results.forEach(result => {
    const sliceAngle = (result.count / total) * 2 * Math.PI;
    cx.beginPath();
    // center=100,100, radius=100
    // from current angle, clockwise by slice's angle
    cx.arc(100, 100, 100, currentAngle, currentAngle + sliceAngle);
    currentAngle += sliceAngle;
    cx.lineTo(100, 100);
    cx.fillStyle = result.color;
    cx.fill();
  });
</script>
```



## Text



```html
<canvas></canvas>
<script>
  const cx = document.querySelector('canvas').getContext('2d');
  cx.font = '28px Georgia';
  cx.fillStyle = 'fuchsia';
  cx.fillText('I can draw text, too!', 10, 50);
</script>
```



## Images



```html
<canvas></canvas>
<script>
  const cx = document.querySelector('canvas').getContext('2d');
  const img = document.createElement('img');
  img.src = '../assets/hat.png'
  img.addEventListener('load', () => {
    for (let x = 10; x < 200; x += 30) {
      cx.drawImage(img, x, 10);
    }
  });
</script>
```



```html
<canvas></canvas>
<script>
  const cx = document.querySelector('canvas').getContext('2d');
  const img = document.createElement('img');
  const spriteW = 24;
  const spriteH = 30;
  img.src = '../assets/player.png'
  img.addEventListener('load', () => {
    let cycle = 0;
    setInterval(() => {
      cx.clearRect(0, 0, spriteW, spriteH);
      cx.drawImage(
        img,
        // Source rectangle
        cycle * spriteW, 0, spriteW, spriteH,
        // Destination rectangle
        0, 0, spriteW, spriteH
      );
      cycle = (cycle + 1) % 8;
    }, 120);
  });
</script>
```



## Transformation



```html
<canvas></canvas>
<script>
  const cx = document.querySelector('canvas').getContext('2d');
  cx.scale(3, .5);
  cx.beginPath();
  cx.arc(50, 50, 40, 0, 7);
  cx.lineWidth = 3;
  cx.stroke();
</script>
```



![Transformations & Translations](assets/transform.svg)



```javascript
function flipHorizontally(context, around) {
  context.translate(around, 0);
  context.scale(-1, 1);
  context.translate(-around, 0);
}
```



![Mirroring an Image](assets/mirror.svg)



```html
<canvas></canvas>
<script>
  const cx = document.querySelector('canvas').getContext('2d');
  const img = document.createElement('img');
  const spriteW = 24;
  const spriteH = 30;
  img.src = '../assets/player.png';
  img.addEventListener('load', () => {
    flipHorizontally(cx, 100 + spriteW / 2);
    cx.drawImage(img, 0, 0, spriteW, spriteH, 100, 0, spriteW, spriteH);
  });
</script>
```

## Storing and Clearing Transformations



```html
<canvas width="600" height="300"></canvas>
<script>
  const cx = document.querySelector('canvas').getContext('2d');
  function branch(length, angle, scale) {
    cx.fillRect(0, 0, 1, length);
    if (length < 8) {
        return;
    }

    cx.save();
    cx.translate(0, length);
    cx.rotate(-angle);
    branch(length * scale, angle, scale);
    cx.rotate(2 * angle);
    branch(length * scale, angle, scale);
    cx.restore();
  }
  cx.translate(300, 0);
  branch(60, 0.5, 0.8);
</script>
```



## Back to the Game



```javascript
function CanvasDisplay(parent, level) {
  this.canvas = document.createElement('canvas');
  this.canvas.width = Math.min(600, level.width * scale);
  this.canvas.height = Math.min(450, level.height * scale);
  parent.appendChild(this.canvas);
  this.cx = this.canvas.getContext('2d');

  this.level = level;
  this.animationTime = 0;
  this.flipPlayer = false;

  this.viewport = {
    top: 0,
    left: 0,
    width: this.canvas.width / scale,
    height: this.canvas.height / scale,
  };

  this.drawFrame(0);
}
```



```javascript
CanvasDisplay.prototype.clear = function() {
  this.canvas.parentNode.removeChild(this.canvas);
}
```



```javascript
CanvasDisplay.prototype.drawFrame = function() {
  this.animationTime += step;
  this.updateViewport();
  this.clearDisplay();
  this.drawBackground();
  this.drawActors();
}
```



```javascript
CanvasDisplay.prototype.updateViewport = function() {
  const view = this.viewport;
  const margin = view.width / 3;
  const player = this.level.player;
  const center = player.pos.plus(player.size.times(0.5));

  if (center.x < view.left + margin) {
    view.left = Math.max(center.x - margin, 0);
  } else if (center.x > view.left + view.width - margin) {
    view.left = Math.min(
      center.x + margin - view.width,
      this.level.width - view.width
    );
  }

  if (center.y < view.top + margin) {
    view.top = Math.max(center.y - margin, 0);
  } else if (center.y > view.top + view.height - margin) {
    view.top = Math.min(
      center.y + margin - view.height,
      this.level.height - view.height
    );
  }
}
```



```javascript
CanvasDisplay.prototype.clearDisplay = function() {
  switch (this.level.status) {
    case 'won':
      this.cx.fillStyle = 'rgb(68, 191, 255)';
      break;
    case 'lost':
      this.cx.fillStyle = 'rgb(44, 136, 214)';
      break;
    default:
      this.cx.fillStyle = 'rgb(52, 166, 251)';
      break;
  }
  this.cx.fillRect(0, 0, this.canvas.width, this.canvas.height);
}
```



```javascript
CanvasDisplay.prototype.drawBackground = function() {
  const view = this.viewport;
  const xStart = Math.floor(view.left);
  const xEnd = Math.ceil(view.left + view.width);
  const yStart = Math.floor(view.top);
  const yEnd = Math.ceil(view.top + view.height);

  for (let y = yStart; y < yEnd; y++) {
    for (let x = xStart; x < xEnd; x++) {
      const tile = this.level.grid[y][x];
      if (tile == null) {
        continue;
      }
      const screenX = (x - view.left) * scale;
      const screenY = (y - view.top) * scale;
      const tileX = tile == 'lava' ? scale : 0;
      this.cx.drawImage(
        otherSprites,
        tileX,
        0,
        scale,
        scale,
        screenX,
        screenY,
        scale,
        scale
      );
    }
  }
}
```



```javascript
CanvasDisplay.prototype.drawPlayer = function(x, y, width, height) {
  let sprite = 8;
  const player = this.level.player;
  width += playerXOverlap * 2;
  x -= playerXOverlap;

  if (player.speed.x != 0) {
    this.flipPlayer = player.speed.x < 0;
  }

  if (player.speed.y != 0) {
    sprite = 9;
  } else if (player.speed.x != 0) {
    sprite = Math.floor(this.animationTime * 12) % 8;
  }

  this.cx.save();

  if (this.flipPlayer) {
    flipHorizontally(this.cx, x + width / 2);
  }

  this.cx.drawImage(
    playerSprites,
    sprite * width,
    0,
    width,
    height,
    x,
    y,
    width,
    height
  );
  this.cx.restore();
}
```



```javascript
CanvasDisplay.prototype.drawActors = function() {
  this.level.actors.forEach(actor => {
    const width = actor.size.x * scale;
    const height = actor.size.y * scale;
    const x = (actor.pos.x - this.viewport.left) * scale;
    const y = (actor.pos.y - this.viewport.top) * scale;

    if (actor.type == 'player') {
      this.drawPlayer(x, y, width, height);
    } else {
      const tileX = (actor.type == 'coin' ? 2 : 1) * scale;
      this.cx.drawImage(
        otherSprites,
        tileX,
        0,
        width,
        height,
        x,
        y,
        width,
        height
      );
    }
  }, this);
}
```



## Choosing a Graphics Interface



## Summary
