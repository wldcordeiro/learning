# Eloquent JavaScript Chapter 13: The Document Object Model - Notes

When you open a page in a browser, the browser parses the HTML, builds up a model of the document structure and uses the model to render the page to the screen. This representation of the document is one of the tools in JavaScript's arsenal, you can read from and change the model. It's a live data structure and modifications to it appear on screen in the browser.

## Document Structure

Think of HTML documents of as nested boxes. Tags enclose one another or text. Here's an example document.

```html
<!DOCTYPE html>
<html>
    <head>
        <title>My Home Page</title>
    </head>
    <body>
        <h1>My Home Page</h1>
        <p>Hello, I am Marijn and this is my home page</p>
        <p>I also wrote a book! Read it
            <a href="http://eloquentjavascript.net">here</a>.</p>
    </body>
</html>
```

The page has this structure.

![Basic HTML Structure](assets/html-boxes.svg)

The data structure works like this, for each box, there is an object, which we can interact with to find out stuff, like it's HTML tag and what children it contains. This representation is the Document Object Model or DOM.

The global variable `document` gives us access to the current document's DOM, its `documentElement` property refers to the object representing the `<html>` tag and provides, `head` and `body` properties for convenient access to those elements.

## Trees

Remember syntax trees from chapter 11, the structure there is really similar to a browser document. Each node may refer to other nodes, "children" which may have children as well. This shape is typical of nested structures where elements can contain nested sub-elements.

We call a data structure a tree when it has a branching structure with no cycles (meaning nodes can't contain themselves), with a single well defined "root", in the case of the DOM that is `document.documentElement`.

Trees come up a lot in Computer Science, aside from representing recursive data structures like HTML documents or programs, they're often used for maintaining sorted data because elements can be found more easily than in a flat array.

Typical trees will have different node types, the Egg language had *variable*, *value*, and *application* node. Applcation nodes always had children, whereas variables and values were leaves (childless nodes.)

The same is true in the DOM, nodes for regular HTML elements determine the structure of the document. These can have child nodes, an example of this type of node is `document.body`. Some of the children can be leafs, like text nodes or comments (HTML comments look like this `<!-- comment here -->`.)

Every DOM node object has a `nodeType` property that returns an integer representing the type of node. Regular elements are 1, or you can use the constant `document.ELEMENT_NODE`, text nodes are 3 or the constant `document.TEXT_NODE`, and comments are 8 and `document.COMMENT_NODE` respectively.

Here's another way to visualize our document tree. The leaves (green) are text nodes, and the arrows indicate parent/child relationships.

![HTML Document Tree Visual](assets/html-tree.svg)

## The Standard

Representing node types with a number is odd and not like the rest of JavaScript, we'll see more parts of the DOM interface that are out of place. The reason for this is that the DOM wasn't designed with just JavaScript in mind, it tried being languaged neutral, so it can be used with other formats like XML. This is kind of shitty, having a common standard is great, provided it isn't awkward when embedded in something. But that isn't the case here, and we would save more time with an interface that's designed with its user in mind.

An example of this bad mapping is the `childNodes` property, on first glance you would think it's an array, it has a length property and you can access its elements with indexes. But it's not an array it's a `NodeList` and has none of the methods like `slice`, `forEach` and others.

Throw in some design issues like not being able to create a node with children or attributes at the same time. You have to instead create the node, then each individual child and finally set attributes using side effects. This isn't a show stopper though, the flaws in the DOM APIs can be abstracted out in JavaScript and we can express the operations we need to perform in terser, clearer, code. There already are many libraries that do just this in the JavaScript ecosystem.

## Moving Through The Tree

DOM nodes have links to other nodes, here's a diagram of the example HTML

![DOM Node Linkage](assets/html-links.svg)

It doesn't show it in the diagram but every element node (type 1) has a `parentNode` and `childNodes` property, the former being a link to the containing node and the latter being an array like object of the children.

You could move around the DOM using just these parent and child links but that's tedious. There's `firstChild` and `lastChild` that point to their respective values or `null` if the element lacks children. There's also `previousSibling` and `nextSibling` that point to the adjacent nodes with the same parent, first children will have a `null` value for their `previousSibling` property and last children for their `nextSibling` property.

Recursive functions often help for dealing with nested data structures, this one scans a document for text nodes containing a certain string and returns true whe it's found one. The `nodeValue` property of text nodes refer to the string they represent.

```javascript
function talksAbout(node, string) {
  if (node.nodeType == document.ELEMENT_NODE) {
    for (let i = 0; i < node.childNodes.length; i++) {
      if (talksAbout(node.childNodes[i], string)) {
        return true;
      }
    }
    return false;
  } else if (node.nodeType == document.TEXT_NODE) {
    return node.nodeValue.indexOf(string) > -1;
  }
}

console.log(talksAbout(document.body, 'book'));
```

## Finding Elements

Navigating links between parent, child and sibling nodes is useful but when we need to find a specific node in the document it's unweildy to start a `document.body` and follow the chain hoping to find what you're looking for. This adds assumptions about the structure of the document which we may wish to change. Additionally things get complicated by the fact that text nodes get created for the whitespace between nodes. The example document's body doesn't just have 3 children (h1 + 2 p tags), it has 7, those plus additional spaces before, after and between them.

To get the `href` attribute of the link in the example document we don't want to say something like "Get the second child of the sixth child of the body", we want to say "get the first link in the document" like so:

```javascript
var link = document.body.getElementsByTagName('a')[0];
console.log(link.href);
```

Every element node has a `getElementsByTagName` method, which collects all the descendents of the element with the given tag and returns them in an array like object. To find a specific single node you can give it an `id` attribute and use `document.getElementById`

```html
<p>My ostrich Gertrude:</p>
<p><img id="gertrude" src="img/ostrich.png"></p>

<script>
    var ostrich = document.getElementById('gertrude');
    console.log(ostrich.src);
</script>
```

There's also `getElementsByClassName`, which like `getElementsByTagName` returns an array like object of nodes but instead looks for elements whose `class` attribute contains the given string.

## Changing The Document

Almost everything in a DOM structure can be changed. Element nodes have a number of methods useful for modifying their content. `removeChild` will remove the given child node from the element. Then there's `appendChild` and `insertBefore` which add elements to the document, either to the end of the list of children in `appendChild`'s case or to the beginning for `insertBefore`.

```html
<p>One</p>
<p>Two</p>
<p>Three</p>

<script>
  var paragraphs = document.getElementsByTagName('p');
  document.body.insertBefore(paragraphs[2], paragraphs[0]);
</script>
```

Nodes can only exist in one place in the document, that's why in the example above the `Three` paragraph is moved above the `One` and not just cloned. Every operation that inserts a node somewhere will remove it from where it was before if it had a location.

The `replaceChild` method can be used to replace a child node with another. It takes two nodes for arguments, a new node and the one to replace. The replaced node must be a child of the element this is called on, note how `insertBefore` and `replaceChild` take the new node first, followed by the old.

## Creating Nodes

In this example we write a function that replaces all the `img` tags with the text in their `alt` attributes. We have to remove the image and add the text node which we use `document.createTextNode` for.

```html
<p>The <img src="img/cat.png" alt="Cat"> in the <img src="img/hat.png" alt="Hat">.</p>

<p><button onclick="replaceImages()">Replace</button></p>

<script>
  function replaceImages() {
    var images = document.body.getElementsByTagName('img');
    for (let i = images.length - 1; i >= 0; i--) {
      let image = images[i];
      if (image.alt) {
        const text = document.createTextNode(image.alt);
        image.parentNode.replaceChild(text, image);
      }
    }
  }
</script>
```

`createTextNode` when given a string returns a text node (type 3), which we can then insert in the document. In our loop we start at the end, this is because the node list returned by methods like `getElementsByTagName` is live. It updates as we change it, so if we start at the front the list would lose its first element so when the loop repeats and `i` is 1 it would stop because the length is also 1.

If you wanted a static list nodes instead of live like above you could convert it to a real array by calling the array `slice` method on it.

```javascript
var arrayish = {0: "one", 1: "two", length: 2};
var real = Array.prototype.slice.call(arrayish, 0);
real.forEach(function(elt) { console.log(elt); });
// -> one
// -> two
```

`document.createElement` can be used to create regular element nodes (type 1), it takes a tag name and returns a new empty node of that type.

In this example we create a utility function `elt` which creates an element node and treats all following arguments as children to that node, we then use it to add an attribution to a quote.

```html
<blockquote id="quote">
  No book can ever be finished. While working on it we learn
  just enough to find it immature the moment we turn away
  from it.
</blockquote>

<script>
  function elt(type) {
    let node = document.createElement(type);
    for (let i = 1; i < arguments.length; i++) {
      let child = arguments[i];
      if (typeof child == 'string') {
        child = document.createTextNode(child);
      }
      node.appendChild(child);
    }
    return node;
  }

  document.getElementById('quote').appendChild(
    elt('footer', '-',
        elt('strong', 'Karl Popper'),
        ', preface to the second edition of ',
        elt('em', 'The Open Society and Its Enemies'),
        ', 1950'));
</script>
```

## Attributes

There are a limited set of common standard attributes that can be easily accessed on an element like `href` that get their own properties.

But in HTML you can set any properties you like on a node. This is useful because you can store extra information in these properties. But custom attributes aren't present as properties, instead you need to use `getAttribute` and `setAttribute`.

```html
<p data-classified="secret">The launch code is 000000000.</p>
<p data-classified="unclassified">I have two feet.</p>

<script>
  let paras = document.getElementsByTagName('p');
  Array.prototype.forEach.call(paras, function(para) {
    if (para.getAttribute('data-classified') == 'secret') {
      para.parentNode.removeChild(para);
    }
  });
</script>
```

A common convention is to prefix custom attributes with `data-`, to prevent name conflicts. Let's write a simple example syntax highlighter that looks for `<pre>` tags with a `data-language` attribute and does some very basic highlighting for the language.

```javascript
function highlightCode(node, keywords) {
  const text = node.textContent;
  node.textContent = '';

  let match, position = 0;
  while (match = keywords.exec(text)) {
    const before = text.slice(pos, match.index);
    node.appendChild(document.createTextNode(before));
    let strong = document.createElement('strong');
    strong.appendChild(document.createTextNode(match[0]));
    node.appendChild(strong);
    pos = keywords.lastIndex;
  }
  const after = text.slice(pos);
  node.appendChild(document.createTextNode(after));
}
```

`highlightCode` takes a `<pre>` node and a regular expression (with the global option) that matches the programming language keywords contained in the node.

We use `textContent` to get all the text in the node the set it to empty, then we loop over all keyword matches, appending the text between them as normal text nodes and appending the keywords as `<strong>` nodes wrapping text nodes.

We can then highlight all programs on the page by looping over all `<pre>` elements with `data-language` attributes and calling `highlightCode` on them with the appropriate language regular expression.

```javascript
const languages = {
  javascript: /\b(function|return|var)\b/g,
}

function highlightAllCode() {
  let pres = document.body.getElementsByTagName('pre');
  for (let i = 0; i < pres.length; i++) {
    let pre = pres[i];
    const lang = pre.getAttribute('data-language');

    if (languages.hasOwnProperty(lang)) {
      highlightCode(pre, languages[lang]);
    }
  }
}
```

A basic example

```html
<p>Here it is, the identity function:</p>
<pre data-language="javascript">
function id(x) { return x; }
</pre>

<script>highlightAllCode();</script>
```

The one commonly used attribute `class` has special meaning in JavaScript and due to old browsers lacking compatibilty with looking up properties that were reserved names, it is accessible via `className` or you can use `getAttribute`/`setAttribute` if you really want to access it as `class`.

## Layout

Elements have different layout types. Some, like `<p>` and `h1` take up the width of the document and render on separate lines. These are *block elements*. Others like `<a>` or `<strong>` render on the same line as their surrounding text. They're *inline elements*.

Browsers compute layouts for documents that give each element a size and position based on its type and content and then this layout is used to draw the document.

You can access size and postion data via JavaScript, `offsetHeight` and `offsetWidth` properties return space taken up by an element in pixels. Pixels are the basic unit of measurement in browsers which are typically the smallest renderable dot on your screen. `clientHeight` and `clientWidth` will give you the size of the space inside an element, minus border width.

```html
<p style="border: 3x solid red">
  I'm boxed in
</p>

<script>
  const para = document.body.getElementsByClassName('p')[0];
  console.log('clientHeight', para.clientHeight);
  console.log('offsetHeight', para.offsetHeight);
</script>
```

To get an element's precise postion use the `getBoundingClientRect` method, it returns an object with `top`, `right`, `bottom`, and `left` properties, that are the pixel positions of the element sides relative to the top left of the document. If you want only the document, you need to add current scroll position with the global `pageXOffset` and `pageYOffset` variables.

For performance browsers don't relayout documents immediately and try to defer as long as possible. When a program that changes the document ends the browser computes a new layout to display the changed document on screen. Also when programs ask for the position or size of something by using something like `offsetHeight` or `getBoundingClientRect` will also trigger a relayout.

Programs that alternate between DOM modifcations and reading layout information can trigger a lot of relayouts that can slow things down. Here's an example, we build up a 2000 pixel wide line of X's with two methods and measure the times taken.

```html
<p><span id="one"></span></p>
<p><span id="two"></span></p>

<script>
  function time(name, action) {
    const start = Date.now(); // Current time in milliseconds.
    action();
    console.log(name, 'took', Date.now() - start, 'ms')
  }

  time('naive', function() {
    const target = document.getElementById('one')
    while (target.offsetWidth < 2000) {
      target.appendChild(document.createTextNode('X'));
    }
  });

  time('clever', function() {
    const target = document.getElementById('two')
    target.appendChild(document.createTextNode('XXXXX'));
    const total = Math.ceil(2000 / (target.offsetWidth / 5));
    for (let i = 5; i < total; i++) {
      target.appendChild(document.createTextNode('X'));
    }
  })
</script>
```

## Styling

So HTML elements have different behavior, some are block, some inline, some add styling like `<strong>` making its content bold, or `<a>` making the text blue and underlined.

The way an element renders is strongly tied to its element type, but the default styling of an element like text color can be changed, for example:

```html
<p><a href=".">Normal Link</a></p>
<p><a href="." style="color: green">Green Link</a></p>
```

The style attribute can contain one or more declarations separated by a semicolon. Each declarations follows a `property: value` format like `color: red; border: none`

Lots of things can be changed with styling, like the `display` property that controls the element display type, like `block` or `inline`.

```html
This text is displayed <strong>inline</strong>,
<strong style="display: block">as a block</strong>, and
<strong style="display: none">not at all</strong>.
```

The `<strong>` with display set to block will render on its own line, and the one with display set to none won't render at all as that value prevents an element from rendering. It's preferred to hide elements instead of removing them as it makes showing them again later easier.

You can modify styling of a node using JavaScript with the `style` property, it holds an object with properties for all style properties, the values are strings that we can change to modify the element's styling.

```html
<p id="para" style="color: purple">
  Pretty text
</p>

<script>
  const para = document.getElementById("para");
  console.log(para.style.color);
  para.style.color = "magenta";
</script>
```

Since style property names can be awkward with names that use dashes, you have camelCase alternatives available to you in the style object, so you have `style.fontFamily` instead of style['font-family'].

## Cascading Styles

This styling system is called Cascading Style Sheets (CSS), a style sheet is a set of rules for how to style elements in a document, you can add one inside a `<style>` tag.

```html
<style>
  strong {
    font-style: italic;
    color: gray;
  }
</style>
<p>Now <strong>strong text</strong> is italic and gray.</p>
```

Cascading refers to the fact that rules combine to produce the final styles for elements. In the example before, `<strong>`'s get `font-weight: bold` by default from browsers, we then add the italic and gray.

When more than one rule defines a property value the most recent gets a higher precedence and wins, so if we added `font-weight: normal` to the example the browser would render our text normal, not bold. Styles on a node's `style` attribute always have highest precedence and win.

You can also tag things aside from tag names in CSS, a rule like `.abc` gets all elements with a `class` attribute containing `abc`, likewise, `#xyz` would get the element with their `id` attrib` set to `xyz`, which should be unique in the document.

```css
.subtle {
  color: gray;
  font-size: 80%;
}
#header {
  background: blue;
  color: white;
}
/* p elements, with classes a and b, and id main */
p.a.b#main {
  margin-bottom: 20px;
}
```

The precedence rule only holds true if the rules have the same specificity, which is a measure of how precisely it describes matching elements. This is determined by the number and kind (classes, ID, or tags) of element aspects required. A rule `p.a` is more specific than just `p` or `.a` rules and would take precedence.

Notation like `p > a {..}` applies the styles to the `<a>` tags that are direct children of `<p>` tags. While `p a {..}` will apply to all `<a>` tags within a `<p>` tag whether directly or indirectly children.

## Query Selectors

Stylesheets aren't used much in the book, they're crucial to programming in the browser but to explain them is the work of another book. We just wanted to introduce selector notations is because it's also useable in JavaScript to find DOM elements.

The `querySelectorAll` method that's on the `document` object and element nodes takes selector strings and returns an array like object containing all matched elements.

```html
<p>And if you go chasing
  <span class="animal">rabbits</span></p>
<p>And you know you're going to fall</p>
<p>Tell 'em a <span class="character">hookah smoking
  <span class="animal">caterpillar</span></span></p>
<p>Has given you the call</p>

<script>
  function count(selector) {
    return document.querySelectorAll(selector).length;
  }
  console.log(count("p"));           // All <p> elements
  // → 4
  console.log(count(".animal"));     // Class animal
  // → 2
  console.log(count("p .animal"));   // Animal inside of <p>
  // → 2
  console.log(count("p > .animal")); // Direct child of <p>
  // → 1
</script>
```

Unlike `getElementsByTagName` the object returned isn't live, it won't change when you change the document. `querySelector` works much the same as `querySelectorAll` but if you want a single specific element, it'll either return the first matching element or null if nothing matches.

## Positioning and Animating

The `position` property is very powerful, its default value of `static`, just means the element sits where it normally should. `relative` will leave the element in its place but allows you to use the `top` and `left` properties to move it relative to its normal location, instead of the top-left of the document. `absolute` will take the element out of the normal document flow and meaning it no longer takes space and can overlap other elements, the `top` and `left` properties can be used to position it relative to the top-left corner of the nearest enclosing element with a `position` set to something other than `static` or relative to the document if no element is found.

Here's an example where we create an animation, it's a cat picture floating around in an ellipse.

```html
<p style="text-align: center">
  <img src="../assets/cat.png" style="position: relative">
</p>
<script>
  let cat = document.querySelector('img');
  let angle = 0, lastTime = null;

  function animate(time) {
    if (lastTime != null) {
      angle += (time - lastTime) * 0.001;
    }
    lastTime = time;
    cat.style.top = `${Math.sin(angle) * 20}px`;
    cat.style.left = `${Math.cos(angle) * 200}px`;
    requestAnimationFrame(animate);
  }
  requestAnimationFrame(animate);
</script>
```

We center the picture and give it a `position: relative`. We'll update the `top` and `left` properties to move it.

We use `requestAnimationFrame` to schedule the browser to call `animate` whenever its ready to repaint the screen. `animate` then calls `requestAnimationFrame` again to schedule our next update. When the tab is active this should produce a smooth animation that updates 60 times a second.

Browsers don't update the display or allow any interaction with the page while JavaScript is running. So if we just updated the DOM on a loop we would freeze the page and nothing would happen. That's where `requestAnimationFrame` comes in, it lets the browser know we're done and that it can continue, doing things like updating the screen and responding to user actions.

The `animate` function takes a single argument, the current time, which we compare to the previous time to make sure the cat's motion is stable per millisecond and the animation runs smoothly. If it moved a fixed amount per step we would have stutter, if another heavy task were to prevent the function from running.

We move in circles using trigonometric functions `Math.sin` and `Math.cos`, let's go over them to understand the animation and because we'll see them again.

We use `Math.sin` and `Math.cos` to find points that lie on a circle around point (0, 0) with a radius of one unit. Both functions take their argument and determine that position on the circle, zero denoting the far right most point on the circle, going clockwise up to 2π (~6.28) is a full rotation. `Math.cos` will tell you the `x-coordinate` of the point that corresponds to the given position on a circle, and `Math.sin` will give you the `y-coordinate`. Positions greater than 2π or less than 0 are valid, the rotation repeats so angle (a+2π) == angle (a).

![Cosine and Sine Example](assets/cos_sin.svg)

We keep a counter `angle` and increment it in proportion to time elapsed since last call to `animate`, we can then use this angle to compute the current position of the image. The `top` style is computed using `Math.sin` and multiplied by 20 to get us the vertical radius, and then the `left` style is computed with `Match.cos` and multiplied by 200 to make it much wider and elliptical.

We use units here by adding the `px` to the end of our calculated position to tell the browser to count in pixels instead of another unit like `em`, unitless numbers will result in styles being ignored with the exception of 0, which means the same regardless of unit.

## Summary

Through a data structure called the DOM, JavaScript is able to inspect and modify the current document. The DOM represents the browser's model of the document and we can modify it with JavaScript to change the document.

The DOM is a tree-structure and objects representing elements have properties like `parentNode` and `childNodes` that can be used to navigate the document.

Styling influences document display, both by attaching directly to nodes as well as by defining rules that match certain nodes. There are lots of style properties like `color` or `display` that you can modify via the `style` property on an element.
