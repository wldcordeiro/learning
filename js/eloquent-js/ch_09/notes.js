"use strict";
// Eloquent Javascript Chapter 09: Regular Expressions - Notes

// Regular expressions are a way to describe patterns in string data, they form
// a small language that is part of JavaScript and other languages. Regular Expressions
// are both extremely useful and terrible to use, with cryptic syntax and clumsy
// programming interfaces in JavaScript. But they're a powerful tool for inspecting
// and processing strings.

/// Creating a Regular Expression

// Regular Expressions are a type of object, constructible with the RegExp function
// or written as a literal value by enclosing the pattern in forward slashes. `/`
var re1 = new RegExp("abc");
var re2 = /abc/;
// both of these represent the same pattern, an "a" character, followed by a "b"
// then a "c". When using the Regexp constructor, the pattern is just a string.
// So the usual backslash rules apply.

// The second notation though treats backslashes differently. Since we use forward
// slashes to mark the start and end of patterns we need to escape forward slashes.
// Also backslashes get preserved rather than ignored like in strings and change
// the pattern meaning. Some characters like "?" or "+" have special meaning in
// regular expressions and need to be escaped if they're to represent the actual
// character.
var eighteenPlus = /eighteen\+/;

/// Testing for Matches

// There are a number of instance methods on RegExp objects, the simplest being
// `test`, if you pass it a string, it returns a boolean telling you if the string
// matches the pattern or not.
console.log(/abc/.test("abcde"));
console.log(/abc/.test("abxde"));
// A regular expression consisting of only nonspecial characters, represents that
// sequence of characters. If "abc" occurs anywhere in the string we're test, test
// will return true, not just if "abc" were at the start.

/// Matching a Set of Characters

// Matching something like "abc" is kind of useless though, let's do some more
// complicated stuff. Let's match any number, put square brackets around a set of
// characters makes the expression match any of the charcters within the brackets
// Both these match all strings containing a digit.
console.log(/[0123456789]/.test("in 1992"));
console.log(/[0-9]/.test("in 1992"));
// The dash between the characters denotes a range of characters based on their
// Unicode number. 0 - 9 are right next to each other so we can just use 0-9,
// There are built-in shortcuts for common character sets. For digits (0-9), use \d

// \d Any digit character
// \w Any alphanumeric character
// \s Any whitespace character
// \W A non-alphanumeric character
// \S A non-whitespace character
// .  Any character except for newline

// You can match a date/time format like 30-01-2003 15:20 like this.
var dateTime = /\d\d-\d\d-\d\d\d\d \d\d:\d\d/;
console.log(dateTime.test("30-01-2003 15:20"));
console.log(dateTime.test("30-jan-2003 15:20"));
// Awful looking huh? We'll do a better version later.
// Escape codes can be used in square brackets like [\d.] which matches any
// digit or period character. Note that the period loses its special meaning
// in the square brackets though.

// To invert some characters (i.e. match any character except the one specified)
// use the caret "^"
var notBinary = /[^01]/;
console.log(notBinary.test("1100100010100110"));
console.log(notBinary.test("1100100010200110"));

/// Repeating Parts of a Pattern

// What about matching sequences? When you put a plus sign after something in a
// RegExp it indicates that it may be repeated more than once.
console.log(/'\d+'/.test("'123'"));
console.log(/'\d+'/.test("''"));
console.log(/'\d*'/.test("'123'"));
console.log(/'\d*'/.test("''"));
// The star allows the pattern to match zero times, something with a star after it
// never prevents a pattern from matching, it'll match 0 instances if it can't find
// text to match.

// A question mark makes part of the pattern "optional", meaning it can occur
// 0-1 time.
var neighbor = /neighbou?r/;
console.log(neighbor.test("neighbour"));
console.log(neighbor.test("neighbor"));
// To indicate that a pattern should occur a precise number of times, use curly
// braces. It's also possible to specify a start and stop point like {2,4} as
// well as specifying open ended ranges like {,5} or {5,} that say to top at five
// or to start at five.

// For example, here's another version of the date time example from earlier.
dateTime = /\d{1,2}-\d{1,2}-\d{4} \d{1,2}:\d{1,2}/;
console.log(dateTime.test("30-1-2003 8:45"));

/// Grouping Subexpressions

// To use an operator like + or * on more than one element at a time, use parantheses.
// A part of a pattern enclosed in paranetheses counts as a single element as far
// as the operators care.
var cartoonCrying = /Boo+(hoo+)+/i;
console.log(cartoonCrying.test("Boohoooohoohooo"));
// The first and second + characters only apply to the second o in boo and hoo,
// The third applies to the whole group (hoo+) matching one or more sequences like
// that. The "i" at the end of the expression makes this a case insensitive pattern.

/// Matches and Groups

// The simplest way to match a regular expression is through the test method. It
// tells you whether it matched. You also have the exec method available that will
// return null if no match is found and an object with information on the match
// otherwise.
var match = /\d+/.exec("one two 100");
console.log(match);
console.log(match.index);
// The object returned from exec has an index property that tells you where the
// match began. Other than that it's an array of strings, whose first element is
// the matched string.

// Strings have a match method that behaves similarly to exec.
console.log("one two 100".match(/\d+/));
// When the regex contains subexpressions grouped with parantheses, the text that
// matched the groups will also show up in the array. The whole match is always
// the first element though and each element after is what matched by the groups.
var quotedText = /'([^']*)'/;
console.log(quotedText.exec("She said 'hello'"));
// When a group doesn't match at all (like when followed by ?) its position in the
// array will hold undefined. Also when a group is matched multiple times only the
// last match ends up in the array.
console.log(/bad(ly)?/.exec("bad"));
console.log(/(\d)+/.exec("123"));
// Groups can be useful for extracting parts of strings. If we don't want to verify
// whether a string contains a date but also extract it and construct an object
// that represents it we can wrap parantheses around the patterns and pick the date
// out of the exec object.

/// The Date Type

// There's a standard Date object in JavaScript for representing dates as points
// in time. It's the Date object and using the new keyword on a Date gets you
// the current date and time.
console.log(new Date());
// Or objects for specific times.
console.log(new Date(2009, 11, 9));
console.log(new Date(2009, 11, 9, 12, 59, 59, 999));
// For reasons unknown months begin at 0 and days begin at 1 in JS, be careful with
// that. The last four arguments (hours, minutes, seconds, and milliseconds) are
// optional and default to 0 if not given. Timestamps are stored as time since epoch.
// Using negative numbers for before epoch (beginning of 1970), this follows Unix
// time convention. The getTime method on a Date object returns this number.
console.log(new Date(2013, 11, 19).getTime());
console.log(new Date(1387436400000));
// If you give the Date constructor only a single argument it's treated as a
// millisecond count. You can get the current millisecond count by creating a new
// Date object and calling getTime on it or Date.now.

// Date objects have methods like getFullYear, getMonth, getDate, getHours, getMinutes,
// and getSeconds to extract components. There's getYear but it gives you a useless
// 2 digit value. Using parantheses we can easily create a date object from a string
function findDate(string) {
  var dateTime = /(\d{1,2})-(\d{1,2})-(\d{4})/;
  var match = dateTime.exec(string);
  return new Date(Number(match[3]),
                  Number(match[2]) - 1,
                  Number(match[1]));
}

console.log(findDate("30-1-2003"));

/// Word and String Boundaries

// Unfortunately findDate will extract the nonsense date 00-1-3000 from the string
// "100-1-30000" A match may happen anywhere in a string. So in this case it starts
// at the second character and ends at the second to last.

// If we want to enforce the match spans the whole string, we add the markers ^ and
// $. The ^ matches the start of the string, and the $ matches the end. So, /^\d+$/
// matches a string consisting of one or more digits. /^!/ matches a string that
// starts with an exclamation and /x^/ doesn't match any string (no x before the
// start of the string.)

// If we just want to start and end on a word boundary we can use "\b", a word
// boundary can be the start, end or any point in the string that has a word character
// on  one side and a non-word character on the other.
console.log(/cat/.test("concatenate"));
console.log(/\bcat\b/.test("concatenate"));
// Note the boundary maker doesn't express a character, it just enforces the regex
// matching only when a certain condition holds at the place where it appears in
// the pattern.

/// Choice Patterns

// Say you want to know whether a piece of text contains a number followed by one
// of the words, cow, pig, or chicken, in their singular or plural form. You use
// the pipe character "|" which denotes a choice between the patterns to its left
// and right.
var animalCount = /\b\d+ (pig|cow|chicken)s?\b/;
console.log(animalCount.test("15 pigs"));
console.log(animalCount.test("15 pigchickens"));
// Parantheses can be used to limit the parts of the pattern the pipe character applies
// to and you can put more than one pipe character next to each other to express
// choices between more than two patterns.

/// The Replace Method

// Strings have a replace method that can replace part of a string with another string.
console.log("papa".replace("p", "m"));
// The first argument can be a regex, in which case the first match is replaced.
// When you add the global "g" option to the regex, all matches are replaced.
console.log("Borobudur".replace(/[ou]/, "a"));
console.log("Borobudur".replace(/[ou]/g, "a"));
// It'd be nice if the choice between replacing just the first or all was an argument
// or separate method like 'replaceAll', but that's not the case. The real power here
// lies in the fact that we can reference back to matched groups in the replacement
// string. Say we had a big string containing the names of people, one per line,
// like "lastName, firstName", if we wanted to swap the order and remove the comma
// we could do this.
console.log(
  "Hopper, Grace\nMcCarthy, John\nRitchie, Dennis"
    .replace(/([\w]+), ([\w]+)/g, "$2 $1")
);
// The $1 and $2 in the replacement string refer to the matched pattern groups.
// This works all the way up to $9, and the whole match can be referred to with
// $&

// You can also pass a function instead of a string, for each replacement the function
// will be called with the matched groups (as well as the whole match) as arguments
// and the return value will be inserted in the new string.
var s = "the cia and fbi";

console.log(s.replace(/\b(fbi|cia)\b/g, str => str.toUpperCase()));

// More involved example.
var stock = "1 lemon, 2 cabbages, and 101 eggs";

function minusOne(match, amount, unit) {
  amount = Number(amount) - 1;
  if (amount == 1) {
    unit = unit.slice(0, unit.length - 1);
  } else if (amount == 0) {
    amount = "no";
  }

  return `${amount} ${unit}`;
}
console.log(stock.replace(/(\d+) (\w+)/g, minusOne));

/// Greed

// We can write code that strips comments from JavaScript like.
function stripCommentsGreedy(code) {
  return code.replace(/\/\/.*|\/\*[^]*\*\//g, "");
}
console.log(stripCommentsGreedy("1 + /* 2 */3"));
console.log(stripCommentsGreedy("x = 10;// ten!"));
console.log(stripCommentsGreedy("1 /* a */+/* b */ 1"));
// The part before the operator matches two slash characters followed by any number
// of non-newline characters. For multiline comments it's a bit more involved.
// We use `[^]` to match any character that is not in the empty set. We can't use
// dot here because we need to traverse lines. But the output appears to have gone
// wrong. Why? The `[^]*` of the pattern matches as much as it can first. If that
// fails the matcher moves back a character and tries again. Fore the example the
// matcher tries matching the whole rest of the string and moves back from there.
// It finds the "*/" after going back four characters, not the intended behavior.
// We say the repetition operators (+, *, ?, and {}) are greedy, they match as much
// as possible and backtrack after. If you put a question mark after them they stop
// being greedy (+?, *?, etc), matching as little as possible and only matching more
// when the pattern doesn't fit the smaller match.

// That's what we wanted, we add the question mark and our comment remover works properly.
// A lot of bugs in Regex come from unintentional greedy operator usage. Try using
// non-greedy repetition operators first before the greedy versions.
function stripComments(code) {
  return code.replace(/\/\/.*|\/\*[^]*?\*\//g, "");
}
console.log(stripComments("1 /* a */+/* b */ 1"));

/// Dynamically Creating RegExp Objects

// There's cases where you won't know the exact pattern you'll need to match. Like
// looking for a user's name in a piece of text and enclosing it in underscores.
// Since you'll only know the name once the program is running you can't use the
// slash notation and need to use the RegExp constructor.
var name = "harry",
    text =  "Harry is a suspicious character.",
    regexp = new RegExp(`\\b(${name})\\b`, "gi");

console.log(text.replace(regexp, "_$1_"));
// When creating the boundary markers (\b) we need to use two slashes because we're
// using them in a normal string. The second argument is the options of the regular
// expression, in this case "gi", global and case insensitive.

// What if the name was dea+hl[]rd because they're a teenager? This would cause a nonsense
// regular expression that won't match. We can work around this by adding backslashes
// before any character we don't trust. Adding slashes before alphanumeric characters
// is a bad idea due to some having special meaning. But escaping everything that's
// not alphanumeric or whitespace works.
name = "dea+hl[]rd";
text = "This dea+hl[]rd guy is super annoying.";
var escaped = name.replace(/[^\w\s]/g, "\\$&");
regexp = new RegExp(`\\b(${escaped})\\b`, "gi");
console.log(text.replace(regexp, "_$1_"));

/// The Search Method

// The indexOf method of strings can't be called with a regular expression, but the
// 'search' method expects a regular expression and like indexOf returns the first
// index on which the expression was found or -1 if it wasn't found.
console.log("  word".search(/\S/));
console.log("    ".search(/\S/));
// Unfortunately you can't indicate that the match should start at a certain offset
// (like the second argument to indexOf).

/// The lastIndex Property

// The exec method also doesn't provide an easy way to search from a certain offset.
// You can however use the lastIndex property on the regular expression object.
// Note though, the global option must be set on the regular expression and the match
// must happen through the exec method.
var pattern = /y/g;
pattern.lastIndex = 3;
var match = pattern.exec("xyzzy");
console.log(match.index);
console.log(pattern.lastIndex);
// If the match succeeds, the lastIndex propert is updated to point after the match.
// Otherwise the lastIndex property is set to 0. When using a global regex for multiple
// exec calls you can have problems with the automatic updates to lastIndex. Your
// regex may start matching at a leftover index.
var digit = /\d/g;
console.log(digit.exec("Here it is: 1"));
console.log(digit.exec("and now: 1"));
// Another effect of the global option is that it changes the way the match method
// on strings works. When called with a global instead of returning an array like
// exec does, match will find all matches and return an array containing all matches.
console.log("Banana".match(/an/g));
// Be cautious with the global option, use them in cases where they're necessary
// like calls to replace or when you're explicitly using lastIndex.

/// Looping Over Matches

// A common pattern is to scan all occurrences of a pattern in a string, in a way
// that gives us access to the match object in the loop body using lastIndex and exec.
var input = "A string with 3 numbers in it... 42 and 88.";
var number = /\b(\d+)\b/g;
var match;
while (match = number.exec(input)) {
  console.log(`Found ${match[1]} at ${match.index}`);
}
// This uses the fact that the value of an assignment expression is the assigned value
// This lets us perform the match at the start of each iteration, save the result
// in a variable, and stop looping when there's no more matches.

/// Parsing an INI File

// Let's do an example, we'll parse an INI file. Let's pretend we're writing a program
// to automatically harvest data on our enemies and we're working on the configuration
// parser.

// get file.
var fs = require('fs');
var file = fs.readFileSync('example.ini', 'utf-8');
// The rules for the INI format are.
// Blank lines and lines starting with semicolons are ignored.

// Lines wrapped in [ and ] start a new section.

// Lines containing an alphanumeric identifier followed by an = character add a
// setting to the current section.

// Anything else is invalid.

// Our task is to convert a string like this to an array of objects, each with a name
// property and an array of settings. We'll need one for each section and one for
// global.

// Since we need to process line by line we'll split by new line, however, not all
// OSes uses just new line. Some use the carriage return before the new line. So
// we split on a regex that matches either case.
function parseINI(string) {
  var currentSection = {name: null, fields: []},
      categories = [currentSection];

      string.split(/\r?\n/).forEach(line => {
        let match;

        if (/^\s*(;.*)?$/.test(line)) {
          return;
        } else if (match = line.match(/^\[(.*)\]$/)) {
          currentSection = {name: match[1], fields: []};
          categories.push(currentSection);
        } else if (match = line.match(/^(\w+)=(.*)$/)) {
          currentSection.fields.push({name: match[1], value: match[2]});
        } else {
          throw new Error(`Line ${line} is invalid.`);
        }
      });

      return categories;
}
var parsedIni = parseINI(file);
console.log(parsedIni);
// The code goes over each line updating the current section object as it goes along.
// First it checks for if the line is a comment or whitespace and ignores these.
// If it's not a comment we check if it's a section header and create a new section
// object if so and push it onto our array. We then check if the line is a setting
// if it is we push a setting object onto the section object's fields array that
// contains the name and value of the setting.
// Finally if it doesn't match any of these forms we error out on invalid.

// We make sure to match the whole line by reusing the `^` and `$` operators in our
// patterns. Not using these produces mostly working code with strange behavior for
// certain input, making a nightmare to debug. Use the start and end input operators.

// The pattern `if (match = line.match(..))` is similar to using an assignment as
// a while condition. You aren't often sure if the match will succeed so you can access
// the resulting object only inside an if statement that tests for it. We assign
// the result of the match to a variable that we immediately use in the if block.

/// International Characters

// Due to historical reasons JavaScript's regex is bad at non-ASCII characters.
// For example in JS a "word character" is only one of the 26 characters in the
// latin alphabet and the underscore. Things like é or β, which are word characters
// won't match `\w`, they will however match `\W`.

// For some odd reasion `\s` doesn't have this problem and matches all unicode white
// space characters.

// Some other languages do support these other language characters in their regex
// implementation and there are plans for future support in JavaScript.

/// Summary

// Regular Expressions are objects that represent patterns in strings. They use
// their own syntax.

// /abc/ - a sequence of characters.
// /[abc]/ - any character from a set of characters.
// /[^abc]/ - any character not from a set of characters.
// /[0-9]/ - Any character in a range of digits (works with characters too /[a-z]/)
// /x+/ - one or more occurences of the pattern x
// /x+?/ - one or more occurences, nongreedy
// /x*/ - zero or more occurences
// /x?/ - zero or one occurence
// /x{2,4}/ - between 2 and 4 occurences
// /(abc)/ - a group
// /a|b|c/ - any one of several patterns
// /\d/ - any digit character
// /\w/ - any alphanumeric character ("word character")
// /\s/ - any whitespace character
// /./ - any character except newlines
// /\b/ - a word boundary
// /^/ - start of input
// /$/ - end of input

// Regular Expressions have a method 'test' that tests whether a given string matches.
// There's also an 'exec' method that returns an array containing the matched groups.
// This array has an index property that indicates where the match began.

// Strings have a 'match' method to match them against a regular expression and a
// 'search' method to search for one, returning only the starting position of the
// match. The 'replace' method can be used to replace matches of a pattern with a
// replacement string based on the match text and groups.

// Regular Expressions have options, written after the closing slash. The 'i' option
// makes the match case insensitive, while the 'g' option makes the expression 'global'
// which among other things makes the replace method replace all matches instead
// of just the first.

// The RegExp constructor can be used to create a regular expression value from a
// string.

// Regular Expressions are a powerful tool with a bad interface, they're great but
// can become unweildy and the trick lies in  knowing when to apply them to a problem.
