# Eloquent JavaScript Chapter 15: Project -- A Platform Game - Notes

We'll walk through implementing a simple platform game. Platform games expect a player to move a figure through a world (often 2D viewed from the side) and jump over/on/around things.

## The Game

The game will be based on [Dark Blue](http://www.lessmilk.com/games/10) this was chosen because it's entertaining, minimalistic and can be built with small amounts of code.

Here's a screenshot:

![Game Screenshot](assets/darkblue.png)

The dark box is the player who needs to collect all the coins (yellow boxes) and avoid the lava (red stuff), when all coins are collected the level is complete.

Players can move around with the left and right keys and jump with the up key. You can jump several times its height and change direction in air to give the player the feeling of direct control.

The game consists of a fixed grid-like background with the moving elements laid on top of the background. Grid fields can be empty, solid or lava. Moving elements are players, coins and certain pieces of lava. Unlike the artificial life from chapter seven, element positions aren't constrained to the grid, coordinates can be fractional, allowing smooth motion.

## The Technology

We'll be using the DOM to display the game and handling key events for user input. Screen and input code will be a small part of the game as the graphics are simple blocks. We can represent the background as a table since it's a static grid of squares, moving elements can be overlaid on top of it with absolute positioning.

In games and programs that animate graphics and respond to user input, efficiency is important. The DOM wasn't designed for high performance graphics, but it's better than expected, on a modern machine our game will run just fine.

Next chapter we'll cover `<canvas>` which is a more traditional way to draw graphics with pixels and shapes instead of DOM elements.

## Levels

Back in chapter 7 we used string arrays to represent a 2D grid, we can use that same technique here to be able to build levels without building a level-editor.

A basic level:

```javascript
const simpleLevelPlan = [
  "                      ",
  "                      ",
  "  x              = x  ",
  "  x         o o    x  ",
  "  x @      xxxxx   x  ",
  "  xxxxx            x  ",
  "      x!!!!!!!!!!!!x  ",
  "      xxxxxxxxxxxxxx  ",
  "                      "
];
```

The plan is laid out like so.

* `x` characters are walls
* ` ` characters are empty spaces
* `!` characters are fixed lava
* `@` is the player's starting position
* `o` are coins
* `=` are blocks of lava that move horizontally
* `|` are blocks of lava that move vertically
* `v` are dripping lava blocks

Note that the grid for moving blocks contains empty spaces and a different data structure will be used for tracking their positions.

A game consists of multiple levels that the user completes by collecting all the coins. Touching lava restarts the level.

## Reading a Level

The following builds a level object, its argument is string array defining a level.

```javascript
function Level(plan) {
  this.width = plan[0].length;
  this.height = plan.length;
  this.grid = [];
  this.actors = [];

  for (let y = 0; y < this.height; y++) {
    const line = plan[y], gridLine = [];
    for (let x = 0; x < this.width; x++) {
      let fieldType = null;
      const ch = line[x],
        Actor = actorChars[ch];

      if (Actor) {
        this.actors.push(new Actor(new Vector(x, y), ch));
      } else if (ch == 'x') {
        fieldType = 'wall';
      } else if (ch == '!') {
        fieldType = 'lava';
      }
      gridLine.push(fieldType);
    }
    this.grid.push(gridLine);
  }

  this.player = this.actors.filter(actor => actor.type == 'player')[0];
  this.status = this.finishDelay = null;
}
```

For brevity we aren't validating our plans, we assume it's getting a proper level plan with a player start position and other essentials.

The level stores its width, height, actors and grid. The grid is an array of arrays, each inner array represents a horizontal line and each square is either null for empty or a string representing the square type (wall, lava).

The actors array has objects that track current position and state of dynamic elements in the level. Each is expected to have a `pos` property giving its position (coordinates for its top-left corner), a size property, and a type property ("coin", "lava" or "player").

After building the grid we find the player object using `filter` and we store it as a property of the level. The status property tracks if a player has won or lost. When this happens `finishDelay` is used to allow us to show a simple animation to avoid looking cheap with immediate resets of the level.

This method can be used for finding out if a level is finished.

```javascript
Level.prototype.isFinished = function() {
  return this.status != null && this.finishDelay > 0;
}
```

## Actors

To store positions and sizes we'll need the `Vector` again for x, y coordinates.

```javascript
function Vector(x, y) {
  this.x = x;
  this.y = y;
}
Vector.prototype.plus = function(other) {
  return new Vector(this.x + other.x, this.y + other.y);
}

Vector.prototype.times = function(factor) {
  return new Vector(this.x * factor, this.y * factor);
}
```

The `times` method scales a vector by an amount, useful when we multiply speed vectors with time intervals to get distance traveled.

In the level section `actorChars` was an object used to associate characters to the constructors, here's it:

```javascript
const actorChars = {
  '@': Player,
  'o': Coin,
  '=': Lava,
  '|': Lava,
  'v': Lava,
};
```

Three characters map to `Lava`, the level passes the character to the actor constructor and the `Lava` constructor uses it to adjust its behavior for its type of lava.

Here's the `Player` type, it has a `speed` property used to help simulate momentum and gravity.

```javascript
function Player(pos) {
  this.pos = pos.plus(new Vector(0, -0.5));
  this.size = new Vector(0.8, 1.5);
  this.speed = new Vector(0, 0);
}
Player.prototype.type = 'player';
```

Since the player is 1.5 units tall we place its initial position half a unit above where the `@` character appears to make sure its bottom aligns with the bottom of the square it appears in.

For lava since it needs to move dynamically we initialize it differently based on the given character. Dynamic lava moves until it hits an obstacle and if it has a `repeatPos` property it jumps back to its starting position, otherwise it inverts its speed and continues in the opposite direction. The constructor just sets up the initial properties the actual moving will be done later.

```javascript
function Lava(pos, ch) {
  this.pos = pos;
  this.size = new Vector(1, 1);

  if (ch == '=') {
    this.speed = new Vector(2, 0);
  } else if (ch == '|') {
    this.speed = new Vector(0, 2);
  } else if (ch == 'v') {
    this.speed = new Vector(0, 3);
    this.repeatPos = pos;
  }
}
Lava.prototype.type = 'lava';
```

Coins are simple, they mostly stay in place but they're given a slight up and down wobble to liven things up. We store a base position and a wobble property to handle the actual position.

```javascript
function Coin(pos) {
  this.basePos = this.pos = pos.plus(new Vector(0.2, 0.1));
  this.size = new Vector(0.6, 0.6);
  this.wobble = Math.random() * Math.PI * 2;
}
Coin.prototype.type = 'coin';
```

Back in chapter 13 we saw `Math.sin` and how it gives us the y-coordinate of a point on a circle. The coordinate goes back and forth in a wave form so the sine function is useful for modeling wavy motion.

To make it so our coins don't all move in sync we use `Math.random` to make each coin's wobble phase start differently. The phase of a sine wave is 2pi and we multiply it by the return of `Math.random` to get that randomized starting wobble position.

```javascript
const simpleLevel = new Level(simpleLevelPlan);
console.log(`${simpleLevel.width} by ${simpleLevel.height}`)
```

We now have everything to represent a level's state. Now to display the level on screen and model time and motion within them.

## Encapsulation as a Burden

Most of our code isn't going to worry about encapsulation for two reasons. First, it takes effort. It makes programs bigger and and adds additional concepts and interfaces. Since there's only so much code a reader cares for we're keeping things small. Second, the elements in the game are so closely tied together that if any change the others will need to change to adapt. Some parts of a system lend themselves to encapsulation but trying to encapsulate something that isn't a suitable boundary is a waste of effort. When you make this mistake you notice your interfaces getting large and detailed, and needing to be modified often as the system evolves.

We will encapsulate the drawing subsystem though, and this is to prepare for displaying the game in a different way in the next chapter. We'll be able to load the game up there with the different drawing system and just have it work.

## Drawing

To encapsulate the drawing system we define a display object, which displays a level. The display type we define here is called `DOMDisplay` because it uses simple DOM elements to display.

We'll use a stylesheet to set the colors and fixed properties of the game elements. It'd also be possible to style the elements directly when we create them but our program would be more verbose.

This little helper gives us a way to make an element and set its class.

```javascript
function elt(name, className) {
  const elt = document.createElement(name);
  if (className) {
    elt.className = className;
  }
  return elt;
}
```

A display is created by giving it a parent element it should append to and a level to render.

```javascript
function DOMDisplay(parent, level) {
  this.wrap = parent.appendChild(elt("div", "game"));
  this.level = level;

  this.wrap.appendChild(this.drawBackground());
  this.actorLayer = null;
  this.drawFrame();
}
```

We create the wrapper element using the `appendChild` method as it returns the element. The background never changes so it's drawn once. Actors are redrawn every time the display is updated. `drawFrame` will use the `actorLayer` property to track the elements that hold actors so they can be easily updated or removed. Our coordinates and sizes are tracked in units relative to grid size where 1 unit means a size or distance of 1.

When it comes to setting pixel sizes we will scale these coordinates up otherwise everything would be really tiny. The `scale` variable is used for our minimum pixels a unit should take up.

```javascript
const scale = 20;
DOMDisplay.prototype.drawBackground = function() {
  const table = elt("table", "background");
  table.style.width = `${this.level.width * scale}px`;
  this.level.grid.forEach(row => {
    const rowElt = table.appendChild(elt("tr"));
    rowElt.style.height = `${scale}px`;
    row.forEach(type => {
      rowElt.appendChild(elt("td", type));
    });
  });
  return table;
}
```

The background is a `<table>` which maps well to the `grid` property on the level object. Each row of the grid is a `<tr>`, the strings in the grid are used as class names for the cells (`<td>`). Here's the CSS to make the table look the way we want.

```css
.background {
  background: rgb(52, 166, 251);
  table-layout: fixed;
  border-spacing: 0;
}

.background td {
  padding: 0;
}

.lava {
  background: rgb(255, 100, 100);
}

.wall {
  background: white;
}
```

We use a few rules to just suppress some unwanted parts of CSS's defaults, (`table-layout`, `border-spacing`, `padding`), we don't want the layout of the table to be determined by its contents and we remove space between cells and padding within them.

We use `background` to set our background color, CSS lets you specify color in multiple ways, some words like `white` or RGB values like `rgb(255, 100, 100)` where the color is split into parts, (red ,green, blue) and you set each to a number between 0 and 255.

We draw the actors by creating a DOM node for it and setting the position and size based on the actor's properties and we multiply the values by `scale` to go from units to pixels.

```javascript
DOMDisplay.prototype.drawActors = function() {
  const wrap = elt("div");
  this.level.actors.forEach(actor => {
    const rect = wrap.appendChild(elt("div", `actor ${actor.type}`));
    rect.style.width = `${actor.size.x * scale}px`;
    rect.style.height = `${actor.size.y * scale}px`;
    rect.style.left = `${actor.pos.x * scale}px`;
    rect.style.top = `${actor.pos.y * scale}px`;
  });
  return wrap;
}
```

We can give an element more than one class by separating the classes with spaces. We give the `actor` class absolute positioning, their type name gets them their color by being used as an extra class.

```css
.actor {
  position: absolute;
}

.coin {
  background: rgb(241, 229, 89);
}

.player {
  background: rgb(64, 64, 64);
}
```

To update the frame we start by removing the `actorLayer` if there already is one and redraw in their new positions. We could try reusing the elements but we'd need a lot more info flow between the display code and simulation code. We'd need to map actors to DOM elements and we need to remove them when they vanish. Since there will only be a handful of actors in the game, redrawing won't be expensive.

```javascript
DOMDisplay.prototype.drawFrame = function() {
  if (this.actorLayer) {
    this.wrap.removeChild(this.actorLayer);
  }

  this.actorLayer = this.wrap.appendChild(this.drawActors());
  this.wrap.className = `game ${this.level.status || ""}`;
  this.scrollPlayerIntoView();
}
```

Using the level status as a class name for the wrapper lets us style the player differently if the game is won or lost.

```css
.lost .player {
  background: rgb(160, 64, 64);
}

.won .player {
  box-shadow: -4px -7px 8px white, 4px -7px 8px white;
}
```

When the player touches lava they turn dark red to suggest burning and when they win we use two blurred white drop shadows to give them a halo effect.

Levels won't always fit into the viewport, and we use `scrollPlayerIntoView` to accommodate that. If the level doesn't fit the viewport, we scroll the viewport to make sure the player is near the center. Here we give the wrapping element of the game a maximum size and make sure things that stick out of the viewport are hidden. We give it a relative position so the inner elements to it are positioned relative to the wrapper's top left corner.

```css
.game {
  overflow: hidden;
  max-width: 600px;
  max-height: 450px;
  position: relative;
}
```

In the `scrollPlayerIntoView` method we find the player's position and update the scroll position of the wrapper. We change the scroll position using the `scrollLeft` and `scrollTop` properties and changed them when the player is too close to the edge.

```javascript
DOMDisplay.prototype.scrollPlayerIntoView = function() {
  const { clientWidth: width, clientHeight: height } = this.wrap;
  const margin = width / 3;

  // Viewport
  const { scrollLeft: left, scrollTop: top } = this.wrap;
  const right = left + width,
    bottom = top + height;
  const player = this.level.player;
  const center = player.pos.plus(player.size.times(0.5)).times(scale);

  if (center.x < left + margin) {
    this.wrap.scrollLeft = center.x - margin;
  } else if (center.x > right - margin) {
    this.wrap.scrollLeft = center.x + margin - width;
  }

  if (center.y < top + margin) {
    this.wrap.scrollTop = center.y - margin;
  } else if (center.y > bottom - margin) {
    this.wrap.scrollTop = center.y + margin - height;
  }
}
```

Finding the center of a player showcases how readable operations on our `Vector` types are. To find the center we add its position (the top left corner of the player) to half its height and then multiply by our scale.

We then check that our player isn't outside our allowed range. This may sometimes set nonsense values below zero or above the element's scrollable area, but this doesn't matter as the DOM contains them to sane values.

We could have always tried keeping the player in the center but that produces a weird effect where the view will shift up and down when you jump. It's better if we have a neutral area near the center of the screen where you can move without causing a scroll.

We'll also need a way to clear a level display when we go to the next level or reset.

```javascript
DOMDisplay.prototype.clear = function() {
  this.wrap.parentNode.removeChild(this.wrap);
}
```

We can display our basic level now!

```html
<link rel="stylesheet" href="style.css">
<script>
  const simpleLevel = new Level(simpleLevelPlan);
  const display = new DOMDisplay(document.body, simpleLevel);
</script>
```

## Motion and Collision

Let's add motion! The basic approach most games take works like this, split up time into small steps and each step, move the actors a distance equal to their speed multiplied by the time step.

The hard part here isn't just moving actors, it's dealing with interactions between elements. When the player hits walls or floors they shouldn't go through it. The game must be aware when a motion causes a collision with another object and respond. For walls motion should stop, for coins, collect the coin, etc.

Solving the general case is a big task and there are libraries called physics engines that simulate interactions in 2D or 3D. We'll go with something smaller and only handle collisions between rectangular objects.

Before moving an actor we test if the motion would move it inside a non-empty part of the background. If so, we cancel the motion. The response to the collision will be determined by the actor type, the player stops, lava will bounce back.

Doing this requires our time step to be small since it causes our motion to stop before actual collision. If the time step is too large, the player ends up hovering above the ground. Another approach would be to find the exact collision point and move there but that's too complicated for our game.

This method tells us whether a rectangle overlaps with any non-empty space on the background:

```javascript
Level.prototype.obstacleAt = function(pos, size) {
  const xStart = Math.floor(pos.x);
  const xEnd = Math.ceil(pos.x + size.x);
  const yStart = Math.floor(pos.y);
  const yEnd = Math.ceil(pos.y + size.y);

  if (xStart < 0 || xEnd > this.width || yStart < 0) {
    return "wall";
  }

  if (yEnd > this.height) {
    return "lava";
  }

  for (let y = yStart; y < yEnd; y++) {
    for (let x = xStart; x < xEnd; x++) {
      const fieldType = this.grid[y][x];
      if (fieldType) return fieldType;
    }
  }
}
```

The method computes the body's overlapping grid squares by using `Math.floor` and `Math.ceil`. Since grid squares are 1x1 units we can round the box sides up or down to get the range of squares the box touches.

![Game Grid](assets/game-grid.svg)

If the body sticks out of the level, we always return "wall" for the sides & top, and "lava" for the bottom to ensure the player dies if they fall out of the world. When the body is in the grid we loop over the block of grid squares found by rounding and return the content of the first non-empty square we find.

Collisions between the player and other dynamic actors are handled after the movement. When the motion has moved the player into the actor the appropriate effect is activated.

This method scans our actor array to find an actor that overlaps with the one given as an argument:

```javascript
Level.prototype.actorAt(actor) {
  return this.actors.find(other => {
    if (other != actor && actor.pos.x + actor.size.x > other.pos.x &&
        actor.pos.x < other.pos.x + other.size.x &&
        actor.pos.y + actor.size.y > other.pos.y &&
        actor.pos.y < other.pos.y + other.size.y) {
      return other;
    }
  });
}
```

## Actors and Actions

The `animate` method loops over all actors and moves them. It takes the time step in seconds and a keys object that contains info about which keys the player has pressed.

```javascript
const maxStep = 0.05;
Level.prototype.animate = function(step, keys) {
  if (this.status != null) {
    this.finishDelay -= step;
  }

  while (step > 0) {
    const thisStep = Math.min(step, maxStep);
    this.actors.forEach(actor => actor.act(thisStep, this, keys), this);
    step -= thisStep;
  }
}
```

When level `status` isn't null (the case for when players win or lose), we decrement the `finishDelay` which tracks the time where we win or lose and the point to stop showing the level.

We cut our animation time step down in the while loop into smaller peices to ensure no step greater than `maxStep` is taken. Like a step of `0.12` would be cut into two `.0.5` steps and one `0.2` step.

Actors have `act` methods that take the time step, level object and `keys` object. Here's the `Lava` actor's `act` method which doesn't use the `keys` object.

```javascript
Lava.prototype.act = function(step, level) {
  const newPos = this.pos.plus(this.speed.times(step));
  if (!level.obstacleAt(newPos, this.size)) {
    this.pos = newPos;
  } else if (this.repeatPos) {
    this.pos = this.repeatPos;
  } else {
    this.speed = this.speed.times(-1);
  }
}
```

We compute the new position by multiplying the time step by the current speed and adding it to the old position. If there's no obstacles the lava moves there. If not it depends on the lava block type. Dripping lava has `repeatPos` as a position it jumps back to after hitting something. Bouncing lava just inverts its speed (multiply by -1) to move the other direction.

The `act` method of a Coin makes it wobble. They ignore collisions since they're just wobbling in their square and colliding with the player will be handled by the player's `act` method.

```javascript
const wobbleSpeed = 8;
const wobbleDist = 0.07;

Coin.prototype.act = function(step) {
  this.wobble += step * wobbleSpeed;
  const wobblePos = Math.sin(this.wobble) * wobbleDist;
  this.pos = this.basePos.plus(new Vector(0, wobblePos));
}
```

The wobble property is updated to track time and used as an argument to `Math.sin` to create a wave to compute the new position.

Now for the player. Player motion is handled for each axis of movement because hitting the floor shouldn't stop horizontal movement and hitting walls shouldn't stop jumping or falling. Here's the horizontal method.

```javascript
const playerXSpeed = 7;
Player.prototype.moveX = function(step, level, keys) {
  this.speed.x = 0;
  if (keys.left) {
    this.speed.x -= playerXSpeed;
  }

  if (keys.right) {
    this.speed.x += playerXSpeed;
  }

  const motion = new Vector(this.speed.x * step, 0);
  const newPos = this.pos.plus(motion);
  const obstacle = level.obstacleAt(newPos, this.size);

  if (obstacle) {
    level.playerTouched(obstacle);
  } else {
    this.pos = newPos;
  }
}
```

Horizontal motion is computed based on the state of the left or right arrow keys. When motion causes a player to hit something, the level's `playerTouched` method (used to handle things like death or collecting coins) is called. Otherwise it updates its position.

Vertical motion is similar but simulates jumping and gravity.

```javascript
const gravity = 30;
const jumpSpeed = 17;
Player.prototype.moveY = function(step, level, keys) {
  this.speed.y += step * gravity;
  const motion = new Vector(0, this.speed.y * step);
  const newPos = this.pos.plus(motion);
  const obstacle = level.obstacleAt(newPos, this.size);

  if (obstacle) {
    level.playerTouched(obstacle);
    if (keys.up && this.speed.y > 0) {
      this.speed.y = -jumpSpeed;
    } else {
      this.speed.y = 0;
    }
  } else {
    this.pos = newPos;
  }
}
```

At method start we accelerate the player vertically to account for gravity. Gravity, jump speed and other constants in this game were set by trial and error until it suited the author.

Next we check for obstacles, if we hit one there's two outcomes. When the up arrow is pressed and we're moving down (meaning we hit something below us), the speed is set to a large negative value. This causes the player to jump. If this isn't the case we just bumped into something and zero the speed.

Heres the `act` method for players.

```javascript
Player.prototype.act = function(step, level, keys) {
  this.moveX(step, level, keys);
  this.moveY(step, level, keys);

  const otherActor = level.actorAt(this);
  if (otherActor) {
    level.playerTouched(otherActor.type, otherActor);
  }

  // losing animation
  if (level.status == "lost") {
    this.pos.y += step;
    this.size.y -= step;
  }
}
```

After we move the method checks for other actors the player could be touching by calling `playerTouched`. This time passing the actor object as the second argument because for coins `playerTouched` needs to know which coin is touched.

Finally when players die we set up a death animation where they shrink by reducing the height of the player.

Here's the method that handles player collisions with other objects.

```javascript
Level.prototype.playerTouched = function(type, actor) {
  if (type == "lava" && this.status == null) {
    this.status = "lost";
    this.finishDelay = 1;
  } else if (type == "coin") {
    this.actors = this.actors.filter(other => other != actor);

    if (!this.actors.some(actor => actor.type == "coin")) {
      this.status = "won";
      this.finishDelay = 1;
    }
  }
}
```

When lava is touched the game `status` is set to "lost", when coins are touched they're removed from the actor array until the last one then we toggle status to "won" and now we have an animatable level!

## Tracking Keys

For a game like this we want the keys to take effect for as long as they're held.

We need a key handler that stores the state of left/right/up/down arrows and we need to call `preventDefault` on the key events.

This function when given an object may of key codes for arrows returns an object that tracks the current state of those keys. It registers handlers for "keyup" and "keydown" events and when the key code in the event matches one in the map it updates the keypress state object.

```javascript
const arrowCodes = { 37: "left", 38: "up", 39: "right" };

function trackKeys(codes) {
  const pressed = {};

  function handler(event) {
    if (codes.hasOwnProperty(event.keyCode)) {
      const down = event.type == "keydown";
      pressed[codes[event.keyCode]] = down;
      event.preventDefault();
    }
  }

  addEventListener("keydown", handler);
  addEventListener("keyup", handler);
  return pressed;
}
```

Note how the same handler is used for both event types. We do this by looking at the event's `type` property to determine if key state should be true for "keydown" or false for "keyup".

## Running the Game

The `requestAnimationFrame` function we saw in chapter 13 is a good way to animate the game. But it has a primitive interface, we have to track the last time the function was called and call `requestAnimationFrame` again after every frame.

Let's define a helper function that wraps all that up for us and lets us call a simple `runAnimation` that takes a function that takes a time step and draws a single frame. When the frame function returns false, the animation stops.

```javascript
function runAnimation(frameFunc) {
  let lastTime = null;
  function frame(time) {
    let stop = false;
    if (lastTime != null) {
      const timeStep = Math.min(time - lastTime, 100) / 1000;
      stop = frameFunc(timeStep) === false;
    }

    lastTime = time;
    if (!stop) {
      requestAnimationFrame(frame);
    }
  }
  requestAnimationFrame(frame);
}
```

The maximum frame step is set to 100 milliseconds (1/10th of a second), when the browser tab running the game is hidden, `requestAnimationFrame` calls will be suspended until the tab is given focus again. In this case the difference between `lastTime` and `time` will be the full time the page was out of focus. Advancing the game by that much in a single step will look silly and may be a lot of work (remember time splitting in the animate method).

The function also converts the time steps to seconds which are an easier quantity to reason about.

The `runLevel` function takes a level object, a display constructor and optionally a callback. It displays the level in `document.body` and lets the user play through it. When the level is finished (either by win or lose), `runLevel` clears the display, stops the animation, and calls the optional callback if it's provided with the level status.

```javascript
const arrows = trackKeys(arrowCodes);

function runLevel(level, Display, andThen) {
  const display = new Display(document.body, level);
  runAnimation(step => {
    level.animate(step, arrows);
    display.drawFrame(step);
    if (level.isFinished()) {
      display.clear();

      if (andThen) {
        andThen(level.status);
      }
      return false;
    }
  });
}
```

Games are sequences of levels whenever the player dies, the current level is restarted. When a level is completed we move to the next level. The next function does these things by taking an array of level plans and a display constructor.

```javascript
function runGame(plans, Display, levelRun) {
  function startLevel(n) {
    levelRun(new Level(plans[n]), Display, status => {
      if (status == "lost") {
        startLevel(n);
      } else if (n < plans.length - 1) {
        startLevel(n + 1);
      } else {
        console.log("You Win!");
      }
    });
  }
  startLevel(0);
}
```

Here we're using a new style of programming, both `runAnimation` and `runLevel` are higher-order functions but not the style seen in chapter five. The function argument is used to arrange things sometime in the future and neither function returns anything useful. They schedule actions, and wrapping the actions in functions gives us a way to store them as values and call them at the right time.

This is called *Asynchronous Programming*. Event handling is also an instance of this style. We'll see more of it when working with tasks that can run for arbitrary lengths of time like network requests in chapter 17 and general input/output in chapter 20.

The `GAME_LEVELS` object contains level plans we call `runGame` with them to start the game.

```html
<link rel="stylesheet" href="style.css">
<script>
  runGame(GAME_LEVELS, DOMDisplay);
</script>
```

Play the game!
