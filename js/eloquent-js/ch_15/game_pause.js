function runLevelPause(level, Display, andThen) {
  const display = new Display(document.body, level);
  let gameState = 1;

  function gameKeyHandler(event) {
    if (event.keyCode == 27) {
      if (gameState == -1) {
        gameState = 1;
        console.log("Game Unpaused");
        runAnimation(animateGame);
      } else if (gameState == 0) {
        gameState = 1;
      } else if (gameState == 1) {
        gameState = 0;
        console.log("Game Paused");
      }
    } else if (event.keyCode == 70) {
      if (!display.wrap.requestFullscreen) {
        display.wrap.webkitRequestFullscreen();
      } else {
        display.wrap.requestFullscreen();
      }
    }
  }

  addEventListener("keydown", gameKeyHandler);
  const arrows = trackKeys(arrowCodes);

  function animateGame(step) {
    if (gameState == 0) {
      gameState = -1;
      return false;
    }

    level.animate(step, arrows);
    display.drawFrame(step);

    if (level.isFinished()) {
      display.clear();
      removeEventListener("keydown", gameKeyHandler);
      arrows.clear();

      if (andThen) {
        andThen(level.status);
      }
      return false;
    }
  }
  runAnimation(animateGame);
}
