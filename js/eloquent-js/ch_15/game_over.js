function runGameOver(plans, Display, levelRun) {
  function startLevel(n, livesRemaining) {
    levelRun(new Level(plans[n]), Display, status => {
      if (status == "lost") {
        if (livesRemaining > 0) {
          console.log(`Lives Remaining: ${livesRemaining - 1}`);
          startLevel(n, livesRemaining - 1);
        } else {
          console.log("You lose!");
          startLevel(0, playerLives);
        }
      } else if (n < plans.length - 1) {
          console.log(`Lives Remaining: ${livesRemaining}`);
        startLevel(n + 1, livesRemaining);
      } else {
        console.log("You Win!");
      }
    });
  }
  console.log(`Lives Remaining: ${playerLives}`);
  startLevel(0, playerLives);
}
