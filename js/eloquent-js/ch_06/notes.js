"use strict";
/// Eloquent Javascript Chapter 06: The Secret Life of Objects - Notes

/// Methods

// Methods are properties that hold function values. For example:
var rabbit = {};

rabbit.speak = function(line) {
  console.log(`The rabbit says '${line}'`);
}

rabbit.speak("I'm alive.");
// Usually the method needs to do something with the object it's called on. When
// a function is called as a method, looked up as a property and called like
// "object.method()" the special variable `this` will point to the object it was
// called on.
function speak(line) {
  console.log(`The ${this.type} rabbit says '${line}'`);
}

var whiteRabbit = {type: "white", speak: speak};
var fatRabbit = {type: "fat", speak: speak};

whiteRabbit.speak("Oh my ears and whiskers, how late it's getting!");
fatRabbit.speak("I could sure use a carrot right now.");
// The code uses the `this` keyword to output the type of rabbit speaking, recall
// that the `apply` and `bind` methods took a first argument to simulate method calls,
// that first argument is used to give `this` a value.

// There's a method similar to `apply` called `call`, which also calls a function
// it's a method of but takes arguments normally, rather than an array. Like
// `apply` and `bind` it can be passed a specific `this` value.
speak.apply(fatRabbit, ["Burp!"]);
speak.call({this: 'old'}, 'Oh my!');

/// Prototypes

var empty = {};

console.log(empty.toString);
console.log(empty.toString());
// A property on an empty object?! Magic? No. Almost all objects in JavaScript
// have a `prototype`. A prototype is another object that's used as a fallback
// property source. When objects get requests for properties they don't have,
// its prototype will be searched for the property, then the prototype's prototype
// and so on.

// What's the prototype of the empty object? The ancestral prototype behind almost
// all objects, `Object.prototype`
console.log(Object.getPrototypeOf({}) == Object.prototype);
console.log(Object.getPrototypeOf(Object.prototype));
// The Object.getPrototypeOf() method gets the prototype of an object.
// The prototype relations in JavaScript form a tree structure with Object.prototype
// at the root, it provides a few methods that show up in all objects, like toString

// Many objects don't have Object.prototype directly as their prototype, but instead
// a different object with its own default properties. Functions derive from
// Function.prototype and arrays from Arrays.prototype.
console.log(Object.getPrototypeOf(isNaN) == Function.prototype);
console.log(Object.getPrototypeOf([]) == Array.prototype);
// Such prototypes will have their own prototype, often Object.prototype, so it's
// still indirectly providing methods.

// Object.getPrototypeOf method obviously gets the objects prototype, but Object.create
// will create an object with a specific prototype.
var protoRabbit = {
  speak: function(line) {
    console.log(`The ${this.type} rabbit says '${line}'`);
  }
};

var killerRabbit = Object.create(protoRabbit);
killerRabbit.type = "killer";
killerRabbit.speak("SKREEEE!");
// The "proto" rabbit acts as a container for shared rabbit properties. Individual
// rabbit objects like the killer rabbit contain properties that only apply to itself.
// It's type in this case while deriving shared properties from its prototype.

/// Constructors

// A more convenient way to create objects that derive from a shared prototype is
// to use a constructor. In JavaScript, calling a function with the keyword `new`
// in front of it, causes it to be a constructor. The constructor will have its
// `this` value set to a new object and unless it explicitly returns another object
// value, this object will be returned from the call.

// When you use new the object created from its constructor is called an instance
// of its constructor.

// Here's a simple constructor for rabbits, it's convention to capitalize the names
// of constructors to distinguish them from other functions.
function Rabbit(type) {
  this.type = type;
}

killerRabbit = new Rabbit('killer');
var blackRabbit = new Rabbit('black');
console.log(blackRabbit.type);
// All functions, including constructors get a property named prototype automatically.
// By default this is an empty object that derives from Object.prototype, every
// instance made with this constructor will have this object as its prototype,
// so to add the speak method to rabbits created with the Rabbit constructor we
// do this.
Rabbit.prototype.speak = function(line) {
  console.log(`The ${this.type} rabbit says '${line}'`);
};

blackRabbit.speak('Doom...');
// An important distinction is that the way a prototype is associated with a constructor
// (through its prototype property) and the way objects *have* a prototype (that
// can be retrieved with Object.getPrototypeOf) are different. The prototype of
// a constructor is Function.prototype since they're functions. Its prototype
// property will be the prototype of instances through it, but not its own prototype.

/// Overriding Derived Properties

// When you add a property to an object, if it's present in the prototype or not,
// the property is added to the object itself, not the prototype. If there is a
// property on the prototype of the same name, this property will no longer affect
// the object.
Rabbit.prototype.teeth = "small";
console.log(killerRabbit.teeth);
killerRabbit.teeth = "long, sharp, and bloody";
console.log(killerRabbit.teeth);
console.log(blackRabbit.teeth);
console.log(Rabbit.prototype.teeth);
// In this case, the Rabbit and Object prototypes lie behind the killerRabbit object
// as a backdrop where properties that are not found on the object, could be looked up.
// Overriding properties that exist on a prototype is often a good thing, it can
// be used to express exceptional properties in instances of a more generic class
// of object, while letting the less exceptional to fall back to a standard value.

// This is used to give standard functions and array prototypes a different version
// of the toString method than the default.
console.log(Array.prototype.toString == Object.prototype.toString);

console.log([1, 2].toString());
// The array toString method is similar to its join method if the separator was a
// comma, directly calling Object.prototype.toString() with an array produces a
// different result. This version doesn't know about arrays, so it just produces
// the word object and the type of object in square brackets.
console.log(Object.prototype.toString.call([1, 2]));

/// Prototype Interference

// Prototypes can be used at any time to add new properties and methods to all
// objects based on it. For example, rabbits can now dance.
Rabbit.prototype.dance = function() {
  console.log(`The ${this.type} rabbit dances a jig.`);
};
killerRabbit.dance();
// Convenient, but there's situations where it can be problematic. In chapter 4,
// we used an object as a map, like so.
var map = {};

function storePhi(event, phi) {
  map[event] = phi;
}

storePhi("pizza", 0.069);
storePhi("touched tree", -0.081);
// We can iterate over an objects phi values with the for/in loop structure,
// and test whether a name is in it with `in` unfortunately the prototype can
// mess this up.
Object.prototype.nonsense = "hi";
for (let name in map)
  console.log(name);
console.log("nonsense" in map);
console.log("toString" in map);
delete Object.prototype.nonsense;
// toString didn't show up in our for/in loop because JavaScript distinguishes between
// enumerable and nonenumerable properties. That's why it's "in" map but not in the
// loop. All properties created by simple assignment are enumerable. The standard
// Object.prototype properties are all nonenumerable.

// It's possible to define our own nonenumerable properties, use Object.defineProperty
// it gives us control over the property type.
Object.defineProperty(
  Object.prototype,
  "hiddenNonsense",
  {enumerable: false, value: "hi"}
);

for (let name in map)
  console.log(name);
console.log(map.hiddenNonsense);
// Now the property is there and won't appear in a loop. But it still returns true
// with the "in" operator. That's where the hasOwnProperty method comes in.
console.log(map.hasOwnProperty("toString"));
// This method tells us whether an object itself has a property without checking
// its prototypes. This is often more useful than the information "in" gives us.

// If you're unsure something might have messed with the base object prototype,
// use this kind of for/in loop
for (let name in map){
  if (map.hasOwnProperty(name))
    console.log(name);
}

/// Prototype-less Objects

// Troubles don't end there, what if someone registered hasOwnProperty to return
// 42? Now the call to the property will return the number instead of the method
// call. In these cases, having a prototype gets in the way so let's create an
// object without a prototype. Using the Object.create and passing it null for the
// prototype you'll create a fresh prototypeless object. For objects like map where
// the properties can be anything this is what we want.
var map = Object.create(null);
map['pizza'] = 0.069;
console.log('toString' in map);
console.log('pizza' in map);
// Better, now we no longer need to test with the hasOwnProperty method and can safely
// use the for/in loop regardless what has been done to Object.prototype.

/// Polymorphism

// When the String function is called it converts a value into a string by calling
// that object's toString method to return a meaningful value. Some of the standard
// prototypes define their own versions of toString to return something more useful
// than [object Object].

// This is a powerful idea, when a piece of code is written to work with objects
// that have a certain interface (in this case toString), any object with that
// interface can be plugged into the code and it will work.

// This technique is called polymorphism, polymorphic code can work with values of
// different types as long as they support the interface it expects.

/// Laying Out A Table

// Needed for the example project.
var mountains = require('./mountains');
// To get a better understanding of polymorphism and object oriented programming
// we'll work with a project where we will create a program that given an array
// will arrange that array in a table like so.

// name         height country
// ------------ ------ -------------
// Kilimanjaro    5895 Tanzania
// Everest        8848 Nepal
// Mount Fuji     3776 Japan
// Mont Blanc     4808 Italy/France
// Vaalserberg     323 Netherlands
// Denali         6168 United States
// Popocatepetl   5465 Mexico

// The way the table-building system will work is that the builder function will
// query each hell for its width and height to determine the width and height of
// columns and rows. The builder function will then "ask" the cells to draw themselves
// at the correct size and assemble the string result.

// The layout program will have a well defined interface to communicate through,
// this makes it so the types of cells that are supported are not fixed, making
// adding new cell styles later doable.

// Here is the interface.
// * minHeight() - returns a number indicating the minimum height this cell needs
//                 in lines.
// * minWidth() - returns a number indicating the minimum width this cell needs
//                in characters.
// * draw(width, height) - returns an array of length `height` containing the series
//                         of strings that are each `width` characters wide, this
//                         represents cell content.

// Heavy use of higher order array methods in examples.

// Here we compute arrays of minimum column widths and row heights for a grid of
// cells. The rows variable will hold an array of arrays, where each inner array
// represents a row of cells.
function rowHeights(rows) {
  return rows.map(function(row) {
    return row.reduce(function(max, cell) {
      return Math.max(max, cell.minHeight());
    }, 0);
  });
}

function colWidths(rows) {
  return rows[0].map(function(_, i) {
    return rows.reduce(function(max, row) {
      return Math.max(max, row[i].minWidth());
    }, 0);
  });
}
// By convention a variable that's just an underscore is an indication that it's
// not going to be used.

// rowHeights function uses reduce to compute the max height of an array of cells
// and wrapsthat in a map to do it for all the rows in the rows array.
// colWidths function is a bit more complicated, since the outer array is an array
// of rows not columns. Map (and other array methods) pass a second argument to
// their predicate with the index of the element, we use the indexes to build up
// an array with an element for every column index. The call to reduce runs over
// the outer array for each index and grabs the widest cell's width at that index.

// Here's how to draw the table.
function drawTable(rows) {
  var heights = rowHeights(rows);
  var widths = colWidths(rows);

  function drawLine(blocks, lineNo) {
    return blocks.map(function(block) {
      return block[lineNo];
    }).join(" ");
  }

  function drawRow(row, rowNum) {
    var blocks = row.map(function(cell, colNum) {
      return cell.draw(widths[colNum], heights[rowNum]);
    });

    return blocks[0].map(function(_, lineNo) {
      return drawLine(blocks, lineNo);
    }).join('\n');
  }

  return rows.map(drawRow).join('\n');
}
// The drawTable function uses the internal helper drawRow to draw all rows and
// joins them together with newline.

// drawRow function converts cell objects in rows to blocks. Which are arrays of
// strings representing the contents of a cell, split line by line. A single cell
// containing just 3776 will be stored as ['3776'] whereas an underlined cell might
// look like ['name' '----'].

// The blocks for a row, that all have the same height should be next to each other.
// The second call to map in drawRow builds up this output line-by-line, by mapping
// the over leftmost block's lines and for each collects a line that spans over the
// full-width of the table. The lines are then joined with a newline to provide the
// whole row as drawRow's return value.

// drawLine extracts lines that should appear next to each other from an array
// of blocks, joining them with a space character to make a 1 character gap between
// table columns

// Let's write a constructor for cells that contain text which implements the interface
// for table cells. The constructor splits a string into an array of lines with
// the split string method, cutting up a string at every occurrence of its argument
// returning an array of the pieces. minWidth finds the maximum line width in the
// array.
function repeat(string, times) {
  var result = "";

  for (let i = 0; i < times; i++)
    result += string;

  return result;
}

function TextCell(text) {
  this.text = text.split('\n');
}

TextCell.prototype.minWidth = function() {
  return this.text.reduce(function(width, line) {
    return Math.max(width, line.length);
  }, 0);
};

TextCell.prototype.minHeight = function() {
  return this.text.length;
};

TextCell.prototype.draw = function(width, height) {
  var result = [];

  for (let i = 0; i < height; i++) {
    let line = this.text[i] || '';
    result.push(line + repeat(' ', width - line.length));
  }

  return result;
};
// We use a helper function repeat that builds a string whose value is the string
// argument repeated n `times`, the draw method uses it to add "padding", so they're
// the same length

// Let's try it out making a 5x5 checkerboard with what we wrote.
var rows = [];

for (let i = 0; i < 5; i++) {
  let row = [];

  for (let j = 0; j < 5; j++) {
    if ((j + i) % 2 == 0)
      row.push(new TextCell('##'));
    else
      row.push(new TextCell('  '));
  }

  rows.push(row);
}
console.log(drawTable(rows));
// Cool, but we have no lines of different lengths so nothing interesting occurs
// We'll use source data of mountain information in the mountains variable. We're
// underlining the headers with a series of dash characters, so we'll write a cell
// type that handles underlining.
function UnderlinedCell(inner) {
  this.inner = inner;
}
UnderlinedCell.prototype.minWidth = function() {
  return this.inner.minWidth();
};
UnderlinedCell.prototype.minHeight = function() {
  return this.inner.minHeight() + 1;
};
UnderlinedCell.prototype.draw = function(width, height) {
  return this.inner.draw(width, height -1).concat([repeat('-', width)]);
};
// An underlined cell contains an inner cell, reporting it's minimum size as the
// same as the inner cell (by calling through to the inner cell's minWidth and
// minHeight methods), adding one to the height to account for the underline.

// Drawign the cell is easy, take the contents of the inner cell and concatenate
// a single line of dashes to it. With the underlining system done we can now write
// a function that builds up a grid of cells from the data set.
function dataTableOld(data) {
  var keys = Object.keys(data[0]);

  var headers = keys.map(function(name) {
    return new UnderlinedCell(new TextCell(name));
  });

  var body = data.map(function(row) {
    return keys.map(function(name) {
      return new TextCell(String(row[name]));
    });
  });

  return [headers].concat(body);
}

console.log(drawTable(dataTableOld(mountains)));
// Using the standard Object.keys method we get an array of property names in an
// object, the top row of the table must contain underlined header cells. Below
// that the values of all the rest of the data set appear as normal cells, they're
// extracted by mapping over the keys array to ensure cell order in every row.

// We've got the table almost right, but our number cells need to be right aligned
// in the height column.

/// Getters and Setters

// When specifying an interface it's possible to include properties that aren't methods
// minWidth and minHeight could have been defined to simply hold numbers, but that
// would mean computing them in the constructor which adds code there and isn't
// relevant to constructing the object. It'd cause problems if the inner cell of
// an underlined cell was changed which should mean that the underlined cell size
// should change.

// Some people have taken the policy of never adding non-method properties. Using
// methods like getSomething() or setSomething() rather than directly accessing a
// value, the downside is that you'll end up writing and reading lots of additional
// methods.

// JavaScript provides techniques that are the best of both worlds, we can specify
// properties that behave like normal properties but have methods behind the scenes
// associated with them.
var pile = {
  elements: ['eggshell', 'orange peel', 'worm'],
  get height() {
    return this.elements.length;
  },
  set height(value) {
    console.log(`Ignoring Attempt to set the height to ${value}`);
  }
};

console.log(pile.height);
pile.height = 100;
// In object literal, the get or set notation for properties allows you to specify
// a function to be run when a property is read or written. You can also add this
// to an existing object using the Object.defineProperty method (which we used to
// create nonenumerable properties.)
Object.defineProperty(TextCell.prototype, 'heightProp', {
  get: function() { return this.text.length; }
});

var cell = new TextCell('no\nway');
console.log(cell.heightProp);
// cell.heightProp = 100; // Ignored in non-strict mode, fails in strict.
console.log(cell.heightProp);
// You can add a set property in the object passed to Object.defineProperty to
// specify a setter. When there's a getter but no setter, attempts to write to the
// property are ignored.

/// Inheritance

// We still need to add right aligned cells to the system to ease readability by
// right aligning the number columns, we'll create another cell like TextCell, but
// one that padds the lines on the left side so they align to the right.

// We could write a whole new constructor with the three prototype methods, but
// prototypes can have prototypes so we can do something clever.
function RTextCell(text) {
  TextCell.call(this, text);
}

RTextCell.prototype = Object.create(TextCell.prototype);

RTextCell.prototype.draw = function(width, height) {
  var result = [];

  for (let i = 0; i < height; i++) {
    let line = this.text[i] || '';
    result.push(repeat(' ', width - line.length) + line);
  }

  return result;
};
// We reuse the constructor, minHeight and minWidth methods from the regular
// TextCell, an RTextCell is basically a TextCell with a custom draw method.

// This pattern is called inheritance, it allows us to build slightly different
// data types from existing ones with little work. Typically the new constructor
// calls the old one (using the call method to give it the new object as its this
// value). After the constructor's been called we can assume all the fields contained
// on the old object type have been added. We then set the constructor's prototype
// to derive from the old prototype so instances will have access to the properties
// in that prototype. Then we can override some of these properties by adding them
// to our new prototype. Now to adjust the dataTable function to use RTextCells
// for cells with number values, to get the table we're aiming for.
function dataTable(data) {
  var keys = Object.keys(data[0]);

  var headers = keys.map(function(name) {
    return new UnderlinedCell(new TextCell(name));
  });

  var body = data.map(function(row) {
    return keys.map(function(name) {
      var value = row[name];

      if (typeof value == 'number')
        return new RTextCell(String(value));
      else
        return new TextCell(String(value));
    });
  });

  return [headers].concat(body);
}
console.log(drawTable(dataTable(mountains)));
// Inheritance is a fundamental of object oriented tradition along with encapsulation
// and polymorphism. But unlike the others inheritance is somewhat regarded as a
// controversial tool.

// This is mainly due to it often being confused for polymorphism, sold as a more
// powerful tool than it really is, and overused in ugly ways. Polymorphism and
// encapsulation can be used to separate pieces of code from each other and reduce
// code tangle. Inheritance introduces tangles in the code by tying types together.

// Inheritance though isn't bad, it's misused, just see it as a slightly dodgy trick
// that helps define new types with little code, not as a grand organizing principle.
// Composition is the preferred way to extend types, like UnderlinedCell does by
// building on another cell object by storing it in a property and forwarding method
// calls to its own methods.

/// The instanceof Operator

// It's useful to know whether an object was derived from a specific constructor.
// That's where the instanceof operator comes in.
console.log(new RTextCell("A") instanceof RTextCell);
console.log(new RTextCell("A") instanceof TextCell);
console.log(new TextCell("A") instanceof RTextCell);
console.log([1] instanceof Array);
// The operator traverses inherited types. An RTextCell is an instance of TextCell
// because RTextCell.prototype derives from TextCell.prototype. The operator can
// be used on standard constructors like Array, almost all objects are instances
// of Object.

/// Summary

// Objects are more complicated than originally portrayed. They have prototypes
// which are other objects and will act like they have properties they don't have
// if the prototype has it. Simple objects have Object.prototype as their prototype.

// Constructors which are functions whose names start with capital letters by convention
// can be used with the new operator to create new objects. The new object's prototype
// will be the prototype of the constructor, you can make use of this by putting
// properties that all values of a given type share in their prototype. The instanceof
// operator can tell you if an object is an instance of a constructor.

// A useful thing for objects is defining a common interface for them and telling
// everyone they are supposed to interact with with your object through that interface.
// The rest of the details making up your object are encapsulated (hidden) behind
// the interface.

// Once you have an interface, other objects may implement this interface. Having
// other objects implement the interface and writing code that works with any object
// with the interface is called polymorphism. This is very useful.

// When implementing multiple types that differ only in some details, it's helpful
// to make the prototype of your new type derive from the prototype of your old type,
// and have your constructor call the old one. This gives you an object type that's
// similar to the old type but whiche you can add or override properties as you see
// fit.
