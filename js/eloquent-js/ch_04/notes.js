/// Eloquent Javascript Chapter 04: Data Structures: Objects and Arrays - Notes

// If numbers, booleans and strings are the building blocks of data structures, but
// in JavaScript, the Object allows you to group values, including other objects
// together and build more complex structures.

/// Data Sets

// Say we want to represent a collection of numbers in JavaScript, 2, 3, 5, 7, and, 11
// With what we know so far we could get creative with strings like "2 3 5 7 11",
// but this is awkward. JavaScript provides the Array type. Arrays are arbitrary sequences
// of ordered data. An array is written like so [1, 2, '3']. For example.
var listOfNumbers = [2, 3, 5, 7, 11];
// To access an array you write the name of the array variable, followed by an expression
// that evaluates to an index, with 0 being the first element.
console.log(listOfNumbers[1]);
console.log(listOfNumbers[1 - 1]);

/// Properties

// We've seen some examples of properties like myString.length or Math.max. These are
// expressions that access a property of some value. In the first we access the length
// property of the value myString and the second we access the property max from the
// Math object. Almost all JavaScript values have properties, except null and undefined.
// Accessing a property on them will result in a ValueError.

// You can access a property in one of two ways, either with a dot or in square brackets.
// foo.bar or foo[bar], they both access a property, but not necessarily the same property.
// When using the dot, the part after must be a valid variable name and it will directly
// fetch the property. But using the square brackets is an expression that will evaluate
// to a value which could be the right property or not. Also since property names
// can be any string, if you want to access one that does not have a valid variable
// name you will need to use the square brackets.

/// Methods

// Both string and array objects contain properties that refer to function values.
var doh = "Doh";
console.log(typeof doh.toUpperCase());
console.log(doh.toUpperCase());
// All strings have the toUpperCase property as well as the toLowerCase property.
// Properties that contain functions are called methods. Here's an example of array methods.
var mack = [];
mack.push('Mack');
mack.push('the', 'Knife');
console.log(mack);
console.log(mack.join(' '));
console.log(mack.pop());
console.log(mack);
// The push method adds values to the end of an array.
// Pop removes the last value of an array and returns it.
// Join will turn the array into a string with the supplied separator.

/// Objects

// Values of the type object are arbitrary collections of properties which can be
// added and removed at will. One way to create an object is with curly braces.
var day1 = {
  squirrel: false,
  events: [
    'work',
    'touched tree',
    'pizza',
    'running',
    'television'
  ]
};
console.log(day1.squirrel);
console.log(day1.wolf);
day1.wolf = false;
console.log(day1.wolf);
// Inside the curly braces we list the properties by putting its identifier, a colon
// then an expression that provides a value. If the name can't be expressed like a variable
// it must be quoted.
var descriptions = {
  work: 'went to work',
  'touched tree': 'Touched a tree'
};
// In JavaScript curly braces are used for both wrapping blocks of statements as well as
// to describe an object. A non-existent property will return undefined, you can assign
// values to properties with the = operator which will either replace the existing value
// or create the property and set if it doesn't exist.

// The octopus metaphor applies to objects as well, the properties are like variables
// and more than one property can point to the same value. So the octopus (object)
// can have multiple tentacles with names that point to values.

// The delete operator will remove a property from an object, it is a unary operator
// that isn't used often.
var anObject = {left: 1, right: 2};
console.log(anObject.left);
delete anObject.left;
console.log(anObject.left);
console.log('left' in anObject);
console.log('right' in anObject);
// The in operator is a binary operator that can be used on strings and objects will
// return a boolean value indicating whether the object has the property. The difference
// between deleting a property and setting its value to undefined is that `in` will
// return false if it's deleted. Arrays are just a special type of object for storing
// sequences. Think of them as long, flat octopuses with numbered tentacles.

/// Mutability

// In programming there are mutable and immutable objects. Strings, numbers, and booleans
// are immutable, meaning that it is impossible to change the value of the data,
// you can combine and derive new values from them, but you can't change it.
// You can't take the string 'cat' and change it to 'rat'.

// Objects on the other hand are mutable, you can actually modify the property values.
// With numbers, 120 and 120 are the same, even if they refer to different bits.
// Objects however can contain the same properties but be different.
var object1 = {value: 10};
var object2 = object1;
var object3 = {value: 10};

console.log(object1 == object2);
console.log(object1 == object3);
object1.value = 15
console.log(object2.value);
console.log(object3.value);
// Here object1 and object2 are both references to the same object, so changing
// object1 will reflect when accessing the object2 identifier. However, object3
// is a different object entirely. The == operator in JavaScript will only return
// true if the two objects compared are precisely the same, and false otherwise,
// even if the contents of two objects are the same, there's no "deep" comparison
// built in to JavaScript.

/// The Lycanthrope's Log

// The chapter example follows the tale of someone who turns into a squirrel but doesn't
// know why. He codes a journal system to try and correlate what he does with why
// he turns into a squirrel.

var journal = [];

function addEntry(events, didITurnIntoASquirrel) {
  journal.push({
    events: events,
    squirrel: didITurnIntoASquirrel
  });
}
// Every evening, he enters data into the journal.
addEntry(
  [
    'work',
    'touched tree',
    'pizza',
    'running',
    'television'
  ],
  false
);
addEntry(
  [
    'work',
    'ice cream',
    'cauliflower',
    'lasagna',
    'touched tree',
    'brushed teeth'
  ],
  false
);
addEntry(
  [
    'weekend',
    'cycling',
    'break',
    'peanuts',
    'beer'
  ],
  true
);
// With enough data points, he intends to compute correlation between events and
// whether he turned into a squirrel.

// Correlation is a measure of dependence between variables, usually expressed as
// a coefficient between -1 and 1. Zero means no correlation, one means perfect correlation
// and -1 means perfect opposite correlation (when one is true the other is false.)
// For binary variables the phi coefficient provides a good measure of correlation.
// To compute phi, we need a table `n` that observes the number of times the combinations
// of the two variables were observed.
// For example.
// No Squirrel, No Pizza, 76 -- No Squirrel, Pizza 9
// Squirrel, No Pizza, 4 -- Squirrel, Pizza 1
// The formula to compute phi with x representing squirrel and y representing pizza is
// phi = x1y1*x0y0 - x1y0*x0y1 / sqrt(total(x1)*total(y0)*total(y1)*total(x0))
// For our example this would be
// phi = ((1 * 76) - (4 * 9)) / sqrt(5 * 85 * 10 * 80)
// phi = 40 / sqrt(340000)
// phi = 0.069
// So eating pizza has little correlation with the transformation.

/// Computing Correlation

// We can represent a 2x2 table in JavaScript as an array with four elements.
// The function for computing phi is.
function phi(table) {
  return (table[3] * table[0] - table[2] * table[1]) /
    Math.sqrt((table[2] + table[3]) *
              (table[0] + table[1]) *
              (table[1] + table[3]) *
              (table[0] + table[2]))
}

console.log(phi([76, 9, 4, 1]));

// Here's the 3 month journal.
journal = require('./journal.js');
// To extract the 2x2 table for events we must loop over all entries and tally their
// occurences in relation to the transformation.
function hasEvent(event, entry) {
  return entry.events.indexOf(event) != -1;
}

function tableFor(event, journal) {
  var table = [0, 0, 0, 0];

  for (var i = 0; i < journal.length; i++) {
    var entry = journal[i], index = 0;

    if (hasEvent(event, entry)) index += 1;
    if (entry.squirrel) index += 2;
    table[index] += 1;
  }

  return table;
}

console.log(tableFor('pizza', journal));
// hasEvent tests whether an entry has an event by using the indexOf method of the
// array which returns -1 if the value you're trying find isn't in the array.

// The body of the for loop in tableFor determines which box in the table each
// entry falls into by checking for if the entry contains the event and if the
// event happens with a transformation, the loop then increments the table element
// that corresponds to the event.

/// Objects as Maps

// A way to store the correlations is to use object properties named after the event
// types, we can use the square bracket notation to create and read properties and
// `in` to test for property existence.
var map = {}
function storePhi(event, phi) {
  map[event] = phi;
}

storePhi('pizza', 0.069);
storePhi('touched tree', -0.081);
console.log('pizza' in map);
console.log(map['touched tree']);
// A map is a way to go from values in one domain to corresponding values in another.
// Since properties don't form a predictable series like an array, a normal for loop
// doesn't work. Instead we must a for in loop.
// Example.
for (var event in map) {
  console.log(`The correlation for '${event}' is ${map[event]}`);
}

/// The Final Analysis

// To process all events, and gather their correlations we combine the steps.
function gatherCorrelations(journal) {
  var phis = {};

  for (var entry = 0; entry < journal.length; entry++) {
    var events = journal[entry].events;

    for (var i = 0; i < events.length; i++) {
      var event = events[i];
      if (!(event in phis)) {
        phis[event] = phi(tableFor(event, journal));
      }
    }
  }

  return phis;
}

var correlations = gatherCorrelations(journal);
console.log(correlations.pizza);
// The results...
for (var event in correlations) {
  console.log(`${event}: ${correlations[event]}`);
}
// Lots of low correlations, filter for greater than 0.1 or less than -0.1
console.log('---------------------');
for (var event in correlations) {
  var correlation = correlations[event];
  if (correlation > 0.1 || correlation < -0.1){
    console.log(`${event}: ${correlation}`);
  }
}
// Two events have strongest correlation.
// Trying something...
for (var i = 0; i < journal.length; i++) {
  var entry = journal[i];

  if (hasEvent('peanuts', entry) && !hasEvent('brushed teeth', entry)) {
    entry.events.push('peanut teeth');
  }
}
console.log(phi(tableFor('peanut teeth', journal)));
// 100% correlation between eating peanuts and not brushing teeth with turning to
// a squirrel.

/// Further Arrayology

// Let's cover some additionaly array methods.
// push and pop add and remove from the end of an array.
// shift and unshift add and remove from the front of an array.
var todoList = [];

function rememberTo(task) {
  todoList.push(task);
}

function whatIsNext() {
  return todoList.shift();
}

function urgentlyRememberTo(task) {
  todoList.unshift(task);
}

rememberTo('eat food');
rememberTo('buy clothes');
console.log(todoList);
urgentlyRememberTo('take over world');
console.log(todoList);
// This program manages a todo list. You can add to the end of the list with rememberTo
// which uses the push method and the urgentlyRememberTo method adds to the front of
// the list using the unshift method. The whatIsNext method uses shift to return
// the next item (first item) in the array using shift.

// indexOf has a sibling called lastIndexOf, which searches from the end of the array
// instead of the front, both can take a second argument that's the start point of
// the search.
console.log([1, 2, 3, 2, 1].indexOf(2));
console.log([1, 2, 3, 2, 1].lastIndexOf(2));
// Arrays also have the slice method that take a start (inclusive) and end index (exclusive)
// and returns an array containing the elements between those.
console.log([0, 1, 2, 3, 4].slice(2, 4));
console.log([0, 1, 2, 3, 4].slice(2));
// If there's no end index, it will slice to the end of the array.
// The concat method will join arrays like the + operator for strings.
// Here's an example that uses slice and concat together.
function remove(array, index) {
  return array.slice(0, index).concat(array.slice(index + 1));
}

console.log(remove(['a', 'b', 'c', 'd', 'e'], 2));

/// Strings and Their Properties

// Because strings are immutable you can't add additional properties to them, JavaScript
// will fail silently but it won't add the property.
var myString = 'Fido';
myString.myProperty = 'value';
console.log(myString.myProperty);
// Strings have the slice and indexOf methods like arrays, though indexOf can take
// a string that's longer than a single character.
console.log('coconuts'.slice(4, 7));
console.log('coconuts'.indexOf('u'));
console.log('one two three'.indexOf('ee'));
// The trim method removes whitespace from the beginning and end of a string.
console.log('    okay \n'.trim());
// Strings also have the length property, as well as the charAt method which takes
// an expression that evaluates to an index and returns the character at that index.
var string = 'abc';
console.log(string.length);
console.log(string.charAt(0));
console.log(string[1]);

/// The Arguments Object

// Functions have a special variable named arguments added to their scope, it's an
// object containing all the arguments passed to the function. This is how JavaScript
// allows you to pass less or more arguments than the function should take.
function noArguments() {
  console.log(arguments);
}
noArguments(1, 2, 3);
function threeArguments(a, b, c) {
  console.log(arguments);
}
threeArguments();
// The arguments object is array like, in that it names each property starting from
// zero and incrementing, but it's not an array, it has a length property, but none
// of the nice methods of arrays (like indexOf, slice, etc.)
function argumentCounter() {
  console.log(`You gave me ${arguments.length} arguments.`);
}
argumentCounter('Straw man', 'Tautology', 'Ad hominem');
// Some functions take any number of arguments, like console.log, usually this is
// done by looping over the arguments object. Like we could have written the addEntry
// function to utilize this system like so.
function addEntry(squirrel) {
  var entry = {events: [], squirrel: squirrel};

  for (var i = 1; i < arguments.length; i++) {
    entry.events.push(arguments[i]);
  }
  journal.push(entry);
}
addEntry(true, 'work', 'touched tree', 'pizza', 'running', 'television');

/// The Math Object

// The Math object is used as a namespace for a collection of math related properties
// and methods, this is done so as to not pollute the global namespace. This is
// especially troublesome in JavaScript because if you shadow a pre-existing name
// JavaScript will neither warn you nor error out.

// The Math object has some trigonometry helpers, cos, sin, tan, acos, asin, and, atan.
// You also have access to Pi with Math.PI.
function randomPointOnCircle(radius) {
  var angle = Math.random() * 2 * Math.PI;

  return {x: radius * Math.cos(angle),
          y: radius * Math.sin(angle)};
}
console.log(randomPointOnCircle(2));
// The Math.random function returns a new pseudorandom number between 0 (inclusive)
// and 1 (exclusive), each time it's called.
console.log(Math.random());
console.log(Math.random());
console.log(Math.random());
// Computers are deterministic and can't produce truly random numbers, however through
// certain processes we can get something that's random enough for most purposes.
// To get a random whole number, we can use Math.floor on the result of Math.random.
console.log(Math.floor(Math.random() * 10));
// Multiplying the random number by 10 gives us a number >= 0 and < 10.
// There's also Math.ceil and Math.round which either round up or to the nearest
// whole number.

/// The Global Object

// The global scope, the place where global variables live can be accessed as an object.
// Each global variable is present in the window object in browsers.
var myVar = 10;
module.exports = {}
module.exports['myVar'] = myVar;
// Browser...
// console.log('myVar' in window);
// console.log(window.myVar);

/// Summary

// Objects and Arrays provide ways to group several values into a single value.
// Most values in JavaScript have properties, except null and undefined. You can
// access a property via either value.propName or value['propName'], objects tend
// to use name for their properties and store fixed sets of them. Arrays use numbers
// to order their values starting from 0. Methods are properties that are functions.
// Objects can also serve as maps, associating values with names. The in operator
// can be used to test membership of a property in an object as well as in a for loop
// to loop over the properties of an object.
