# Eloquent JavaScript Chapter 12: JavaScript and the Browser - Notes

Let's cover JavaScript and the web. Web tech has been from the start, decentralized. Various browser vendors have added functionality in ad-hoc ways that ended up being adopted and set as standards.

This is a two sided coin. On one hand it's good, to not have centralized control of a system and instead an open collaboration. On the other hand though this way the web has been developed means the system isn't very internally consistent and can be messy and confusing.

## Networks and the Internet

Computer networks have been around since the 50s, put cables between 2+ computers that allow them to send data back and forth and you have a network.

If we can do awesome stuff by connecting two machines in the same building, what happens if we do it globally? We get the Internet. To effectively communicate at this scale we needed standard ways to communicate. Enter network protocols. These describe different data communication methods, email, sharing files, remote computer access, etc. Protocols build on top of each other, the TCP (Transmission Control Protocol) is the most common. It standardized a way to ensure correct destination and order of data. It works like this: one computer listens for other computers to talk to it. Each listener has a port number associated with it, protocols have default standards. For example port 25 is email over SMTP. Another computer then can connect to this port. This computer is the client and the listener is the server. This is a 2 way data stream between the machines and TCP provides a nice network abstraction.

## The Web

The World Wide Web, a subset of the Internet, is a set of protocols and formats that let us visits sites in browsers. The Web in the name refers to the linking that can easily be done between sites. To connect a server to the Internet you need to listen on port 80 using the HTTP (HyperText Transfer Protocol), this protocol lets computers request documents over the network.

Documents on the web are identified by their URL (Uniform Resource Locator) that looks like:

```
http://eloquentjavascript.net/12_browser.html
|     |                      |              |
protocol       server              path
```

The first part is the protocol, HTTP in this case (if it's encrypted it would be `https://`), then comes the server's identifier. Last is a path string that identifies the document we want. Every machine connected to the internet gets an IP address that looks like `37.187.28.82`, these can be used directed for the server part of the URL but that's awkward and hard to remember. So you can register a *domain name* to point to your server(s), such as `eloquentjavascript.net` pointing to servers the author controls.

If you type the URL above in, your browser will take you to the page. It does it by first finding out what IP address `eloquentjavascript.net` points to, then it connects over HTTP, and finally it requests the `12_browser.html` document.

## HTML

HTML (HyperText Markup Language) is the document format of the web. HTML documents can contain text, links, images, videos and other media. Here's a simple HTML document.

```html
<!DOCTYPE html>
<html>
<head>
    <title>My Home Page</title>
</head>
<body>
    <h1>My Home Page</h1>
    <p>Hi I'm Will, this is my home page.</p>
    <p>Neat huh?</p>
</body>
</html>

```

The tags (`<html>`, etc) describe the structure of the document, and the text within is just normal text.

The first line `<!DOCTYPE html>` tells the browser to interpret the file as *modern* HTML instead of older HTML standards.

HTML documents have a head and a body, the head contains info about the document while the body contains the contents. In the example we declare the title of the site to be "My Home Page" in the head and then in the body we have a heading (`<h1>`, i.e. heading 1, there's `<h1>`-`<h6>`) and two paragraphs, `<p>`.

There are many other types of tags for images (`<img>`), video and more. Most tags have a opening `<html>` and closing `</html>` tag. They may also contain additional info in the form of attributes like this anchor (the anchor is used for linking) tag, the href (hypertext reference) says what to link to `<a href="http://eloquentjavascript.net/12_browser.html"`.

An example of a tag that doesn't require an end tag would be the image tag. Like this `<img src="http://example.com/coolpic.jpg">`, that would display the image from the source URL.

To be able to include angle brackets in your document without them being interpreted as a tag, you must use a notation like `&lt;` (less than) and `&gt;` (greater than) for the two brackets. In HTML, an ampersand, followed by some characters and a semicolon (like `&gt;`) is called an *entity* and is replaced by the character it encodes.

This is just like the backslash escaping done in JavaScript, since this gives ampersands special meaning you have to escape them with `&amp;`, inside an attribute value wrapped in double quotes you can use `&quot;` to place a quote character.

Browsers are very error tolerant of HTML due to legacy history reasons. This means if some tags are missing the browser will act like they are there. This document is equivalent to the one from before.

```html
<!DOCTYPE html>

<title>My Home Page</title>

<h1>My Home Page</h1>
<p>Hi I'm Will, this is my home page.
<p>Neat huh?
```

The `<html>`, `<head>`, and `<body` tags are gone, yet the browser renders fine. It knows the `<title>` goes in the `<head>` and `<h1>` and `<p>`'s in the body, you can also drop the end paragraph tag if there is another paragraph after or the end of the document.

In the examples the doctype isn't included, pretend it's there and for brevity same goes for head/body.

## HTML and JavaScript

Relative to this book the most important HTML tag is `<script>`, this lets us include JavaScript in the document.

```html
<h1>Testing Alert</h1>
<script>alert("Hello!");</script>
```

This script runs as soon as the browser hits the line in the document the tag is on. This would pop up an alert after loading.

Including large programs directly in the `<script>` tag isn't practical so you can pass an `src` attribute just like images to point to a JavaScript file.

```html
<h1>Testing Alert</h1>
<script src="code/hello.js"></script>
```

The `code/hello.js` file would just include the `alert()` function call like before and when the browser fetches the JavaScript file it will include it immediately after into the document. Script tags always need closing tags even if there is no content within and just a linked source, forget this and the browser treats the rest of the document as JavaScript. Some attributes can also contain JavaScript code. The `<button>` tag has an `onclick` attribute that can be set to a string of JavaScript code.

```html
<<button onclick="alert('BOOM');">DO NOT PRESS</button>
```

We used single quotes within because we had wrapping quotes. We could have used `&quot;` but that's extra typing and harder to read.

## In the Sandbox

Running programs downloaded from the Internet has risks, you don't know the operators of websites and they could do malicious things like hiding malware in the program. But part of the attraction of the web is that you can surf it without trusting all the pages you visit. Browsers limit what JavaScript programs can do because of this.

Isolating a program environment like this is called **sandboxing**, the hard part of sandboxing is giving the program enough privileges without incurring security risks. There is lots of potential for misuse of useful functionality like reading from the clipboard without sandboxing.

Every so often a new exploit appears that circumvents browser security and the sandbox. This is a cat and mouse game and the browser vendors will update their software to take care of this exploit, but hackers constantly look for other exploits so this is constant battle.

## Compatability and the Browser Wars

In the beginning Mosiac was the dominant browser, then Netscape came along and took the crown. Then Microsoft gave the web the worst thing there was, Internet Explorer 6. Whenever there was a dominant browser though, they took it upon themselves to create new features for the web with no regard for compatability with other browsers. The spec was mostly ignored.

This was the dark ages, for a long time being a web developer was much tougher than it needed to be. After Netscape died out, a non-profit offshoot called Mozilla came to be, the introduced the world to Firefox, and by adding more support to the browser for the HTML specifications it was able to take a large chunk of use from Internet Explorer. Soon after Google introduced Chrome, and Apple's Safari also gained popularity and compatability.

This competitive landscape of browsers that push the specifications forward has lead Microsoft to get it's situation in line and release Edge, it's a completely new browser that is built to be standards compliant as well.

All this is great, but there are still things that pop up. Corporations often lock systems down to old versions, forcing developers to work to older versions of browsers with less support for HTML specification features. We're going to go over working with the "greenfield" browsers, meaning usually the latest version + the last two in most cases.
