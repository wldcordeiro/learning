# Eloquent JavaScript Chapter 14: Handling Events - Notes

Working with direct user input like mouse or keyboard interaction, requires a different approach to working with control flow than we've used because the timing and order of these inputs can't be predicted ahead of time.

## Event Handlers

Browsers allow us to register functions as handlers for specific events. This is an improvement over past systems where you had to read the current state of say a key press constantly in primitive machines and it would be dangerous to do other intensive tasks if you didn't want to miss key presses.

A step above this primitive level would be for the hardware or operating system to place key presses in a queue that a program could periodically poll but the time between between the key being pressed and the program polling would make the software feel unresponsive. The handlers we have give us a way to address the problems of primitive systems and polling by lettings us react!

Below the `addEventListener` function registers the second argument to be called whenever the event provided in the first argument occurs.

```html
<p>Click this document to activate the handler.</p>
<script>
  addEventListener('click', function() {
    console.log('You clicked!');
  });
</script>
```

## Events and DOM Nodes

Browser event handlers are registered in a context, when you use `addEventListener` like before you're calling it on the whole `window` object because that's the global context of a browser. Every DOM element has its own `addEventListener`

```html
<button>Click Me</button>
<p>No handler here.</p>
<script>
  var button = document.querySelector('button');
  button.addEventListener('click', function() {
    console.log('Button clicked!');
  });
</script>
```

Here we add an event handler to the button, which is triggered on clicks to the button node but not the rest of the document. Giving a node an `onclick` attribute works similarly but a node only has one `onclick` whereas you can have any number of handlers with the `addEventListener` method. Its mirror `removeEventListener`, does what its named.

```html
<button>Act-once Button</button>
<script>
  var button = document.querySelector('button');
  function once() {
    console.log('Done');
    button.removeEventListener('click', once);
  }
  button.addEventListener('click', once);
</script>
```

To be able to unregister a handler function it must have a name (`once` in the example above), so it can be passed to both `addEventListener` and `removeEventListener`.

## Event Objects

Event handler functions are passed the Event as an argument, which gives us additional info about the event. Here in this example we use the `which` attribute on the event object to get which mouse button is used when pressing.

```html
<button>Click me any way you want</button>
<script>
  var button = document.querySelector('button');
  button.addEventListener('mousedown', event => {
    if (event.which == 1) {
      console.log('Left button');
    } else if (event.which == 2) {
      console.log('Middle button');
    } else if (event.which == 3) {
      console.log('Right button');
    }
  })
</script>
```

Different events have different attributes and methods, and we'll go over those, the Event object's `type` attribute always has a string to identify the event, like `mousedown`, or `click`.

## Propagation

Event handlers on parent nodes will receive some events that happen in the children, if a button in a paragraph is clicked, the parent's handler will also receive the event. If both the child and parent have handlers, the more specific handler wins. Events propagate outwardly from the node the event fired on to its parent and up to the document root. After all the handlers in the document have fired, any registered on the `window` would fire.

Event handlers can call the `stopPropagation` method which prevents further up handlers from receiving the event. Which is useful for things like when you want only an inner element's handlers to activate and not containing elements' event handlers.

The example makes handlers for 'mousedown' events on a button and its containing paragraph, when right clicked the button's handler will fire alone by calling `stopPropagation`, otherwise the event will propagate.

```html
<p>A paragraph with a <button>button</button>.</p>
<script>
  var para = document.querySelector('p'),
      button = document.querySelector('button');
  para.addEventListener('mousedown', event => {
    console.log('Handler for paragraph');
  });
  button.addEventListener('mousedown', event => {
    console.log('Handler for button');
    if (event.which == 3) {
      event.stopPropagation();
    }
  });
</script>
```

Event objects usually have a `target` property, this property refers to the originating node of  the event. It's useful to ensure you're handling the event you wanted.

You can also use `target` to more widely capture events, for example if you had a a node with a long list of buttons, you could register a single click handler and use `target` to figure out whether a button was clicked, instead of handlers for each button.

```html
<button>A</button>
<button>B</button>
<button>C</button>
<script>
  document.body.addEventListener('click', event => {
    if (event.target.nodeName == 'BUTTON') {
      console.log('Clicked', event.target.textContent);
    }
  });
</script>
```

## Default Actions

Events usually have default actions, clicking links take you to the link's target, pressing the down arrow will scroll the browser, etc. For most events the JavaScript event handlers get called first, followed by the default. If you don't want the default action you can call the `preventDefault` method on the event.

This can be used to implement your own keyboard shortcuts or be used to interfere with expected behavior, like in this example where the link can't be followed.

```html
<a href="https://developer.mozilla.org">MDN</a>
<script>
  const link = document.querySelector('a');
  link.addEventListener('click', event => {
    console.log('Nope.');
    event.preventDefault();
  });
</script>
```

Unless you have a good reason to do this, don't. You can make your websites a very unpleasant experience this way. Also depending on the browser certain events can't be prevented, like in Chrome you can't handle the keyboard shortcut for closing the window (ctrl/cmd + w).

## Key Events

Pressing keys on the keyboard will fire the `keydown` event and releasing the key fires `keyup`.

```html
<p>This page turns violet when you hold the V key.</p>
<script>
  addEventListener('keydown', event => {
    if (event.keyCode == 86) {
      document.body.style.background = 'violet';
    }
  });
  addEventListener('keyup', event => {
    if (event.keyCode == 86) {
      document.body.style.background = '';
    }
  });
</script>
```

The `keydown` event will continue firing when a key is held down. Sometimes you don't want this, like in a game where you increase acceleration of a character by pressing an arrow key and decrease on release you need to make sure not to increase it again when the key repeats to avoid unintentional large values.

In the example we used the `keyCode`, which can identify the key, but it isn't always obvious how to translate the key code to an actual key. For letters and numbers the key code will be the Unicode character code associated with the number or (uppercase) letter. We can use the string `charCodeAt` method to figure this code out.

```javascript
console.log('Violet'.charCodeAt(0));
// -> 86
console.log('1'.charCodeAt(0));
// -> 49
```

Other keys aren't as straightforward, and the best way to find the codes is by experimenting, create a key handler and log the key codes. Modifier keys like Ctrl, Alt, Shift, Meta (Command on Mac) have key code like normal keys, but when looking for key combinations you can look at the `ctrlKey`, `altKey`, `metaKey`, and `shiftKey` properties of keyboard and mouse events.

```html
<p>Press Ctrl-Space to continue.</p>
<script>
  addEventListener('keydown', event => {
    if (event.keyCode == 32 && event.ctrlKey) {
      console.log('Continuing!');
    }
  });
</script>
```

`keyup` and `keydown` tell you about the physical keys being pressed, but if you're interested in the text being typed the `keypress` event is what you want. It fires after `keydown` and repeats with it but only for keys that produce character input. You can use the `charCode` property on the event combined with the `String.fromCharCode` function to produce the single character string input.

```html
<p>Focus this page and type something.</p>
<script>
  addEventListener('keypress', event => {
    console.log(String.fromCharCode(event.charCode));
  });
</script>
```

Depending on what element has focus, the originating DOM node of a key event can differ. Normal nodes can't have focus without setting the `tabindex` attribute on them, but links, buttons, and form fields can. If nothing has focus, the `document.body` is the default target for key events.

## Mouse Clicks

There's a number of events associated with a mouse click, you have `mousedown` and `mouseup` that fire when you press and release the button. These happen on the DOM node beneath the cursor. After `mouseup` fires we then have the `click` event on the most specific node that contained both the press and release. If you clicked down on one paragraph and released on another, the `click` event will happen on the containing element of the two. If two clicks happen close enough together a `dblclick` event will fire after the second `click`.

Using the `pageX` and `pageY` properties of mouse events will let you know precisely where it happened relative to the top-left of the document in pixel coordinates. Here is an example of a primitive drawing tool that adds dots where you click.

```html
<style>
  body {
    height: 200px;
    background: beige;
  }
  .dot {
    height: 8px;
    width: 8px;
    border-radius: 4px; /* round corners */
    background: blue;
    position: absolute;
  }
</style>
<script>
  addEventListener('click', event => {
    const dot = document.createElement('div');
    dot.className = 'dot';
    dot.style.left = `${event.pageX - 4}px`;
    dot.style.top = `${event.pageY - 4}px`;
    document.body.appendChild(dot);
  });
</script>
```

The `clientX` and `clientY` properties are like the `pageX` and `pageY` properties but relative to the part of the document scrolled into view. These are useful for comparing mouse coordinates from something like `getBoundingClientRect` which returns viewport-relative coordinates also.

## Mouse Motion

Moving the mouse fires the `mousemove` event, which is useful for tracking the mouse position. Here's an example that displays a bar and sets up handlers so dragging left or right on the bar changes its width.

```html
<p>Drag the bar to change its width:</p>
<div style="background: orange; width: 60px; height: 20px;">
</div>
<script>
  let lastX;
  const rect = document.querySelector('div');
  rect.addEventListener('mousedown', event => {
    if (event.which == 1) {
      lastX = event.pageX;
      addEventListener('mousemove', moved);
      event.preventDefault(); // Prevent selection.
    }
  });

  function buttonPressed(event) {
    if (event.buttons == null) {
      return event.which != 0;
    } else {
      return event.buttons != 0;
    }
  }

  function moved(event) {
    if (!buttonPressed(event)) {
      removeEventListener('mousemove', moved);
    } else {
      const dist = event.pageX - lastX;
      const newWidth = Math.max(10, rect.offsetWidth + dist);
      rect.style.width = `${newWidth}px`;
      lastX = event.pageX;
    }
  }
</script>
```

We register the `mousemove` event listener on the whole window so we can keep updating the bar size until  the mouse is released. This has its own challenges though because browsers don't all set the `which` property of `mousemove` events to anything useful and some support the standard `buttons` property but not `which` or vice-verse. So in the example we create `buttonPressed` to handle both possible situations.

When you move the mouse in or out of a node a `mouseover` or `mouseout` event fires. You can use these events create hover effects and other things. But it isn't just starting on `mouseover` and ending on `mouseout` events as `mouseout` events will fire if you move the mouse from a parent node to a child. Couple this with event propagation and you'll receive `mouseout` when the mouse leaves the child too.

We can get around this by using the `relatedTarget` property on the event as on `mouseover` it'll tell us what element the pointer was over and `mouseout` will tell you the element you're moving to. We want to change the hover effect when the `relatedTarget` is outside our target, only then does it represent crossing from outside to in the node and vice-verse.

```html
<p>Hover over this <strong>paragraph</strong>.</p>
<script>
  const para = document.querySelector('p');
  function isInside(node, target) {
    for (; node != null; node = node.parentNode) {
      if (node == target) {
        return true;
      }
    }
  }
  para.addEventListener('mouseover', event => {
    if (!isInside(event.relatedTarget, para)) {
      para.style.color = 'red';
    }
  });
  para.addEventListener('mouseout', event => {
    if (!isInside(event.relatedTarget, para)) {
      para.style.color = '';
    }
  });
</script>
```

`isInside` takes the given node and follows its parent link till it reaches document root (node == null) or the parent we're looking for. But this example is silly because it can be achieved in a single CSS rule using the `:hover` pseudoselector.

```html
<style>
  p:hover {
    color: red;
  }
</style>
<p>Hover over this <strong>paragraph</strong>.</p>
```

## Scroll Events

When an element is scrolled, a `scroll` event fires, you can use this for various things, like tracking user progress through a document. In the example we do just that and display a progress bar in the top right of the page.

```html
<style>
  .progress {
    border: 1px solid blue;
    width: 100px;
    position: fixed;
    top: 10px;
    right: 10px;
  }

  .progress > div {
    height: 12px;
    background: blue;
    width: 0%;
  }

  body {
    height: 2000px;
  }
</style>
<div class="progress"><div></div></div>
<p>Scroll me...</p>
<script>
  const bar = document.querySelector('.progress div');
  addEventListener('scroll', () => {
    const max = document.body.scrollHeight - innerHeight;
    bar.style.width = `${(pageYOffset / max) * 100}%`
  });
</script>
```

So to start let's cover some CSS stuff, setting an element's `position` to `fixed` is like setting it to `absolute` only it doesn't scroll with the document. We have a div inside the progress bar that we resize to show progress. We use `%` over `px` to get relative sizing.

The global `innerHeight` will gives us the window height and we then subtract it from the scrollable height. `innerWidth` also exists to compliment `innerHeight`. The current scroll position is available in the `pageYOffset` global, we divide it by the maximum scroll position and multiply by 100 to get our percentage for the progress bar. `preventDefault` won't prevent scrolling. Event handlers aren't fired till after the scrolling occurs.

## Focus Events

`focus` and `blur` events are fired when an element gains or loses focus. These events don't propogate like the others and parent handlers don't receive the event. The window object receives focus and blur events when you move between tabs or window containing the document.

Here's an example that shows help text based on element focus.

```html
<p>Name: <input type="text" data-help="Your full name"></p>
<p>Age: <input type="text" data-help="Age in years"></p>
<p id="help"></p>
<script>
  const help = document.querySelector('#help');
  const fields = document.querySelectorAll('input');
  for (let i = 0; i < fields.length; i++) {
    fields[i].addEventListener('focus', event => {
      const text = event.target.getAttribute('data-help');
      help.textContent = text;
    });
    fields[i].addEventListener('blur', event => {
      help.textContent = '';
    });
    });
  }
</script>
```

## Load Event

When the page finishes loading the `load` event is fired on the window and document body objects. Often this is used to schedule actions that require the whole document to have been built. `<script>` tags are run immediately when they're encountered which can be too soon, like when the script needs to do something with parts of the document that appear after it.

Elements like `<script>` and `<img>` also have a `load` event to indicate when the file they reference has finished loading, like `focus` events, `load` events don't propagate. When a page is closed or navigated away from, a `beforeunload` event is fired, the main use of this event is to prevent data lose when a user closes a document. Unlike what you'd expect you don't use `preventDefault` to prevent a page from unloading, instead you return a string from the `beforeunload` handler that will be used in a dialog that asks the user to confirm they want to leave the page, even if a script is running trying to keep the user there.

## Script Execution Timeline

Many things can trigger script execution like a `<script>` tag being read, an event firing, or even `requestAnimationFrame` being called. An important thing to understand is that even with events firing at any time, no two scripts in a single document ever run at the same time. If a script is running, event handlers and other code will have to wait, this is why browsers freeze when running a script for a long time, the browser can't react to other events inside the document because it's waiting for the current script to finish.

Some programming environments **do** allow multiple *threads of execution* at the same time, which can make a program run faster. But when there are multiple actors touching the same parts of a system at the same time, thinking about a program becomes at least an order of magnitude harder.

JavaScript programs only doing one thing at a time makes our lives easier. But in cases where you really do want to do something in the background without freezing the page, browsers have something called *web workers*. Workers are isolated JavaScript environments that run alongside the main thread and can communicate with it only by sending and receiving messages.

Assume we have this in `code/squareworker.js`

```javascript
addEventListener('message', event => {
  postMessage(event.data * event.data);
});
```

Pretend that squaring a number is a heavy, long-running computation that we want to perform in the background. This code spawns a worker, sends some messages and outputs the responses.

```html
<script>
  const squareWorker = new Worker('squareworker.js');
  squareWorker.addEventListener('message', event => {
    console.log(`The worker responded: ${event.data}`);
  });
  squareWorker.postMessage(10);
  squareWorker.postMessage(24);
</script>
```

The `postMessage` function sends a message to the worker, which fires a "message" event in the receiver. The script that created the worker sends and receives messages through the `Worker` object, while the worker talks to the script that created it by sending and listening directly in its global scope that is unique to it and not shared with its originating script.

## Setting Timers

`setTimeout` is similar to `requestAnimationFrame`, it schedules another function to be called later, but instead of calling the function at the next redraw, it waits a given amount of milliseconds.

Simple example changes page from blue to yellow.

```html
<script>
  document.body.style.background = 'blue';
  setTimeout(() => document.body.style.background = 'yellow', 2000);
</script>
```

Sometimes you need to cancel a scheduled function, you do this by storing the value returned by `setTimeout` and calling `clearTimeout` on it.

```javascript
const bombTimer = setTimeout(() => console.log("BOOM"), 500);

if (Math.random() < 0.5) { // 50% chance.
  console.log('Defused.');
  clearTimeout(bombTimer);
}
```

`cancelAnimationFrame` works similarly to `clearTimeout`, it will cancel that frame (assuming it hasn't already been called).

Similarly there is `setInterval` and `clearInterval` to set timers that should repeat every `n` milliseconds.

```javascript
let ticks = 0,
const clock = setInterval(() => {
  console.log('tick', ticks++);
  if (ticks == 10) {
    clearInterval(clock);
    console.log('stop.');
  }
  }, 200);
```

## Debouncing

Some events have the potential to fire a rapidly, like "mousemove" or "scroll", and when you handle these events you need to take care to not do anything too time consuming as you could make the document feel slow or choppy. If you need to do non-trivial work in a handler of this type you can use `setTimeout` to make sure you aren't doing it too often. A practice known as "debouncing" the event. There are a few approaches to doing this.

In this example we want to do something when the user has typed something, but not for every key event, when they're typing quickly we want to wait until a pause occurs. Instead of immediately performing an action in the handler, we set a timeout, clearing any pre-existing timeouts (if any) so that when events occur close together (closer than the delay), the previous timeout is canceled.

```html
<textarea></textarea>
<script>
  const textarea = document.querySelector('textarea');
  let timeout;
  textarea.addEventListener('keydown', () => {
    clearTimeout(timeout);
    timeout = setTimeout(() => console.log('You stopped typing.'), 500);
  });
</script>
```

Giving `clearTimeout` an undefined value or a timeout that already occurred does nothing so we don't need to check for an existing timeout.

With a slightly different pattern we can space responses so that they're by at least a certain length of time but during a series of events, not just after. For example, we may want to respond to `mousemove` events and show the mouse coordinates, but only every 250 milliseconds.

```html
<script>
  function displayCoords(event) {
    document.body.textContent = `Mouse at ${event.pageX}, ${event.pageY}`;
  }

  let scheduled = false, lastEvent;
  addEventListener('mousemove', event => {
    lastEvent = event;
    if (!scheduled) {
      scheduled = true;
      setTimeout(() => {
        scheduled = false;
        displayCoords(event);
      }, 250);
    }
  });
</script>
```

## Summary

We can detect and react to events using event handlers using the `addEventListener` method to register handlers.

Every event has a type (like "mousedown", "focus"), most events are called on specific DOM elements and propagated to that element's ancestors allowing handlers associated with those elements to handle them.

When a handler is called it's passed an event object with additional information about the event. This object has methods that let us stop propagation (`stopPropagation`) and prevent the browser's default event handling (`preventDefault`).

Pressing a key fires `keydown`, `keypress`, and `keyup`
Pressing a mouse button fires `mousedown`, `mouseup`, and `click`
Moving the mouse fires `mousemove`, `mouseenter`, and `mouseout`
Scrolling fires `scroll`
Element focus changes fire `focus` and `blur`
When the document finishes loading `load` is fired on the window

Only one part of a JavaScript program can run at a time so event handlers and other scheduled scripts need to wait until other scripts finish before they can execute.
