exports = (typeof window === 'undefined') ? global : window;

exports.functionsAnswers = {
  argsAsArray : function(fn, arr) {
    return fn.apply(null, arr);
  },

  speak : function(fn, obj) {
    return fn.call(obj);
  },

  functionFunction : function(str) {
    return function(second) {
      return `${str}, ${second}`;
    }
  },

  makeClosures : function(arr, fn) {
    var closures = [];

    for (let i = 0; i < arr.length; i++) {
      closures.push(fn.bind(null, arr[i]));
    }

    return closures;
  },

  partial : function(fn, str1, str2) {
    return fn.bind(null, str1, str2);
  },

  useArguments : function() {
    let results = 0;

    for (let i = 0; i < arguments.length; i++) {
      results += arguments[i];
    }

    return results;
  },

  callIt : function(fn) {
    var args = Array.prototype.slice.call(arguments, 1, arguments.length);
    return fn.apply(null, args);
  },

  partialUsingArguments : function(fn) {
    var args = Array.prototype.slice.call(arguments, 1, arguments.length);

    return function() {
      var args2 = Array.prototype.slice.call(arguments);
      return fn.apply(null, args.concat(args2));
    }
  },

  curryIt : function(fn) {
    var args = Array.prototype.slice.call(arguments, 1);

    return (n) => {
      return fn.apply(this, args.concat(
        Array.prototype.slice.call(arguments, 0)
      ));
    }
  }
};
