exports = (typeof window === 'undefined') ? global : window;

exports.arraysAnswers = {

  indexOf : function(arr, item) {
    return arr.indexOf(item);
  },

  sum : function(arr) {
    return arr.reduce(function(a, b) {
      return a + b;
    });
  },

  remove : function(arr, item) {
    return arr.filter(function(i) {
      return i != item;
    });
  },

  removeWithoutCopy : function(arr, item) {
    arr.forEach(function(i) {
      var index = arr.indexOf(item);
      if (index != -1)
        arr.splice(index, 1);
    });

    return arr;
  },

  append : function(arr, item) {
    arr.push(item);

    return arr;
  },

  truncate : function(arr) {
    arr.pop();

    return arr;
  },

  prepend : function(arr, item) {
    arr.unshift(item);

    return arr;
  },

  curtail : function(arr) {
    arr.shift();

    return arr;
  },

  concat : function(arr1, arr2) {
    return arr1.concat(arr2);
  },

  insert : function(arr, item, index) {
    arr.splice(index, 0, item);
    return arr;
  },

  count : function(arr, item) {
    return arr.filter(function(i) {
      return i == item;
    }).length;
  },

  duplicates : function(arr) {
    var elms = {};

    return [... new Set(arr.filter(function(item) {
      return item in elms ? elms[item] += 1 : elms[item] = 1;
    }).filter(function(item) {
      return elms[item] > 1;
    }))];
  },

  square : function(arr) {
    return arr.map(function(item) {
      return item * item;
    });
  },

  findAllOccurrences : function(arr, target) {
    return arr
    .map(function(item, index) {
      return index;
    }).filter(function(index) {
      return arr[index] == target;
    });
  }
};
