exports = (typeof window === 'undefined') ? global : window;

exports.numbersAnswers = {
  valueAtBit: function(num, bit) {
    return 1 & (num >> (bit - 1));
  },

  base10: function(str) {
    return parseInt(str, 2);
  },

  convertToBinary: function(num) {
    var bits = [];

    for (var i = 0; i < 8; i++) {
      bits.unshift(1 & (num >> i));
    }

    return bits.join('');
  },

  multiply: function(a, b) {

  }
};
