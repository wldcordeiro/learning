exports = (typeof window === 'undefined') ? global : window;

exports.asyncAnswers = {
  async : function(value) {
    return new Promise((resolve, _) => {
      return resolve(value);
    });
  },

  manipulateRemoteData : function(url) {
    function toObject(json) {
      return JSON.parse(json);
    }

    return new Promise((resolve, reject) => {
      var req = new XMLHttpRequest();

      req.open('GET', url);

      req.onload = done => {
        if (req.status == 200 || req.status == 304) {
          var peopleArray = toObject(req.response).people;
          resolve(peopleArray
            .map(person => person.name)
            .sort()
          );
        } else {
          reject(Error(req.statusText));
        }
      };

      req.onerror = function() {
        reject(Error("Network Error"));
      };

      req.send();
    });
  }
};
