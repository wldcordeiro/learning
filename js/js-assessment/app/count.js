exports = (typeof window === 'undefined') ? global : window;

exports.countAnswers =  {
  count : function (start, end) {
    var current;

    function wrapped() {
      console.log(start++);

      if (start <= end) {
        current = setTimeout(wrapped, 100);
      }
    }

    wrapped();

    return {
      cancel: function() {
        return current && clearTimeout(current);
      }
    }
  }
};
