new Vue({
  el: "#app",
  data: {
    message: "Hello Vue!",
    newTodo: "",
    todos: [
      { text: "Learn JavaScript!", priority: true },
      { text: "Learn Vue.js v1!" },
      { text: "Build awesome things!" },
    ],
  },
  methods: {
    addTodo() {
      const text = this.newTodo.trim();
      if (text) {
        this.todos.push({ text });
        this.newTodo = "";
      }
    },

    removeTodo(index) {
      this.todos.splice(index, 1);
    },

    togglePriority(index) {
      console.log(this.todos.length, this.todos, index);
      this.todos[index].priority = !this.todos[index].priority;
    },

    reverseMessage() {
      this.message = this.message.split(" ").reverse().join(" ");
    }
  }
});
