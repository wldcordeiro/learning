// Elements
const player = document.querySelector('.player'),
  video = player.querySelector('.viewer'),
  progress = player.querySelector('.progress'),
  progressBar = player.querySelector('.progress__filled'),
  toggle = player.querySelector('.toggle'),
  skipButtons = player.querySelectorAll('[data-skip]'),
  ranges = player.querySelectorAll('.player__slider'),
  fullscreen = player.querySelector('button[name=fullscreen]');

// Functions
function togglePlay() {
  if (video.paused) {
    video.play();
  } else {
    video.pause();
  }
}

function updateButton() {
  const icon = this.paused ? '►' : '❚ ❚';
  toggle.textContent = icon;
}

function skip() {
  video.currentTime += parseFloat(this.dataset.skip);
}

function handleRangeUpdate() {
  video[this.name] = this.value;
}

function handleProgress() {
  const percent = (video.currentTime / video.duration) * 100;
  progressBar.style.flexBasis = `${percent}%`;
}

function scrub(event) {
  const scrubTime = (event.offsetX / progress.offsetWidth) * video.duration;
  video.currentTime = scrubTime;
}

// Event listeners
video.addEventListener('click', togglePlay);
video.addEventListener('play', updateButton);
video.addEventListener('pause', updateButton);
video.addEventListener('timeupdate', handleProgress);

toggle.addEventListener('click', togglePlay);

skipButtons.forEach(button => button.addEventListener('click', skip));

let rangeFlags = {};
ranges.forEach(range => rangeFlags[range.name] = false);
ranges.forEach(range => range.addEventListener('change', handleRangeUpdate));
ranges.forEach(range => range.addEventListener('mousemove', event =>
  rangeFlags[range.name] && handleRangeUpdate(event)));
ranges.forEach(range => range.addEventListener('mousedown', () =>
  rangeFlags[range.name] = true));
ranges.forEach(range => range.addEventListener('mouseup', () =>
  rangeFlags[range.name] = false));

let mousedown = false;

progress.addEventListener('click', scrub);
progress.addEventListener('mousemove', event => mousedown && scrub(event));
progress.addEventListener('mousedown', () => mousedown = true);
progress.addEventListener('mouseup', () => mousedown = false);

let fullFlag = false;

function toggleFullscreen() {
  if (fullFlag) {
    fullFlag = false;
    document.exitFullscreen();
    return;
  }

  player.requestFullscreen();
  fullFlag = true;
}

fullscreen.addEventListener('click', toggleFullscreen);