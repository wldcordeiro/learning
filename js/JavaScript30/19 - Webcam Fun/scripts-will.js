const video = document.querySelector('.player');
const extVideo = document.querySelector('.video-src');
const canvas = document.querySelector('.photo');
const ctx = canvas.getContext('2d');
const strip = document.querySelector('.strip');
const snap = document.querySelector('.snap');

function canvasFullScreen() {
  this.requestFullscreen();
}

function getVideo(camera) {
  if (camera) {
    navigator.mediaDevices.getUserMedia({ video: true, audio: false })
      .then(localMediaStream => {
        video.src = window.URL.createObjectURL(localMediaStream);
        video.play();
      })
      .catch(err => console.log('OH NOEZ', err));
  } else {
    extVideo.play();
  }
}

function paintExtVideoToCanvas() {
  const { videoWidth: width, videoHeight: height } = extVideo;
  canvas.width = width * 2;
  canvas.height = height * 2;
  let globalAlpha = 0.1;
  ctx.drawImage(extVideo, 0, 0, width * 2, height * 2);
  let pixels = ctx.getImageData(0, 0, width * 2, height * 2);
  pixels = rgbSplit(pixels);
  pixels = alphaFiddle(pixels);
  ctx.putImageData(pixels, 0, 0);
  window.requestAnimationFrame(paintExtVideoToCanvas);
}

function paintToCanvas() {
  const { videoWidth: width, videoHeight: height } = video;
  canvas.width = width;
  canvas.height = height;
  let globalAlpha = 0.1;

  ctx.drawImage(video, 0, 0, width, height);
  let pixels = ctx.getImageData(0, 0, width, height);
  // pixels = radialEffect(pixels, width, height);
  pixels = rgbSplit(pixels);
  // pixels = greenScreen(pixels);
  // pixels = alphaFiddle(pixels);
  ctx.globalAlpha = globalAlpha;
  if (globalAlpha > 1) {
    globalAlpha += 0.2;
  } else {
    glbalAlpha = 0.1;
  }
  ctx.putImageData(pixels, 0, 0);
  window.requestAnimationFrame(paintToCanvas);
}

function takePhoto() {
  snap.currentTime = 0;
  snap.play();

  const data = canvas.toDataURL('image/jpeg');
  const link = document.createElement('a');
  link.href = data;
  link.setAttribute('download', 'handsome');
  link.innerHTML = `<img src="${data}", alt="Fetching Person">`;
  strip.insertBefore(link, strip.firstChild);
}

function radialEffect(pixels, width, height) {
  const pixWidth = width * 4, // 4 because RGBA per pixel
    pixHeight = height * 4,
    dataMidPoint = (0.5 * pixels.data.length),
    midPoint = dataMidPoint + (pixWidth / 2);

  // Build a grid... Need to figure out data-points to create a circle using
  // the circle equation.
  let multX = 128, multY = 24;
  for (let i = 0; i < pixels.data.length; i+= 4) {
    // Vertical grid lines
    if (i % multX === 0) {
      pixels.data[i + 1] = 255;
      multX += 128;
    }

    // Horizontal grid lines
    if (i % (pixWidth * multY) === 0) {
      for (let j = (pixWidth * multY); j < (pixWidth * multY) + pixWidth; j+= 4) {
        pixels.data[j + 1] = 255;
      }
      multY += 24;
    }
  }

  return pixels;
}

function redEffect(pixels) {
  for (let i = 0; i < pixels.data.length; i += 4) {
    pixels.data[i] = pixels.data[i] + 100; // RED
    pixels.data[i + 1] = pixels.data[i + 1] - 50; // GREEn
    pixels.data[i + 2] = pixels.data[i + 2] * 0.5; // BLUE
  }

  return pixels;
}

function rgbSplit(pixels) {
  for (let i = 0; i < pixels.data.length; i += 4) {
    pixels.data[i - 50] = pixels.data[i];
    pixels.data[i + 400] = pixels.data[i + 1];
    pixels.data[i - 550] = pixels.data[i + 2];
    pixels.data[i - 550] = pixels.data[i + 3];
  }
  return pixels;
}

function alphaFiddle(pixels) {
  let alphaVal = 32;
  let colorVal = 123;
  for (let i = 0; i < pixels.data.length; i += 4) {
    if (i % 255 === 0 || i % 127 == 0) {
      pixels.data[i + 0] = 0;
      pixels.data[i + 1] = 0;
      pixels.data[i + 2] = 0;
      pixels.data[i + 3] = 255;
      colorVal += 30;
    } else {
      pixels.data[i + 0] = pixels.data[i + 0] - 80;
      pixels.data[i + 1] = pixels.data[i + 1] + 90;
      pixels.data[i + 2] = pixels.data[i + 2] + 10;
    }
  }

  return pixels;
}

function greenScreen(pixels) {
  const levels = {};

  [...document.querySelectorAll('.rgb input')].forEach(input => {
    levels[input.name] = input.value;
  });

  for (let i = 0; i < pixels.data.length; i += 4) {
    let red = pixels.data[i],
      green = pixels.data[i + 1],
      blue = pixels.data[i + 2],
      alpha = pixels.data[i + 3];

    if (red >= levels.rmin && green >= levels.gmin && blue >= levels.bmin
      && red <= levels.rmax && green <= levels.gmax && blue <= levels.bmax) {
      pixels.data[i + 3] = 0;
    }

  }

  return pixels;
}

getVideo(true);

video.addEventListener('canplay', paintToCanvas);
// extVideo.addEventListener('canplay', paintExtVideoToCanvas);
canvas.addEventListener('click', canvasFullScreen);
