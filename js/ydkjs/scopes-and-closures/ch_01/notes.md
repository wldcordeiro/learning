# You Don't Know JS - Scope and Closures Chapter 01: What Is Scope? Notes

Scope refers to the set of rules that determine where to store variables and
how to retrieve them.

JS is (JIT) compiled
It goes through the following steps.

## Compiler Theory

### Tokenizing/Lexing

Difference between the two is if the tokens are identified in a stateful way.
If so then it is lexing.
Breaking statements into meaningful chunks.

#### Example:
`var a = 2;`

gets broken into var, a, =, 2, ;

### Parsing

Taking the tokens and turning it into an Abstract Syntax Tree
Example:

The tree for var a = 2;
Would look something like
```
VariableDeclaration
|                  |
Identifier = a     AssignmentExpression
                   |
                   NumericLiteral = 2
```

### Code Generation

This is the process of taking the AST and turning it into executable code.
Put simply there is a process that takes the AST and turns it into a set of machine
instructions that actually create a variable `a` and assign it to the value 2.

*This is a vast simplification and there are many more intermediate steps, but this
is a high level overview of the process.*

## Understanding Scope
We'll understand scope via the metaphor of a conversation.

### The Cast
These are the "characters" to process the `var a = 2;` statement.

* Engine: Responsible for start to finish compilation and execution of our code.
* Compiler: Handles the work of parsing and code generation of our code.
* Scope: Collects and maintains a lookup list of all declared identifiers (variables)
       and enforces a strict set of rules as to how these are accessible to
       currently executing code.

### Back and Forth
When you see `var a = 2;` you usually think of it as one statement, the engine sees
it as two. One it handles and one the compiler handles.
Compiler will lex the statement, then create an AST and then when it gets to
code generation it works slightly different than expected.
It first checks with the scope to see if a variable `a` already exists, if so
it ignores the declaration and moves on, otherwise scope declares a new variable `a`
Then the compiler produces code for the engine to execute. This code that engine
runs will first check the scope for an existing `a` and if found use it, otherwise
it looks elsewhere (nested scopes...)

To summarize: two distinct actions are taken for a variable assignment:

1. Compiler declares a variable (if not previously declared in the
current scope)
2. When executing, Engine looks up the variable in Scope and assigns to it, if found.

### Compiler Speak
The engine does a LHS (Left Hand Side) lookup for `a`
Then it does a RHS (Right Hand Side) of the assignment.
You could think of RHS as Retrieve His/Her Source.

#### Example:

`console.log(a);`
This would be considered RHS because no assignment is taking place.
it's better to conceptually think about it as: "who's the target of the
assignment (LHS)" and "who's the source of the assignment (RHS)"

#### Another example

```javascript
function foo(a) {
  console.log(a); // 2
}


foo(2);
```

This has both a RHS in the console.log(a) line, as well as a LHS through
the function invocation, since 2 is passed into the fuction, it bound to the
`a` identifier within the scope of foo.
In addition there is an LHS lookup of console and property resolution of log
for execution.

### If this were a conversation...
>Engine: Hey Scope, I have an RHS reference for foo. Ever heard of it?

>Scope: Why yes, I have. Compiler declared it just a second ago. He's a function. Here you go.

>Engine: Great, thanks! OK, I'm executing foo.

>Engine: Hey, Scope, I've got an LHS reference for a, ever heard of it?

>Scope: Why yes, I have. Compiler declared it as a formal parameter to foo just recently. Here you go.

>Engine: Helpful as always, Scope. Thanks again. Now, time to assign 2 to a.

>Engine: Hey, Scope, sorry to bother you again. I need an RHS look-up for console. Ever heard of it?

>Scope: No problem, Engine, this is what I do all day. Yes, I've got console. He's built-in. Here ya go.

>Engine: Perfect. Looking up log(..). OK, great, it's a function.

>Engine: Yo, Scope. Can you help me out with an RHS reference to a. I think I remember it, but just want to double-check.

>Scope: You're right, Engine. Same guy, hasn't changed. Here ya go.

>Engine: Cool. Passing the value of a, which is 2, into log(..).

>...

### Quiz

```javascript
function foo(a) {
    var b = a;
    return a + b;
}


var c = foo( 2 );
```

#### LHS Look-ups (3)
1. a is bound to 2 in the call to foo (a = )
2. b is bound to a within the function foo (b = )
3. c is bound to the result of a + b that is returned by foo (c = )

#### RHS Look-ups (4)

1. The var b = a; line where a is looked up from the source. ( = a)
2. The var c = foo(2); line where the return value of foo(2) is looked up from the source. ( foo(2) )
3. The return line has the next two where it looks up the values for a and b. ( a + )
4. ( + b )

## Nested Scope

We said scope is a set of rules for looking up variables by their indentifier names.
However, there is usually more than one scope to consider.
Like blocks and functions can be nested so too can scopes be nested.
If a variable can not be found in a scope, the engine checks the next outer scope
continuing until it either finds the variable or until the outermost scope is reached.
Example:

```javascript
function foo(a) {
    console.log( a + b );
}

var b = 2;

foo( 2 ); // 4
```

b's RHS can't be found within the function foo's scope but it is in the outer scope.
If this were a conversation it'd look like...
>Engine: "Hey, Scope of foo, ever heard of b? Got an RHS reference for it."

>Scope: "Nope, never heard of it. Go fish."

>Engine: "Hey, Scope outside of foo, oh you're the global Scope, ok cool. Ever
         heard of b? Got an RHS reference for it."

>Scope: "Yep, sure have. Here ya go."

### Rule for Scope
The simple rules for traversing nested Scope: Engine starts at the currently
executing Scope, looks for the variable there, then if not found, keeps going
up one level, and so on. If the outermost global scope is reached, the search
stops, whether it finds the variable or not.

## Errors

LHS and RHS look ups behave differently when the variable has not been declared
yet, (could not be found in any scope.)

Example:

```javascript
function foo(a) {
    console.log( a + b );
    b = a;
}

foo( 2 );
```

When the RHS lookup for b occurs for the first time it won't be found, it's an
undeclared variable, when this occurs a ReferenceError is thrown by the Engine.
Whereas if the LHS lookup fails for a variable and we aren't using strict mode
the Scope will create a new variable of that name in the global scope and return
it to the Engine.

Strict mode has a number of different behaviors from the standard mode and one
of those behaviors is that it disallows the implicit creation of global variables
and will throw a ReferenceError.

Now if a RHS lookup returns but you attempt to do something to its value that is
impossible it will throw a TypeError (like trying to invoke a null value like a function)
ReferenceError is scope resolution failure related whereas TypeError implies that
the scope resolution was successful but an illegal operation was attempted against the
value.

## Review
Scope is the set of rules that determines where and how a variable (identifier)
can be looked-up. This look-up may be for the purposes of assigning to the
variable, which is an LHS (left-hand-side) reference, or it may be for the
purposes of retrieving its value, which is an RHS (right-hand-side) reference.

LHS references result from assignment operations. Scope-related assignments
can occur either with the = operator or by passing arguments to (assign to)
function parameters.

The JavaScript Engine first compiles code before it executes, and in so doing,
it splits up statements like `var a = 2;` into two separate steps:

First, var a to declare it in that Scope. This is performed at the beginning,
before code execution.

Later, a = 2 to look up the variable (LHS reference) and assign to it if found.

Both LHS and RHS reference look-ups start at the currently executing Scope, and
if need be (that is, they don't find what they're looking for there), they work
their way up the nested Scope, one scope (floor) at a time, looking for the
identifier, until they get to the global (top floor) and stop, and either find
it, or don't.

Unfulfilled RHS references result in ReferenceErrors being thrown. Unfulfilled
LHS references result in an automatic, implicitly-created global of that name
(if not in "Strict Mode"), or a ReferenceError (if in "Strict Mode").
