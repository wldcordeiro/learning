# You Don't Know JS - Scope and Closures Chapter 02: Lexical Scope Notes

We defined scope as the set of rules for how the Engine can look up a variable by its name and find it either in the current scope or in any other enclosing scopes.

There are two main models for how scope works.

* Lexical Scope (The more common of the two, used by most programming languages.)
* Dynamic Scope (used by shell scripting and some modes in Perl.)

## Lex-Time

The first phase of a standard language compiler is lexing (or tokenizing), lexing examines a string of source code and assigns semantic meaning to the tokens as a result of stateful parsing.

This concept provides the foundation to understanding lexical scope and where the name comes from.

Lexical scope is scope defined at lexing time. So it is based on where variables and scope blocks are authored by you at write time and is (mostly) permanent in the time the lexer processes the code.

*You can cheat around lexical scoping in some ways but this a bad practice so it's best to treat the scope as author-time in nature*

Example:

```javascript
function foo(a) {
    var b = a * 2;

    function bar(c) {
        console.log(a, b, c);
    }

    bar(b * 3);
}

foo(2);

// output -> 2 4 12
```

There are three nested scopes in this example. It helps to think of scopes as bubbles.

* Bubble 1: the global scope, has only one identifier `foo`
* Bubble 2: foo's scope, has three identifiers `a`, `bar` and `b`
* Bubble 3: bar's scope, has only one identifier `c`

Scope bubbles are defined by where the blocks are written, which is nested in which, etc. In the next chapter scope units will be discussed, for now assume each function has its own.

`bar`'s bubble is contained within `foo`'s only because it's defined within `foo`

Scope bubbles are strictly nested, not like a venn diagram where bubbles cross boundaries.

### Look-ups

The structure and placement of these bubbles explain to the Engine where it needs to look to find an identifier.

In the previous example the Engine executes the `console.log` statement and begins searching for the three referenced identifiers `a`, `b`, and `c`. First it starts in the innermost scope of `bar` for `a`, doesn't find it, so moves up to `foo`'s scope where it finds `a` and `b`, finally it finds `c` within `bar`'s scope and completes its look ups for those.

If it were to find an `a` or `b` in the innermost scope it would use that value and finishing looking.

**Scope look-up stops once it finds the first match.**

The same identifier can be used within nested scopes, this is called shadowing,regardless though, scope lookup always begins in the innermost scope being executed and works up the levels.

In the browser, all global variables are properties of the `window` object so you can reference then as `window.name` with this you can access a global value that would be shadowed. No matter where or how a function is invoked from the lexical scope is only defined by where it was declared.

Lexical scope lookup process only applies to first-class identifiers, like `a`, `b`, or `c` something like `foo.bar.baz` would only use the lexical scope lookup to find `foo` then object property access lookup would take over.

## Cheating Lexical

JavaScript has two mechanisms that can modify lexical scope at run-time, both are frowned upon by the community. However the typical arguments against them miss the most important point, **They lead to worse performance**

### `eval`

The `eval` function takes a string as an argument and treats it like code that had been authored at that point in the program. `eval` lets you modify the lexical scope environment by pretending author time code was there all along.

Anything following an `eval` will not know that scope has been modified, it will attempt to run with the same rules it always does. Consider this example.

```javascript
function foo(str, a) {
    eval(str);
    console.log(a, b);
}

var b = 2;

foo("var b = 3;", 1);
```

The `"var b = 3;"` string will be treated like code that was there all along
and because it declares a new variable `b` it modifies the scope of `foo()`. When the `console.log` call is reached it will now find the `a` and `b` variables within the `foo` scope and shadow the global `b` variable. So the output will be `1 3` rather than `1 2`.

By default if a string passed to `eval` has one or more declarations, those declarations will modify the lexical scope in which the `eval` resides. You can even use tricks to invoke `eval` indirectly so that it affects the global scope.

**Note** In strict mode `eval` operates inside its own lexical scope, meaning declarations made within `eval` do not affect enclosing scopes.

Example:
```javascript
function foo(str) {
    "use strict";
    eval(str);
    console.log(a); // ReferenceError: a is not defined.
}

foo("var a = 2;");
```
There are other functions within JavaScript that can have a similar effect to `eval`, `setTimeout` and `setInterval` can both take a string as their first argument that will be evaluated as the code of a dynamically generated function, however this is old and deprecated so **don't** do it.

The `new Function()` constructor can take a string as its last argument to turn into a dynamically generated function (the first arguments would be named parameters for the new function), this is slightly safer than `eval` but still to be avoided.

Uses-cases for dynamically generated code inside programs are incredibly rare since performance hits are rarely if ever worth it.

### `with`

The other frowned upon and deprecated way to cheat lexical scope is the `with` keyword. `with` is typically explained as short-hand for making multiple property references against an object without repeating the object reference each time.

Example:
```javascript
var obj = {
    a: 1,
    b: 2,
    c: 3
}

// "tedious"
obj.a = 2;
obj.b = 3;
obj.c = 4;

// "easier"
with (obj) {
    a = 3;
    b = 4;
    c = 5;
}
```

But this doesn't tell the whole story, more is happening in `with`.
Example:
```javascript
function foo(obj) {
    with (obj) {
        a = 2;
    }
}

var o1 = {
    a: 3
}

var o2 {
    b: 3
}

foo(o1);
console.log(o1.a); // -> 2

foo(o2);
console.log(o2.a); // -> undefined
console.log(a); // -> 2 (leaked global!)
```

In this example two objects are created, `o1` and `o2`, one with an `a` property and the other without. `foo` takes an object reference as its argument and calls `with` on the object, and does what appears to be assigning `a` to two on the object.

In `o1`'s case the `with` block is able to find the `a` property and updates its value to 2. However for `o2`, since it has no `a` property it remains `undefined`.

The side-effect though is that a global variable `a` is created and assigned a value of 2. Why?

The `with` statement takes an object and treats it like a wholly separate lexical scope, so the properties are treated like identifiers in that scope.

**Note** Even though `with` treats an object like a lexical scope, a var declaration will not be scoped to the object but rather the enclosing function scope.

So while `eval` can modify a lexical scope, `with` creates a whole new lexical scope from the object passed to it. With that understood, it makes sense that `o1.a` is assigned a value of 2 but when we used `o2` since there was no `a` in the scope the normal LHS identifier lookup rules were followed.

Since `o2`'s "scope", `foo`'s scope, and the global scope all lack an `a` identifier, a global `a` identifier is created.

**Note** `with` is not allowed in strict mode.

### Performance

Both `eval` and `with` cheat around the author time defined lexical scope by either modifying or creating scopes at run-time.

The JavaScript engine has a number of performance optimizations that are performed at compile time. Some boil down to static analysis of code to predetermine where all variable and function declarations are, to reduce effort in resolving identifiers during execution.

However if the engine runs into an `eval` or `with` the engine must assume all awareness of identifier location may be invalid since it can't know at lexing time what may be passed to `eval` when modifying the lexical scope, or what the contents of an object you may pass to `with` are to create a new lexical scope.

In other words these two make the engine optimizations useless so it doesn't perform them. This means your code will almost certainly run slower with `eval` or `with` in it, no matter how smart the engine may be about applying optimizations.

## Review

Lexical scope means that scope is defined by author time decisions of where identifiers and functions are declared. The lexing phase of compilation is able to know where and how all identifiers are declared and predict look-up during execution.

Two mechanisms in JavaScript can cheat around lexical scope, `eval` and `with`.
`eval` can modify scopes, while `with` can create entirely new scopes. The downside of these is that it defeats the purpose of engine performance optimizations because the engine will have to make pessimistic assumptions that the optimizations will be invalid. Thus code will run slower as a result of using either. **Don't use them!**
