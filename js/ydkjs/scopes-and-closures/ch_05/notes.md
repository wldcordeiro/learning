# You Don't Know JS - Scope and Closures Chapter 05: Scope Closure Notes

This chapter is about the important concept of *closure*.

## Enlightenment

For the experienced in JavaScript that haven't yet come to terms with closures, understanding them is like achieving nirvana.

The secret of JavaScript is that **closure is all around you, you just have to recognize it and embrace it.** They aren't some special opt-in tool that you must learn new syntax and patterns for. Closures happen as a result of code that relies on lexical scope. They just happen, you don't even need to create them intentionally to take advantage of them. You're just missing the proper mental context to recognize, embrace and leverage closures for your use.

Enlightenment comes when you realize **closures are occurring all over your code and you can see them.** It's akin to when Neo seems the Matrix for the first time.

## Nitty Gritty

Here's a definition of what you need to understand and recognize closure

>Closure is when a function is able to remember and access its lexical scope even when that function is executing outside its lexical scope.

For Example:
```javascript
function foo() {
    var a = 2;

    function bar() {
        console.log(a);
    }

    bar();
}

foo();
```
This was used for examples of nested scope. Function `bar()` has access to the variable `a` in the outer enclosing scope because of lexical scope look up rules (RHS Reference here.)

Is this closure?

Perhaps, but by the quoted definition not exactly. `bar()` being able to reference `a` comes from lexical scope rules and those rules are an important **part** of understanding closure, but not the whole story.

From an academic perspective function `bar()` has a *closure* over the scope of `foo()` (and any scopes it has access to, including the global). Put simply, `bar()` closes over the scope of `foo()` because `bar()` is nested within `foo()` But this isn't an observable closure and we can't see it exercised in the snippet. We see lexical scope, but closure remains a shadow behind the code. Here's an example that sheds light on closure.
```javascript
function foo() {
    var a = 2;

    function bar() {
        console.log(a);
    }

    return bar;
}

var baz = foo();

baz(); // 2 -- closure was just observed.
```
Function `bar()` has lexical scope access to the scope of `foo()` and we then pass `bar()` as a value by returning it. We assign this return value to `baz` by executing `foo()` and invoke `baz()` which invokes the inner `bar()` function. `bar()` is then executed *outside* of its declared lexical scope.

After `foo()` executed, normally we expect that its inner scope would go away due to garbage collection. Since it would appear that the contents of `foo()` are no longer in use, it seems natural to consider that they would be gone.

But the "magic" of closures doesn't let this happen, that inner scope is still in use and thus isn't garbage collected. Who's using it? **The function `bar()`**

Due to where it was declared, `bar()` has a lexical scope closure over the inner scope of `foo()` which keeps that scope alive for `bar()` to reference any time later.

**`bar()` still has a reference to that scope, and that reference is called closure**

A few microseconds later when `baz()` is invoked (which invokes the inner function called `bar()`) it has access to author-time lexical scope, so it can access the `a` variable as expected. The function is being invoked outside of its author-time scope, but **closure** lets the function continue to access the lexical scope it was defined in at author-time.

Of course any of the various ways a function can be passed around as a value and invoked in other locations are examples of observing/exercising closure.
```javascript
function foo() {
    var a = 2;

    function baz() {
        console.log(a); // 2
    }

    bar(baz);
}

function bar(fn) {
    fn(); // I saw closure!
}
```
We pass the inner function `baz()` to `bar()` and call that function
(labeled `fn`), then it's closure over the inner scope of `foo()` is observed by accessing `a`, we can also pass functions indirectly, like so:
```javascript
var fn;

function foo() {
    var a = 2;

    function baz() {
        console.log(a);
    }

    fn = baz;
}

function bar() {
    fn();
}

foo();

bar(); // 2
```
Whatever means we use to transport an inner function outside of its lexical scope, it will maintain a scope reference to where it was originally declared and wherever it's executed the closure will be exercised.

## Now I Can See

The previous snippets were somewhat academic and theoretical. Lets look at a more "real-world" example:
```javascript
function wait(message) {
    setTimeout(function timer() {
        console.log(message);
    }, 1000);
}

wait("Hello, closure!");
```
We take an inner function `timer()` and pass it to `setTimeout()`, but `timer()` has a scope closure over `wait()`, keeping and using a reference to the variable `message`. 1000 milliseconds after we executed `wait()` and its inner scope should be gone that anonymous function still has closure over that scope.

Somewhere in the *Engine*, the built-in `setTimeout()` has a reference to some parameter probably called `fn` or `func`, the *Engine* invokes that function, which invokes our inner `timer()` function and the lexical scope reference remains intact.

**Closure**

Or for a jQuery or other framework example:
```javascript
function setupBot(name, selector) {
    $(selector).click(function activator() {
        console.log("Activating: " + name);
    });
}

setupBot("Closure Bot 1", "#bot_1");
setupBot("Closure Bot 2", "#bot_2");
```
Essentially, *wherever* and *whenever* you treat functions (which access their own lexical scopes) as first-class values and pass them around, you're likely to see those functions exercising closure. Timers, event handlers, Ajax requests, cross-window messaging, web workers, or any other asynchronous or synchronous tasks that take callback functions can and will sling closures around.

**Note:** In chapter 3 the IIFE pattern was introduced, it's often said that this pattern is an example of an observed closure, it doesn't quite match our definition.
```javascript
var a = 2;

(function IIFE() {
    console.log(a);
})();
```
This "works", but it's not strictly an observation of closure. The IIFE function is not executed outside its lexical scope. It's invoked right in the same scope it was declared in, the enclosing/global scope `a` was declared in and `a` is found via normal lexical scope look-up not closure.

While closure may be occurring technically at declaration time, it's not strictly observable, you would say *it's a tree falling in a forest without anyone around to hear it.*

Though an IIFE isn't an example of closure it does create scopes, and it's one of the most common tools to create scopes that can be closed over. So IIFE's are heavily related to closure, even if not exercising closure themselves.

## Loops + Closures

The most common example of closure involves the for loop, for example:
```javascript
for (var i = 1; i <= 5; i++) {
    setTimeout(function timer() {
        console.log(i);
    }, 1000 * i);
}
```
**Note:** Linters often complain about when you put functions within a loop, because the mistakes of not understanding closure are **so common among developers**. Here it's explained how to properly leverage the full power of closure. But that subtlety is often lost on linters and the will complain assuming you don't know what you're doing.

The idea of this code snippet is that we normally *expect* it to print out each number after a one second delay each.

But if you run this code, you'll get "6" printed out five times at one second intervals.

**HUH?**

Let's explain where the `6` is coming from, the terminating condition of the loop is `i <= 5`, the first time that's the case is when `i` is 6. So the output is reflecting the final value of the `i` after the loop terminates.

This seems obvious on second glance, the timeout functions are all running well after loop completion, in fact as timers go, even if it was `setTimeout(.., 0)` on each iteration, all the callbacks would still run after the completion of the loop and thus print 6 each time.

The deeper question at play here is what's missing from our code to actually have it behave the way we semantically implied?

What's missing is that we're trying to *imply* that each loop iteration captures its own copy of `i` at iteration time. But the way scope works, all five of those functions even though they're defined separately in each iteration are all **closed over the same shared global/enclosing scope** which has only one `i` in it.

Put this way, it's obvious all functions share the same reference to `i`, something about the loop structure tends to confuse us that there's more sophistication, there isn't. There's no difference than if each of the `setTimeout` callbacks were just declared one after the other with no loop.

So what's missing? More closured scope! Specifically a new closured scope per iteration. In Chapter 3 we learned IIFE creates a new scope by declaring a function and immediately executing it. Let's try that:
```javascript
for (var i = 1; i <= 5; i++) {
    (function() {
        setTimeout(function timer() {
            console.log(i);
        }, i * 1000);
    })();
}
```
Still not working though... why? There's more lexical scope and each callback is closing over its own per-iteration scope created by IIFE.

It's not enough to have a scope to close over if **it's an empty scope**, the IIFE is just an empty scope, it needs *something* in it to be useful, it needs it's own variable with a copy of `i` at each iteration.
```javascript
for (var i = 1; i <= 5; i++) {
    (function() {
        var j = i;
        setTimeout(function timer() {
            console.log(j);
        }, j * 1000);
    })();
}
```
**It works**

A variation some prefer is:
```javascript
for (var i=1; i<=5; i++) {
    (function(j){
        setTimeout( function timer(){
            console.log( j );
        }, j*1000 );
    })( i );
}
```
Since these IIFE's are just functions we can pass `i` and call it `j` or even `i` again, either way the code works now.

The use of an IIFE in each iteration created a new scope for each iteration, which gave the timeout callback the opportunity to close over a new scope for each iteration, one with a variable with the right per-iteration value available to access.

### Block Scoping Revisited

Look carefully at the analysis of the previous solution. An IIFE was used to create a new scope per-iteration. In other words we needed a per-iteration **block scope**. In Chapter 3 the `let` declaration was introduced that hijacks a block and declares a variable right there in the block. **Essentially turning a block into a scope that we can close over.** So this just works:
```javascript
for (var i = 1; i <= 5; i++) {
    let j = i; // block scope for closure
    setTimeout(function timer() {
        console.log(j);
    }, j * 1000);
}
```
But there's a special behavior for `let` declarations in a for-loop head. This behavior makes it so that the variable will be declared for **each iteration** of the loop and will be initialized at each subsequent iteration with the value from the previous iteration.
```javascript
for (let i = 1; i <= 5; i++) {
    setTimeout(function timer() {
        console.log(i);
    }, i * 1000);
}
```

## Modules

There are other code patterns that leverage closure but do not appear to be about callbacks. Let's explore the most powerful, *modules.*
```javascript
function foo() {
    var something = "cool";
    var another = [1, 2, 3];

    function doSomething() {
        console.log(something);
    }

    function doAnother() {
        console.log(another.join(" ! "));
    }
}
```
As it stands now, there's no observable closure in the snippet above, we just have some private data variables `something` and `another` and some inner functions `doSomething` and `doAnother` which both have lexical scope and closure over `foo()`.

But consider:
```javascript
function coolModule() {
    var something = "cool";
    var another = [1, 2, 3];

    function doSomething() {
        console.log(something);
    }

    function doAnother() {
        console.log(another.join(" ! "));
    }

    return {
        doSomething: doSomething,
        doAnother: doAnother
    };
}

var foo = coolModule();

foo.doSomething(); // cool
foo.doAnother(); // 1 ! 2 ! 3 !
```
This is the *module* pattern, specifically the "revealing module" pattern.

Let's examine this example. `coolModule()` is just a function, so it must be invoked for there to be a module instance, without that then the inner scope and closures don't occur. `coolModule()` returns an object (denoted by the `{ key: value }` syntax), which has references to our inner functions but not the inner data variables. Those remain private, think of this object return value as a **public API for our module**.

The returned object value is assigned to the `foo` variable and we can access the property values on the API like `foo.doSomething()`.

**Note:** It's not required to return an object, you could return just a function. jQuery is a good example of this, the `jQuery` and `$` identifiers are the public API for the module, but they are just functions (which can have properties, since all functions are objects.)

`doSomething()` and `doAnother()` both have closure over the inner scope of the module "instance" (arrived at by invoking `coolModule()`), when they're transported outside of the lexical scope by way of property reference on the return object we've setup conditions where closure can be observed and exercised.

Simply put, there are two "requirements" for the module pattern to be exercised.

1. There must be an outer enclosing function and it must be invoked at least once (each time creating a new module instance).
2. The enclosing function must return at least one inner function, so that the inner function has closure over the private scope and can access/modify that scope.

An object with a function property on it alone is not *really* a module. An object returned from a function invokation with only data properties and no closured functions is not *really* a module in the observable sense.

The code snippet above shows a standalone module creator `coolModule()` that can be invoked any number of times each with a new instance. A variation on this pattern is when you only care to have one instance, a "singleton" of sorts:
```javascript
var foo = (function coolModule() {
    var something = "cool";
    var another = [1, 2, 3];

    function doSomething() {
        console.log(something);
    }

    function doAnother() {
        console.log(another.join(" ! "));
    }

    return {
        doSomething: doSomething,
        doAnother: doAnother
    };
})();

foo.doSomething(); // cool
foo.doAnother(); // 1 ! 2 ! 3 !
```
Here the module function is turned into an IIFE and we *immediately* invoke it and assign the return object to the identifier `foo`.

Since modules are functions they can receive arguments:
```javascript
function coolModule(id) {
    function identify() {
        console.log(id);
    }

    return {
        identify: identify
    };
}

var foo1 = coolModule("foo 1");
var foo2 = coolModule("foo 2");

foo1.identify(); // foo 1
foo2.identify(); // foo 2
```
Another powerful variation on the module pattern is to name the return object of the API:
```javascript
var foo = (function coolModule(id) {
    function change() {
        publicAPI.identify = identify2;
    }

    function identify1() {
        console.log(id);
    }

    function identify2() {
        console.log(id.toUpperCase());
    }

    var publicAPI = {
        change: change,
        identify: identify1
    };

    return publicAPI;
})("foo module");

foo.identify(); // foo module
foo.change();
foo.identify(); // FOO MODULE
```
By retaining an inner reference to the public API object with the module instance, you can modify the module instance **from within**, including adding/removing methods, properties and changing their values.

### Modern Modules

Various module dependency loaders/managers essentially wrap up this pattern of module definition into an API. Rather than examine a particular library, here is a *very simple* proof of concept for **illustration purposes only:**
```javascript
var myModules = (function Manager() {
    var modules = {};

    function definition(name, deps, impl) {
        for (let i = 0; i < deps.length; i++) {
            deps[i] = modules[deps[i]];
        }
        modules[name] = impl.apply(impl, deps);
    }

    function get(name) {
        return modules[name];
    }

    return {
        define: define,
        get: get
    };
})();
```
The key part of this snippet is `modules[name] = impl.apply(impl, deps);` This invokes the definition wrapper function for a module (passing any dependencies) and stores the return value, the module's API, into an internal list of modules tracked by name.

Here's it in use:
```javascript
myModules.define("bar", [], function() {
    function hello(who) {
        return `Let me introduce: ${who}`;
    }

    return {
        hello: hello
    };
});

myModules.define("foo", ["bar"], function() {
    var hungry = "hippo";

    function awesome() {
        console.log(bar.hello(hungry).toUpperCase());
    }

    return {
        awesome: awesome
    };
});

var bar = myModules.get("bar");
var foo = myModules.get("foo");

console.log(bar.hello()); // Let me introduce: hippo

foo.awesome(); // LET ME INTRODUCE: HIPPO
```
Both `foo` and `bar` modules are defined with a function that returns a public API. `foo` even receives the instance of `bar` as a dependency parameter and can use it accordingly. The key take-away is that there isn't any "magic" to module managers, they fulfill both characteristics of the module pattern listed above: invoking a function definition wrapper, and keeping its return value as the API for that module.

Modules are just modules, even with a friendly wrapper tool on top of them.

### Future Modules

In ES6 there will be first class support for modules. When loaded via the module system ES6 treats a file as a separate module, each module can both import other modules or specific API members as well as export their own API members.

**Note:** Function based modules aren't a statically recognized pattern (something the compiler knows about), so their API semantics aren't considered till runtime. Meaning you can actually modify a module's API at runtime.

In contrast, ES6 modules are static (no API change at runtime). Since the compiler knows this it can check during file loading and compilation that a reference to a member of an imported module's API *actually exists*. If the member doesn't exist, the compiler will throw an *early* error at compile-time, rather than waiting for the traditional runtime resolution and error (if any).

ES6 modules **do not** have an "inline" format. They must be defined in separate files (1 per module). The engines have a default "module loader" (overridable, but beyond scope of book) which synchronously loads a module file when imported.

Consider this example:
**bar.js**
```javascript
function hello(who) {
    return `Let me introduce: ${who}`;
}

export hello;
```
**foo.js**
```javascript
import hello from "bar";

var hungry = "hippo";

function awesome() {
    console.log(hello(hungry).toUpperCase());
}

export awesome;
```
In use:
```javascript
module foo from "foo";
module bar from "bar";

console.log(bar.hello("rhino")); // Let me introduce: rhino

foo.awesome(); // LET ME INTRODUCE: RHINO
```
**Note:** separate `foo.js` and `bar.js` files would need to be created, then your program would load/import the modules to use them like in the third snippet.

`import` imports one or more members of a module's API into the current scope. Each bound to a variable (`hello` in the example). `module` imports an entire module API into the current scope to a bound variable (`foo` and `bar` in our case). `export` exports an identifier (variable, function), to the public API for the current module. These operators can be used as many times as necessary in a module definition.

The contents of a *module file* are treated like they're enclosed in a scope closure, just like function-closure modules.

## Review

Closures appear to the unenlightened like a mystical world inside JavaScript that only the brave can reach. But it's actually a standard and obvious fact of how we write code in a lexically scoped environment, where functions are values and can be passed around.

**Closure is when a function can remember and access its lexical scope even when it’s invoked outside its lexical scope.**

Closures can trip you up, like in loops if you're not careful to recognize them and how they work. But they're a powerful tool that enable patterns like modules in their various forms.

Modules require two key characteristics.

1. An outer wrapping function being invoked to create the enclosing scope.
2. The return value of the enclosing function must include reference to at least one inner function that then has closure over the private inner lexical scope of the wrapper.

Closures are all around existing code, and you can recognize and leverage them to your benefit.
