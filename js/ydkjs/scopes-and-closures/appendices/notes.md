# You Don't Know JS - Scope and Closures Appendices: Notes

# Appendix A: Dynamic Scope

In chapter 2 it was mentioned that dynamic scope is in contrast to lexical scope which JavaScript and most other languages use. Let's examine dynamic scope to show this contrast.

Lexical scope is the set of rules about how the engine can look up variables and where to find it. The key characteristic of lexical scope is that it's defined at author-time. Dynamic scope implies that there's a model where scope can be determined dynamically at runtime. For example:
```javascript
function foo() {
    console.log(a); // 2
}

function bar() {
    var a = 3;
    foo();
}

var a = 2;
bar();
```
In lexical scope the RHS reference to in `a` in `foo()` would resolve to the global variable `a` which makes the value printed 2. Dynamic scope, in contrast would be concerned with where the function and scopes are called from rather than where they're declared. Meaning the scope-chain is based on the call-stack, not the scope nesting.

If JavaScript had dynamic scope, when `foo()` is executed, it would theoretically output 3 rather than 2.
```javascript
function foo() {
    console.log(a); // 3
}

function bar() {
    var a = 3;
    foo();
}

var a = 2;
bar();
```
How? Because when `foo()` can't resolve the variable reference for `a`, instead of moving up the lexical scope chain, it moves up the call stack to find where it was called from. Since `foo()` was called from `bar()` it finds the variable in scope for `bar()` with a value of 3.

Strange? That's only because you've likely only worked on lexically scoped code. So dynamic scope seems foreign. Lexical scope would seem just as weird if you were used to dynamic scope. JavaScript doesn't have dynamic scope, but the `this` mechanism is kind of like dynamic scope.

The key contrast is that lexical scope is write time and dynamic scope (and `this`) are runtime. Where functions and variables are declared matter to lexical scope, but dynamic scope cares for where it was called from. Also `this` cares how a function was called, which shows how closely related to dynamic scope the mechanism is.

# Appendix B: Polyfilling Block Scope

We've explored block scope. We've seen that `with` and the `catch` clause are both examples of block scope in JavaScript. But in ES6 we're getting real block scope thanks to the `let` keyword. This is awesome, but what about environments that are pre-ES6? Consider this:
```javascript
{
    let a = 2;
    console.log(a) // 2
}

console.log(a) // ReferenceError
```
This is awesome in ES6 environments, but what to do about pre-ES6? Use `catch`.
```javascript
try{ throw 2 } catch(a) {
    console.log(a); // 2
}

console.log(a); // ReferenceErro
```
Ugly hugh? The `try/catch` forcibly throws an error with the value of 2 and then the variable declaration is in the `catch(a)`. Yep, the `catch` clause has block-scoping so it can be used as a polyfill in pre-ES6 environments.

This code is ugly, but the point is that tools can transpile ES6 code into working pre-ES6 environments. You can let a build step tool take care of producing code that will work when deployed while taking advantage of block scope.

This is the preferred migration path for most of ES6, use a code transpiler to the the ES6 code and produce ES5 compatible code.

## Traceur

Google has a project called Traceur, it's an ES6 transpiler. The TC39 committee uses this tool to test out the semantics of the features they add.

What does our snippet produce in Traceur?
```javascript
{
    try {
        throw undefined;
    } catch(a) {
        a = 2;
        console.log(a);
    }
}

console.log(a);
```
With tools like this we can use block scope regardless if we're targeting ES6.

## Implicit vs Explicit Blocks

We've identified potential pitfalls to conde maintainability/refactorability when block scoping is introduced. Let's see ways to take advantage of block scoping while reducing the downsides.

This is the `let` block or `let` statement (as opposed to let declarations)
```javascript
let (a = 2) {
    console.log(a); // 2
}

console.log(a); // ReferenceError
```
Instead of hijacking an existing block implicitly the `let` statement creates an explicit block for its scope binding. This stands out more and fares more robustly in refactoring, it also produces cleaner code by, forcing all declarations to the top of the block, making it easier to know what's scoped to it and not.

This pattern mirrors the approach many take with function scoping where they manually move/hoist all their `var` declarations to the top of the function. The `let` statement puts them there at the top by intent, and if you don't use `let` declarations throughout the block scoping declarations are easier to maintain.

Unfortunately the `let` statement isn't included in ES6 and Traceur doesn't transpile it. There's two options we can format using ES6 valid syntax and a little code discipline:
```javascript
/*let*/ { let a =2;
    console.log(a);
}

console.log(a); // ReferenceError
```
The other option is to write explicit `let` statement blocks and have a tool convert it to valid code. The author wrote a tool called *let-er*, it's a transpiler that converts `let` statements into ES6 compatible code that Traceur can parse into ES5 if necessary, by default it uses the `try/catch` trick but with the `--es6` flag it'll produce this.
```javascript
{
    let a = 2;
    console.log(a);
}

console.log(a); // ReferenceError
```

So using *let-er* you can target all pre-ES6 environments or use the flag for ES6 and best of all you can use the `let` statement form without worry.

## Performance

A note on performance: comparing `try/catch` to an IIFE for creating scopes.

`try/catch` is slower, there's no reason why it has to be this way or that it will always be this way since the official TC39 approved ES6 transpiler uses it, the Traceur team has asked Chrome to improve performance for `try/catch`.

Also IIFE isn't a fair comparison, because a function wrapped around any arbitrary code breaks the meaning inside that code of `this`, `return`, `break` and `continue`. IIFE isn't a suitable substitute it's best used in certain situations only. The question is do you want block scoping or not? If so the tools provide you the option, if not use `var` and don't worry.

# Appendix C: Lexical-this

ES6 has a special syntactic form of function declarations called *arrow* function. It affects lexical scope and `this` values. It looks like this.
```javascript
var foo = a => {
    console.log(a);
}

foo(2); // 2
```
The "fat arrow" is mentioned as shorthand for the `function` keyword. But there's more to it than that. This example has a problem:
```javascript
var obj = {
    id: "awesome",
    cool: function coolFn() {
        console.log(this.id);
    }
};

var id = "not awesome";

obj.cool(); // awesome

setTimeout(obj.cool, 100); // not awesome
```
The problem is the loss of  the `this` binding on the `cool()` function. There are ways to address this issue, an often used one is `var self = this;` Like this:
```javascript
var obj = {
    count: 0,
    cool: function coolFn() {
        var self = this;

        if (self.count < 1) {
            setTimeout(function timer() {
                self.count++;
                console.log('awesome?');
            }, 100);
        }
    }
};

obj.cool(); // awesome?
```
Briefly, the `var self = this;` "solution" just gets around the problem of properly using `this` by making it into a lexical scope identifier, not caring what happened to `this` along the way.

Verbose code sucks, so a motivation of ES6 is alleviating these scenarios. The ES6 solution is the arrow function introducing a new behavior called lexical `this`.
```javascript
var obj = {
    count: 0,
    cool: function coolFn() {
        if (this.count < 1) {
            setTimeout(() => {
                this.count++;
                console.log('awesome?');
            }, 100);
        }
    }
};

obj.cool(); // awesome?
```
Shortly put, arrow functions don't behave like normal functions when it comes to `this`, they discard normal rules for `this` binding and take on the `this` value of their immediate enclosing scope, whatever that is.

So in the snippet the arrow function doesn't have the `this` value unbound, it just "inherits" the `coo()` function's `this` value. This may make for shorter code, they're just codifying into the syntax a common *mistake* of developers of confusing and conflating `this` rules with lexical scope rules.

Put simply: why go to the trouble of using `this` style coding paradigm, only to weaken it by mixing it with lexical references? It seems natural to choose one approach and stick with it for any given piece of code.

**Note:** Another detraction from arrow functions is that they're anonymous, not named.

A more appropriate approach to this "problem" is to use the `this` mechanism correctly:
```javascript
var obj = {
  count: 0,
  cool: function coolFn() {
    if (this.count < 1) {
      setTimeout(function timer() {
        this.count++ // `this` is safe because of `bind()`
        console.log('more awesome');
      }.bind(this), 100); // look, `bind()`
    }
  }
};

obj.cool(); // more awesome
```
Whether you prefer the new lexical `this` of arrow functions, or you prefer `bind()`, it's worth noting that arrow functions aren't just about typing less of `function`.

There is an intentional behavioral difference we should learn and understand, and leverage if we want.
