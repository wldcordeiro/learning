# You Don't Know JS - Scope and Closures Chapter 03: Function vs. Block Scope Notes

In chapter 2 we discussed how scope can be seen as "bubbles" that contain their values and how they can nest, but is it only functions that create scopes or can other structures in JavaScript create scopes?

## Scope From Functions

JavaScript has function based scope, meaning each function has its own scope. There are also a few other structures with their own scopes which we'll explore later. First an example of function scope.
```javascript
function foo(a) {
    var b = 2;
    // some code

    function bar() {
        // some code
    }

    // more code

    var c = 3;
}
```
The `foo` scope includes the `a`, `b`, `c` and `bar` identifiers, **it doesn't matter** where in the scope these identifiers are declared they belong to the scope regardless. `bar()` has its own scope as well, and so does the global scope which only has the `foo()` identifier` in it.

Because `a`, `b`, `c`, and `bar()` all belong to `foo`'s scope they are inaccessible from outside of `foo` and would cause ReferenceError like in this example.
```javascript
bar(); // fails -> ReferenceError
console.log(a, b, c); // all three would fail.
```
However all these identifiers are accessible within `foo` as well as `bar` assuming that none are shadowed within `bar`.

Function scope encourages the idea that all variables belong to a function, and can be used by that function and any inner functions. This design approach is useful and can make use of the "dynamic" nature of JavaScript variables to take on different types as needed. However if you don't take precautions variables existing across an entire scope can lead to some unexpected pitfalls.

## Hiding In Plain Scope

The traditional way of thinking of functions is that you declare a function and add code in the body, but the opposite way of thinking is equally useful. You take an arbitrary section of code and wrap it in a function, effectively "hiding" the code. The practical result is creating a scope around the code, meaning any declarations within are now tied to the scope of the wrapping function, meaning you can "hide" variables and functions by enclosing them within a function.

Why would this hiding of identifiers be a useful technique? There are a variety of reasons behind this approach, tending to arise from the "principle of least privilege." This principle states that in software design such as an API you should only expose what is minimally necessary while hiding everything else. The principle extends to the choice of which scope to contain variables and functions. If all variables and functions were in the global scope they would be accessible to any inner scopes, but this would violate the "least" part of the principle in that you're likely exposing many identifiers that you should keep private, for example:
```javascript
function doSomething(a) {
    b = a + doSomethingElse(a * 2);

    console.log(b * 3);
}

function doSomethingElse(a) {
    return a - 1;
}

var b;

doSomething(2); // -> 15
```
In this example, `b` and `doSomethingElse` are likely "private" details of how `doSomething` does its work. Giving the enclosing scope access to these is unnecessary and possibly dangerous in that they may be used in unexpected ways and it may violate pre-condition assumptions of `doSomething`.

A better way to design this would be to hide all this in `doSomething`'s scope:
```javascript
function doSomething(a) {
    function doSomethingElse(a) {
        return a - 1;
    }

    var b;

    b = a + doSomethingElse(a * 2);

    console.log(b * 3);
}

doSomething(2); // -> 15
```
Now `b` and `doSomethingElse` aren't accessible to outside influence and are controlled by `doSomething`. Functionality and end result may not be affected, but the design keeps private details private.

### Collision Avoidance

Another benefit of "hiding" identifiers inside a scope is to avoid unintended collision between two different identifiers with the same name. Collisions often result in unexpected overwriting of values. For example:
```javascript
function foo() {
    function bar(a) {
        i = 3; // changing the `i` in the enclosing scope's for loop.
        console.log(a + i);
    }

    for (var i = 0; i < 10; i++) {
        bar(i * 2);
    }
}

foo();
```
The reassignment of `i` within `bar` will cause an infinite loop because it sets `i` to 3 which will always remain `< 10`. The assignment within `bar` needs to declare a local variable, regardless of what identifier is chosen. `var i = 3;` would work but so would renaming the variable to something else like say `var j = 3;` but your design may naturally call for the same name, so utilizing scope to "hide" your inner declaration is the best/only option.

### Global "Namespaces"

A good example of variable collisions occurs in the global scope. Multiple libraries loaded can easily collide if they don't properly hide their private functions and variables.

A solution is to create a single variable in the global scope, often an object with a unique name that is used as a "namespace" for the library with all specific exposures of the library done as properties of the object.

For Example:
```javascript
var myReallyCoolLibrary = {
    awesome: "stuff",
    doSomething: function() {
        // ...
    },
    doSomethingElse: function() {
        // ...
    },
};
```

### Module Management

Another option for collision avoidance is the modern "module" approach, where you use a dependency manager. Using a dependency manager, no libraries ever add identifiers to the global scope but are instead required to import their identifiers into another specific scope through the dependency manager's systems. These systems use no magic that makes them exempt from lexical scope, they use the lexical scope rules to enforce that no identifiers are injected into a shared scope and are kept private instead. You can also achieve these same results by coding defensively rather than using a dependency manager.

## Functions As Scopes

You can take a bit of code, wrap it in a function and effectively "hide" it, like so:
```javascript
var a = 2;

function foo() { // <- insert this
    var a = 3;
    console.log(a); // 3
} // <- and this
foo(); // <- and this

console.log(a);
```

However this isn't ideal, doing this still puts the `foo` identifier in the enclosing scope and then we must call the function explicitly as well so the code executes. Ideally the function would either not need a name or it wouldn't be in the global scope and it would automatically execute, we can do this. For example:
```javascript
var a = 2;

(function foo() { // <- insert this
    var a = 3;
    console.log(a); // 3
})(); // <- and this

console.log(a);
```

Let's break this down. We wrap the function declaration in parentheses, this turns this into a *function expression*.

**Note** To distinguish between a function declaration and a function expression look at the location of the function keyword, if it's the first thing in a statement, it's a declaration, otherwise it's an expression.

The key difference between a function declaration and an expression relates to where its name is bound as an identifier. In the first snippet `foo` is bound to the enclosing scope and we call it directly. In the second snippet the name `foo` is not bound in the enclosing scope and only bound inside of its own function. `(function foo() {...})` as an expression means that the `foo` identifier is found only inside its scope (the `...`), hiding it like this avoids polluting the global/enclosing scope unnecessarily.

### Anonymous vs. Named

An anonymous function expression looks like this
```javascript
setTimeout(function() {
    console.log("I waited one second!");
}, 1000);
```

It's called this because the function has no name identifier attached to it, a function expression can be anonymous but a function declaration can't. Anonymous functions are easy to use and many libraries encourage this style, but there are draw-backs to consider.

1. Anonymous functions have no useful name to display in a stack trace, making debugging more difficult.
2. Since it's nameless if the function needs to refer to itself you need to use the **deprecated** arguments.callee, another example is when an event handler needs to unbind itself after it fires.
3. Function names are helpful in providing more readable/understandable code which can be self documenting.

**Inline Function Expressions** are useful, whether they're named or anonymous, providing a name for the function can remove the drawbacks with no real downsides, for example.
```javascript
setTimeout(function timeoutHandler() {
    console.log("I waited one second!");
}, 1000);
```

### Invoking Function Expressions Immediately

```javascript
var a = 2;

(function foo() {
    var a = 3;
    console.log(a); // 3
})();

console.log(a); // 2
```
We can invoke a function expression by adding the `();` to the end like
`(function foo() {...})();`, the first parentheses makes it a function expression, the second invokes it. This is such a common pattern it's called
**IIFE**, standing for Immediately Invoked Function Expression.

IIFE's can be anonymous and most commonly are, but naming them has benefits over anonymous function expressions, so it's worth adopting.
```javascript
var a = 2;

(function IIFE() {
    var a = 3;
    console.log(a); // 3
})();

console.log(a); // 2
```
There's a variation on the IIFE form, it doesn't offer any additional functionality and is purely stylistic but here it is.
```javascript
var a = 2;

(function IIFE() {
    var a = 3;
    console.log(a); // 3
}());

console.log(a); // 2
```
You just move the invocation to within the first set of parentheses. Another variation on IIFE is an IIFE with arguments, for example.
```javascript
var a = 2;

(function IIFE(global) {
    var a = 3;
    console.log(a); // 3
    console.log(global.a); // 2
})(window);

console.log(a); // 2
```
We pass in the `window` object, but we name the parameter `global` so that we have a delineation for global vs. non-global references. Of course any enclosing scope could be passed, or any object with an `a` property.

This pattern address the niche concern that `undefined` might be overwritten, by naming a parameter undefined but not passing any parameter to the function invocation we can guarantee that `undefined` has the right value in the function scope.
```javascript
var undefined = true; // setting a land mine.

(function IIFE(undefined){
    var a;
    if (a === undefined) {
        console.log('undefined is safe here!');
    }
})();
```

Another variation inverts the order of things and has the function to execute second after the invocation and parameters to pass it. UMD (Universal Module Definition) uses this pattern. Some people find it easier to understand though it is more verbose.
```javascript
var a = 2;

(function IIFE(def) {
    def(window);
})(function def(global) {
    var a = 3;
    console.log(a);
    console.log(global.a);
});
```
The `def` function is defined in the second half of this block, and then passed as a parameter to the `IIFE` function which invokes `def` with the `window` object as a parameter.

## Blocks As Scopes

Functions are the most common unit of scope in JavaScript. There are however other units of scope available and the usage of some these can lead to cleaner, better code.

In many languages, blocks can be their own scope, that isn't the case in JavaScript, at least not yet. Here's an example of a block in JavaScript,
```javascript
for (var i = 0; i < 10; i++) {
    console.log(i);
}
```
We declare the variable `i` in the for-loop's head because our intent was to use it within the loop and ignore the fact that `i` is actually scoped to the enclosing scope. Block scoping is all about declaring variables in as local a fashion as possible, so that they are scoped as close to their use as possible. Another example:
```javascript
var foo = true;

if (foo) {
    var bar = foo * 2;
    bar = something(bar);
    console.log(bar);
}
```
We're using `bar` only within the context of the if block, so it makes sense that we declare it with the block. However in JavaScript where we declare the variable doesn't matter, because they always belong to the enclosing scope, this is basically "fake" block scoping and relies on self-enforcement to not use `bar` in other places. Block scoping is another tool to use for the principle of least privilege/exposure to hide implementation details.

Considering the for-loop example, why do we pollute the enclosing scope with the `i` variable if that's going to be the only place it's used? More importantly, developers may wish to check themselves against accidental variable reuse by having an error when using a variable if you use it in the wrong place. If block scoping were possible for `i` would make it only available for that for-loop, causing an error if it were used elsewhere. The reality though is that JavaScript has no facility for this, at least until you dig further...

### `with`

`with` is an example of a form of block scoping, in that the scope that is created only exists for the lifetime of the `with` statement and not the enclosing scope, but it is a frowned upon construct.

### `try/catch`

A very little known fact in JavaScript ES3 is that the `catch` of a `try/catch` has its own block scope. For example:
```javascript
try {
    undefined(); // illegal operation to force exception...
} catch (err) {
    console.log(err); // works here.
}

console.log(err); // doesn't work here, ReferenceError.
```
`err` only exists within the `catch` block and while this behavior has been in JavaScript for years, linters may still complain if you have two or more `catch` clauses in the same scope that each use the same name for their error variable, it's not being redefined, but the linters don't know better. To avoid this, some people give their errors different names or disable the linting rule.

### `let`

Up till now JavaScript hasn't had a form of block scoping except for niche behaviors, but with ES6 the `let` keyword is introduced. The `let` keyword attaches a variable declaration to whatever block scope it's contained in.
```javascript
var foo = true;

if (foo) {
    let bar = foo * 2;
    bar = something(bar);
    console.log(bar);
}

console.log(bar); // ReferenceError
```
Using `let` to attach a variable to a block is somewhat implicit. It can be confusing if you don't pay attention to which blocks have variables scoped to them. Creating explicity blocks for `let` scoping can fix this issue somewhat by making it more obvious where variables are attached. Explicit code is preferred over implicit and explicit blocks are easy to achieve. For example:
```javascript
var foo = true;

if (foo) {
    {
        let bar = foo * 2;
        bar = something(bar);
        console.log(bar);
    }
}

console.log(bar); // ReferenceError
```
You can create a block for `let` by including `{...}` anywhere a statement is valid. In the previous case we made an explicit block within the if block, making it easier for the whole block to move in a refactoring without affecting the if block. In the next chapter, hoisting will be discussed, but `let` will not hoist the entire scope of the block they're in. The declarations won't exist until the declaration statement.
```javascript
{
    console.log(bar); // ReferenceError
    let bar = 2;
}
```

#### Garbage Collection

Block scoping can be useful due to closures and garbage collection. Consider:
```javascript
function process(data) {
    // do something interesting
}

var someReallyBigData = { .. };

process( someReallyBigData );

var btn = document.getElementById( "my_button" );

btn.addEventListener( "click", function click(evt){
    console.log("button clicked");
}, /*capturingPhase=*/false );
```
The `click` function handler doesn't need the `someReallyBigData` variable, this means that after `process` runs that it could be garbage collected. But it's likely the JS engine will keep it around since the `click` function has a closure over the entire scope. Block scoping could fix this by making it clear to the engine when `someReallyBigData` loses its usefulness and is garbage collectible.
```javascript
function process(data) {
    // do something interesting
}

// anything declared inside this block can go away after!
{
    let someReallyBigData = { .. };

    process( someReallyBigData );
}

var btn = document.getElementById( "my_button" );

btn.addEventListener( "click", function click(evt){
    console.log("button clicked");
}, /*capturingPhase=*/false );
```
Declaring explicit blocks for variables to locally bind to is a powerful tool to use.

#### `let` Loops

`let` is especially great in loops.
```javascript
for (let i = 0; i < 10; i++) {
    console.log(i);
}

console.log(i); // ReferenceError
```
Not only does using `let` bind the `i` variable to the block, it also **rebinds** for each iteration. Here's another way to illustrate this.
```javascript
{
    let j;
    for (j = 0; j < 10; j++) {
        let i = j; // rebind for each iteration
        console.log(i);
    }
}
```
This rebinding behavior will be cleared up in chapter five. Since `let` declarations attach to arbitrary blocks instead of enclosing scopes there are gotchas when dealing with code that relies on function scoped `var`s and replacing it with `let` might require extra care.
For Example:
```javascript
var foo = true, baz = 10;

if (foo) {
    var bar = 3;

    if (baz > bar) {
        console.log( baz );
    }

    // ...
}
```
Could be refactored as:
```javascript
var foo = true, baz = 10;

if (foo) {
    var bar = 3;

    // ...
}

if (baz > bar) {
    console.log( baz );
}
```
But be careful when using block-scoped variables.
```javascript
var foo = true, baz = 10;

if (foo) {
    let bar = 3;

    if (baz > bar) { // <-- don't forget `bar` when moving!
        console.log( baz );
    }
}
```
In appendix B, an alternate more explicit block scoping is shown that makes it easier to refactor code to be more robust.

### `const`

ES6 introduces the `const` keyword which creates block scoped constants, attempting to change a `const` variable results in an error.
```javascript
var foo = true;

if (foo) {
    var a = 2;
    const b = 3; // block-scoped to the if

    a = 3; // cool
    b = 4; // error!
}

console.log(a); // 3
console.log(b); // ReferenceError
```

## Review

Functions are the most common unit of scope in JavaScript. Variables and functions declared within a function are "hidden" from enclosing scopes, an intentional design principle of good software. Functions though are not the only unit of scope. Block-scope refers to the idea that variables and functions can belong to any block `{...}` of code rather than the enclosing function. In ES3 the `try/catch` structure has a block-scope in the `catch`

In ES6 the `let` keyword is introduced to allow declarations of variables in blocks of code `{...}`, but even with this being introduced it doesn't make `var` useless, developers should utilize both appropriate to produce better code.
