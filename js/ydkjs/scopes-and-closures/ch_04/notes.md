# You Don't Know JS - Scope and Closures Chapter 04: Hoisting Notes

There is a subtle details of how scope attachment works with declarations that appear within a scope, we'll examine that now.

## Chicken Or The Egg

It's tempting to think that all JavaScript code is interpreted line by line in a top-down order. This is mostly true, however there's some subtle details to be aware of. For Example:
```javascript
a = 2;

var a;

console.log(a);
```
What's the value logged to the console? Many developers would assume `undefined` since the definition of the variable occurs after the `a = 2` line, however it will output `2`. Here's another example:
```javascript
console.log(a);

var a = 2;
```
You might assume that here `2` would be output or a `ReferenceError` would be thrown, but in this case it would output `undefined`. **What's going on here?**

## The Compiler Strikes Again

To answer what's going on here, recall that the engine actually compiles the code before it is run and that it finds and associates all declarations with their scopes. So think about it like this, all declarations, both variables and functions are processed before anything else is executed. So when you see `var a = 2;` JavaScript doesn't see it as one statement, it sees it as `var a;` and `a = 2;` with the declaration being processed first during compilation and the second being left **in place** for execution. Think of the first example as:
```javascript
var a;
```

```javascript
a = 2;
console.log(a);
```
And think of the second example as:
```javascript
var a;
```

```javascript
console.log(a);
a = 2;
```
A way of thinking of this is that variable and function declarations are "moved" from where they appear to the top of their scope, thus the term "hoisting". **The declaration comes before the assignment**

**Note:** Only declarations are hoisted, if anything else were rearranged that would cause havoc.

```javascript
foo();

function foo() {
    console.log(a); // undefined

    var a = 2;
}
```
The `foo` function declaration is hoisted to the top of the global scope, and it's important to note that hoisting is scope based, so within `foo`, `var a` is hoisted to the top of the function body, the program can be interpreted as this:
```javascript
function foo() {
    var a;
    console.log(a);
    a = 2;
}

foo();
```
Function declarations are hoisted but not function expressions, for example:
```javascript
foo();

var foo = function bar() {
    // ...
};
```
Here the variable identifier `foo` is hoisted and set to undefined, so the `foo();` call doesn't fail as a `ReferenceError`, but since it has no value yet and you attempt to invoke it as a function a `TypeError` is thrown. Also recall that though it's a named function expression the name identifier is not available in enclosing scope.
```javascript
foo(); // TypeError
bar(); // ReferenceError

var foo = function bar() {
    // ...
};
```
With Hoisting the snippet is more accurately interpreted as:
```javascript
var foo;

foo(); // TypeError
bar(); // ReferenceError

foo = function bar() {
    // ...
};
```

## Functions First

Both function and variable declarations are hoisted, but function declarations are hoisted first. For Example:
```javascript
foo(); // 1

var foo;

function foo() {
    console.log(1);
}

foo = function() {
    console.log(2);
};
```
`1` is printed rather than `2`, the engine interprets this as:
```javascript
function foo() {
    console.log(1);
}

foo(); // 1

foo = function() {
    console.log(2);
};
```
`var foo` was a duplicate so it was ignored, even though it came before the function declaration. This is because functions get hoisted first. While multiple duplicate variable declarations get ignored, function declarations will override previous declarations.
```javascript
foo(); // 3


function foo() {
    console.log(1);
}

var foo = function() {
    console.log(2);
};

function foo() {
    console.log(3);
}
```
While this may seem trivial, it highlights the fact that duplicate definitions in the same scope are a bad idea, and function declarations within blocks will hoist to the enclosing scope, not the conditional block. For example:
```javascript
foo(); // "b"

var a = true;

if (a) {
    function foo() {console.log('a');}
} else {
    function foo() {console.log('b');}
}
```
But it's important to note that this behavior isn't reliable and may change in future versions of JavaScript, so avoid declarations within blocks.

## Review

It's tempting to see `var a = 2;` as one statement, but the engine sees it as two, `var a;` and `a = 2;`, the first being a compiler phase statement and the second an execution phase statement. This means that all declarations in a scope are processed first before execution. You can see this as function and variable declarations being "moved" to the top of the scope, this is called "hoisting". Function declarations are hoisted first so they'll win out over variable declarations, also while declarations are hoisted, assignments aren't, not even function expression assignments. Be careful with duplicate declarations, there be dragons.
