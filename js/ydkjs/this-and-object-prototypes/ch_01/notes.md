# You Don't Know JS - This & Object Prototypes Chapter 01: `this` or That? Notes

In JavaScript one of the most confused mechanisms is the `this` keyword. It's a special identifier that's automatically defined in the scope of every function, but what it's referring to confuses even seasoned developers.

The `this` mechanism isn't really *that* advanced, but people often think of it as "complex" or "confusing", and without understanding `this` can appear magical to your confusion.

## Why `this`?

If the `this` mechanism is so confusing, why is it useful? Is it more trouble than it's worth? Let's examine an example to answer the *why* before we answer the *how*:
```javascript
function identify() {
  return this.name.toUpperCase();
}

function speak() {
  var greeting = "Hello, I'm" + identify.call(this);
  console.log(greeting);
}

var me = {
  name: "Kyle"
};

var you = {
  name: "Reader"
};

identify.call(me); // KYLE
identify.call(you); // READER

speak.call(me); // Hello, I'm KYLE
speak.call(you); // Hello, I'm READER
```
If the *how* is confusing don't worry, lets just cover the *why* first.

This snippet lets the `identify()` and `speak()` functions to be reused against multiple *context* objects (`me` and `you`) rather than needing a different function for each object.

Instead of relying on `this` you could have passed a context object explicitly to the functions.

```javascript
function identify(context) {
  return context.name.toUpperCase();
}

function speak(context) {
  var greeting = "Hello, I'm" + identify.call(context);
  console.log(greeting);
}

identify(you); // READER
speak(me); // Hello, I'm KYLE
```

But the `this` mechanism provides a more elegant way of implicitly "passing" an object reference, leading to cleanly designed, easier to reuse APIs.

The more complex your usage pattern is, the more clearly you'll see that passing context around explicitly is messier than passing around a `this` context. When we get to objects and prototypes you'll see the helpfulness of a collection of functions being able to automatically reference the proper context object.

## Confusions

We'll cover how `this` works shortly but let's dispel some misconceptions first about how it *doesn't* work.

The name `this` creates confusion when trying to think about it too literally. There are two often assumed, but incorrect meanings.

### Itself

The first temptation is to assume `this` refers to the function itself. Why would you wish to refer to a function from within itself? The most common reasons would be things like recursion or having an event handler that can unbind itself when it's first called.

New JavaScript developers often think that referencing the function as an object (since all functions are objects in JavaScript) lets you store *state*  (values in properties) between function calls. This is possible and has limited uses, the rest of the book will cover many other patterns for *better* places to store state.

But we'll explore this pattern to illustrate how `this` doesn't let a function get a reference to itself like what is assumed.

Consider this snippet where we attempt to track how many times a function was called:

```javascript
function foo(num) {
  console.log("foo: " + num);

  // keep track of the count of calls to `foo`
  this.count++;
}

foo.count = 0;

var i;

for (i = 0; i < 10; i++) {
  if (i > 5) {
    foo(i);
  }
}

// foo: 6;
// foo: 7;
// foo: 8;
// foo: 9;

// how many times was `foo` called?
console.log(foo.count); // 0 -- lol wut?
```

`foo.count` is still 0, even with four outputs. This stems from a *too literal* interpretation of what `this` means.

When the code executes `foo.count = 0` it adds a property `count` to the function object `foo`. But for the `this.count` reference within the function, `this` is not in fact pointing at all to that function object, so even though the property names are the same, the root objects are different.

**Note:** A responsible developer would ask "If I was incrementing some `count` property, but it wasn't the one I expected, what was I incrementing?" They would find that they had accidentally created a global `count` variable and that its value is `NaN`. Once they've identified this they would have more questions, like "how was it global, and why is it `NaN` instead of a count value?"

Instead of stopping and digging into why `this` doesn't behave as expected and answering those tough but important questions, most developers avoid the issue altogether and hack some solution. Like creating another object to hold the count.

```javascript
function foo(num) {
  console.log("foo: " + num);

  // keep track of the number of calls to `foo`
  data.count++;
}

var data = {
  count: 0
};

var i;

for (i = 0; i < 10; i++) {
  if (i > 5) {
    foo(i);
  }
}

// foo: 6
// foo: 7
// foo: 8
// foo: 9

// how many times was `foo` called?
console.log(data.count); // 4
```

Sure this approach "solves" the problem, unfortunately it simply ignores the real problem, lack of understanding what `this` means and how it works, and instead falls back to the comfort zone of the familiar lexical scope mechanism.

**Note:** Lexical scope is a useful and perfectly fine mechanism; but constantly guessing at how to use `this` and usually being wrong is not a reason to retreat back to lexical scope and never learn why `this` eludes you.

To reference a function from within, `this` by itself will be insufficient. You generally need a reference to the function via a Lexical identifier that points to it.

Consider this example:

```javascript
function foo() {
  foo.count = 4; // `foo` refers to itself
}

setTimeout(function() {
  // anonymous function can't refer to itself.
}, 10);
```

In the first function, `foo` is a reference that can be used to refer to the function from within itself. But in the second the anonymous function has no proper name identifier so there's no proper way to refer to the function object itself.

**Note:** The old, deprecated and frowned upon `arguments.callee` reference inside a function also points to the function object of the currently executing function. This reference is typically the only way to access an anonymous function object from within. The best approach though is to avoid anonymous functions altogether and instead use a named function (expression) since `arguments.callee` is deprecated and shouldn't be used.

Another solution to our example would have been to use the `foo` identifier as the reference and not use `this` at all, which works:

```javascript
function foo(num) {
  console.log("foo: " + num);

  foo.count++;
}

foo.count = 0;

var i;

for (i = 0; i < 10; i++) {
  if (i > 5) {
    foo(i);
  }
}

// foo: 6
// foo: 7
// foo: 8
// foo: 9

console.log(foo.count); // 4
```

But this approach also side-steps understanding `this` and relies on lexical scoping of the variable `foo`.

Another approach is to force `this` to actually point at the `foo` function object.

```javascript
function foo(num) {
  console.log("foo: " + num);

  // keep track of how many times `foo` is called
  // Note: `this` actually IS `foo` now because of how it's called.
  this.count++;
}

foo.count = 0;

var i;

for (i = 0; i < 10; i++) {
  if (i > 5) {
    // using `call(..)`, we ensure `this` points to the function object
    // `foo` itself.
    foo.call(foo, i);
  }

// foo: 6
// foo: 7
// foo: 8
// foo: 9

console.log(foo.count); // 4
```

Here we embrace `this`, we'll explain the details of *how* such techniques work more completely later.

### Its Scope

The next common misconception about the meaning of `this` is that it refers to the function's scope. This is tricky because in one sense there's some truth, but in the other, it's misguided.

To be clear `this` doesn't refer to a function's lexical scope. It's true that internally scope is kind of like an object with properties for each identifier. But the scope "object" isn't accessible to JavaScript code. It's a part of the *engine's* inner implementation. Consider this code that attempts and fails to cross the boundary and use `this` to refer to a function's lexical scope.

```javascript
function foo() {
  var a = 2;
  this.bar();
}

function bar() {
  console.log(this.a);
}

foo(); // ReferenceError: a is not defined.
```

There's more than one mistake in this snippet. First, an attempt is made to reference the `bar()` function with `this.bar()`. It works, but almost certainly by accident. We'll explain the *how* of that shortly. The more natural way to invoke bar would have been to drop the `this.` and let lexical scope take over.

However developers who write code such as this are attempting to use `this` to create a bridge between the scope of `foo()` and `bar()`, so that `bar()` has access to the `a` variable in `foo()`'s scope. This isn't possible. You can't use a `this` reference to look something up in a lexical scope.

Everytime you feel like trying to mix lexical scope look-ups with `this` remind yourself: *there is no bridge*.

## What's `this`?

Now that we've covered incorrect assumptions, let's turn our attention to how the `this` mechanism works.

Earlier we said that `this` is not an author time binding but a runtime binding. It's contextual based on the conditions of the function's invocation. `this` binding has nothing to do with where a function was declared, but everything to do with with the manner in which it is called.

When a function is invoked, an activation record, known as an execution context, is created. This record contains information about where the function was called from (the call-stack), *how* the function was invoked, what parameters, etc. One of the properties of the record is the `this` reference, which will be used for the duration of the function's execution. We'll learn how to find a function's call-site to determine how its execution will bind `this`.

## Review

`this` binding is a source of confusion for JavaScript developers who don't take the time to learn how the mechanism works. To learn `this` you first have to learn what it is *not*, despite any assumptions or misconceptions you ay have. `this` is neither a reference to the function itself, nor is it a reference to the function's *lexical* scope.

`this` is actually a binding that is made when a function is invoked and *what* it refers to is determined by the call-site where the function is called.
