# You Don't Know JS - This & Object Prototypes Chapter 02: `this` All Makes Sense Now! Notes

## Call-Site

To understand the `this` binding we need to understand the *call-site*, the location where a function is called. We need to inspect the call-site to know what `this` references. Finding the call site is usually just finding where a function is called from but it's not always that easy, as certain patterns can obscure the *true* call-site.

It's important to think about the *call-stack* (the function stack that have been called to get us to the current executing code), the call-site we care about is in the invokation *before* the currently executing function. For example:
```javascript
function baz() {
  // call-stack is: `baz`
  // so, our call-site is the global scope

  console.log("baz");
  bar(); // <-- call-site for `bar`
}

function bar() {
  // call-stack is: `baz` -> `bar`
  // so, our call-site is in `baz`
  console.log("bar");
  foo(); // <-- call-site for `foo`
}

function foo() {
  // call-stack is: `baz` -> `bar` -> `foo`
  // so our call-site is in `bar`
  console.log("foo");
}

baz(); // <-- call-site for `baz`
```
Note the actual call-site (from the call-stack) when analyzing code, it's the only thing that matters for `this` binding.

**Note:** A call stack can be visualized by looking at the chain of function calls in order, as was done in the comments. But this is error-prone. Another way to see it is using a debugger tool where you can either set a breakpoint or add a `debugger;` statement in the code. Use these tools and find the second item from the top and that's the real call-site.

## Nothing but Rules

Let's see *how* the call-site determines the `this` binding during execution. You need to inspect the call-site and determine which of four rules applies. We'll explain each of the rules independently and then illustrate their order of precedence, if multiple rules *could* apply to the call-site.

### Default Binding

The first rule comes from the most common case of function calls: standalone function invokation. This of `this` rule as the default catch-all rule when none of the other rules apply. Consider this:
```javascript
function foo() {
  console.log(this.a);
}

var a = 2;

foo(); // 2
```
Thee first thing of note is that globally scoped variables are synonymous with global-object properties of the same name. They aren't copies, they are the same. Second we see that when `foo()` is called `this.a` resolves to our global variable `a` Why? Because in this case the *default binding* for `this` applies to the function call, and points at the global object.

How do we know the default binding rules applied here? We examine the call-site to see how `foo()` was called. In the snippet, `foo()` is called with a plain function reference. None of the other rules we'll learn about apply here. If `strict` mode is in effect the global object isn't eligible for default binding so `this` is set to undefined.
```javascript
function foo() {
  "use strict";
  console.log(this.a);
}

var a = 2;

foo(); // TypeError: `this` is undefined
```
An important detail is that though the overall `this` binding rules are entirely different based on the call-site, the global object is only eligible for the default binding if the contents of `foo()` aren't running in `strict` mode, whether the call-site is in `strict` mode or not.
```javascript
function foo() {
  console.log(this.a);
}

var a = 2;

(function() {
  "use strict";

  foo(); // 2
})();
```
**Note:** Mixing `strict` and non-strict mode together in your code is frowned upon. Your entire program should either be `strict` or not. However, some 3rd party libraries have different strictness so care must be taken for compatibility.

### Implicit Binding

Another rule is based on whether the call-site has a context object also referred to as an owning or containing object, though *these* alternate terms could be misleading.

Consider:
```javascript
function foo() {
  console.log(this.a);
}

var obj = {
  a: 2,
  foo: foo,
};

obj.foo(); // 2
```
Notice the manner in which `foo()` is declared and then later added as a reference property on `obj`. Regardless of whether `foo()` is initially declared on `obj` or added as a reference later, in neither case is the function really "owned" or "contained" by the object.

However the call-site uses the obj context to reference the function, so you could say that the obj object owns the function reference at call-time. Whatever you call this pattern, at the point `foo()` is called, it's preceded by an object reference to `obj`. When there is a context object for a function reference, the *implicit binding* rule says that it's *that* object that should be used for the function call's `this` binding. Because obj is the `this` for the `foo()` call, `this.a` is synonymous with `obj.a`. Only the top/last level of an object proprety reference chain matters to the call-site. For example:
```javascript
function foo() {
  console.log(this.a);
}

var obj2 = {
  a: 42,
  foo: foo,
};

var obj1 = {
  a: 2,
  obj2: obj2
};

obj1.obj2.foo(); // 42
```

#### Implicitly Lost

One common frustration that `this` binding creates is when an *implicitly bound* function loses that binding, which usually means falling back to the *default binding* of either the global object or undefined, depending on `strict` mode.

Example:
```javascript
function foo() {
  console.log(this.a);
}

var obj = {
  a: 2,
  foo: foo
};

var bar = obj.foo; // function reference/alias

var a = "oops, global"; // `a` also a property on global object.

bar(); // "oops, global"
```
Here `bar` appears to be a reference to `obj.foo` but it's really just another reference to `foo` itself. Plus the call-site is what matters and the call-site is `bar()`, which is a plain call and *default binding* applies.

More subtle though, is when we consider passing a callback function:
```javascript
function foo() {
  console.log(this.a);
}

function doFoo(fn) {
  // `fn` is just another reference to `foo`
  fn(); // <-- call-site!
}

var obj = {
  a: 2,
  foo: foo
};

var a = "oops, global"; // `a` also property on global object

doFoo(obj.foo); // "oops, global"
```
Passing parameters is just implicit assignment, and since we're passing a function, it's an implicit reference assignment, making the end result the same as the previous example. What if the function the callback is being passed to is built into the language? Same outcome!
```javascript
function foo() {
  console.log(this.a);
}

var obj = {
  a: 2,
  foo: foo
};

var a = "oops, global"; // `a` also a property on global object

setTimeout(obj.foo, 100); // "oops, global"
```
It's quite common for function callbask to lose their `this` binding. But another way that `this` can surprise you is when the function we've passed our callback intentionally changes the `this` for the call. Popular JavaScript library's event handlers are fond of forcing your callback to have a `this` that points to something like the DOM element that triggered the event. This is sometimes useful but can be infuriating. Either way the `this` is changed unexpectedly, you're not really in control of how your callback function reference will be executed, so you don't have a way (yet) of controlling the call-site to give the right binding.

### Explicit Binding

With implicit binding as we saw we needed to mutate the object to include a reference on itself to the function, and use this property function reference to indirectly bind `this` to the object. But what if you want to force a function call to use a specific object for the `this` binding, without putting property function references on the object?

"All" functions in JS have some utilities available to them (via their [[Prototype]]), which can be useful for this task. Specifically, functions have `call()` and `apply()` methods. Some JavaScript environments sometimes provide functions that are special enough that they don't have this functionality. The vast majority of functions provided or created do have access to `call()` and `apply()` How do the utilities work? They take as their first argument an object to use for the `this` and then invoke the function with that `this` specified. Since you directly state this, it's called *explicit binding.*

Example:
```javascript
function foo() {
  console.log(this.a);
}

var obj = {
  a: 2
};

foo.call(obj); // 2
```
Invoking `foo` with explicit binding using `foo.call()` allows us to force its `this` to be `obj`. If you pass a primitive value (like `string`, `boolean`, or `number`) as the `this` binding the primitive value is wrapped in its object-form (`new String(..)`, etc) this is referred to as "boxing".

**Note:** In respect to `this` binding `call` and `apply` are the same but they do behave differently with their additional parameters.

Explicit binding alone still doesn't offer any solution to the issue of a function "losing" its intended `this` binding, or having it paved over by a framework, etc.

#### Hard Binding

There's a variation of explicit binding that does solve this issue. Consider:
```javascript
function foo() {
  console.log(this.a);
}

var obj = {
  a: 2
};

var bar = function() {
  foo.call(obj);
};

bar(); // 2
setTimeout(bar, 100); // 2

// hard-bound `bar` can no longer have its `this` overridden
bar.call(window); // 2
```


#### API Call "contexts"

Many libraries and built-in functions take an optional context argument that's designed as a work-around for you not having to use `bind(...)` to ensure callback functions use a particular this. Example:
```javascript
function foo(el) {
  console.log(el, this.id);
}

var obj = {
  id: "awesome"
};

// use `obj` as `this` for `foo(...)` calls
[1, 2, 3].forEach(foo, obj);
// 1 awesome 2 awesome 3 awesome
```
Internally these functions almost certainly use *explicit binding* via `call(...)` or `apply(...)`.

### `new` Binding

The last rule of `this` binding makes us rethink a common misconception about functions and objects in JavaScript. In traditional class oriented languages, "constructors" are special methods attached to classes and when the class is instantiated with the `new` keyword the constructor is called, usually like so:
```javascript
something = new myClass(..);
```
JavaScript has the `new` keyword and the code pattern for using it looks similar to above, and developers assume it's doing something similar to class based languages, but there's no connection between its behavior in JavaScript vs other languages. In JavaScript constructors are just functions called with the `new` keyword in front of them. They aren't attached to classes or instantiating a class. They aren't even special functions, they're just regular functions that are hijacked by the `new` keyword when invoked.

Consider the `Number(..)` function as a constructor from the ES5.1 spec.

> 15.7.2 The Number Constructor
> When Number is called as part of a new expression it is a constructor:
> it initializes the newly created object.

Any function including builtin object functions like `Number(..)` can be called with `new` in front of them, making that a *constructor call*, an important but subtle distinction: there's no such thing as a *constructor function*, just construction calls of functions. When a function is invoked with `new` in front of it the following things are done.

1. A brand new object is created.
2. The new object is `[[Prototype]]`-linked.
3. The new object is set as the `this` binding for the function call.
4. Unless the function returns its own alternate object, the `new` invoked function will return the newly constructed object.

Let's cover steps, 1, 3, and 4 for now. Consider this:
```javascript
function foo(a) {
  this.a = a;
}

var bar = new foo(2);
console.log(bar.a); // 2
```
By calling `foo(..)` with `new` in front we've constructed a new object and set it as the `this` binding for the invokation of `foo`, so `new` is the final way that a function's `this` can be bound, call it *new binding*.

## Everything in Order

Now you know the four rules to binding `this` in a function call. Find the callsite, inspect it and see which rules apply. Let's cover the order of precedence of the rules.

The Default binding is the lowest priority of the four.

What's more precedent? Implicit binding or explicit?
```javascript
function foo() {
  console.log(a);
}

var obj1 = {
  a: 2,
  foo: foo,
}

var obj2 = {
  a: 3,
  foo: foo,
}

obj1.foo(); // 2
obj2.foo(); // 3

obj1.foo.call(obj2); // 3
obj2.foo.call(obj1); // 2
```
Explicit binding takes more precedence than implicit. So ask first if explicit binding applies before checking for implicit.

Now where does `new` binding is in precedence.
```javascript
```


### Determining `this`

## Binding Exceptions

### Ignored `this`

#### Safer `this`

### Indirection

### Softening Binding

## Lexical `this`

## Review
