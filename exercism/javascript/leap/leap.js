//
// This is only a SKELETON file for the "Leap" exercise. It's been provided as a
// convenience to get you started writing code faster.
//

class Year {
  constructor(year) {
    this.year = year;
  }
  isLeap() {
    const { year } = this;
    return year % 4 == 0 && (year % 100 != 0 || year % 400 == 0);
  }
}

module.exports = Year;
