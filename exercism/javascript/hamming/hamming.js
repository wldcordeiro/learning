class Hamming {
  compute(strandA, strandB) {
    if (strandA.length != strandB.length) {
      throw Error('DNA strands must be of equal length.');
    }
    return strandA
      .split('')
      .reduce((acc, part, i) => part != strandB[i] ? acc +=1 : acc, 0);
  }
}

module.exports = Hamming;
