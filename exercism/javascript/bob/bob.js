class Bob {
  hey(sentence) {
    if (!/\w+/.test(sentence)) {
      return 'Fine. Be that way!';
    }

    const numbersOnly = /[0-9,\.;'"]+/.test(sentence);
    const allUpper = sentence == sentence.toUpperCase();
    const exclamation = sentence.slice(-1) == '!';
    const question = sentence.slice(-1) == '?';
    if ((!allUpper || numbersOnly) && question) {
      return 'Sure.';
    }

    if ((allUpper && !numbersOnly) || (allUpper && (exclamation || question))) {
      return 'Whoa, chill out!';
    }

    return 'Whatever.';
  }
}

module.exports = Bob;
