class Words {
  count(text) {
    return text
      .split(/\b([\w']+|\w+)\b/)
      .map(part => part.toLowerCase())
      .filter(part => /(\w+)/.test(part))
      .reduce((acc, part) => {
        if (!acc.hasOwnProperty(part)) {
          acc[part] = 0;
        }
        acc[part]++;

        return acc;
      }, {});
  }
};

module.exports = Words;
