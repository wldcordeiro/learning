class DnaTranscriber {
  constructor() {
    this.nucleotideMap = { G: 'C', C: 'G', T: 'A', A: 'U' };
  }

  toRna(nucleotides) {
    if (!nucleotides.split('').every(n => n in this.nucleotideMap)) {
      throw Error('Invalid input');
    }
    return nucleotides
      .split('')
      .map(nucleotide => this.nucleotideMap[nucleotide])
      .join('');
  }
}

module.exports = DnaTranscriber;
