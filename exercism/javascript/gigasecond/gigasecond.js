class Gigasecond {
  constructor(start) {
    this.start = start;
  }

  date() {
    return new Date(this.start.getTime() + (1000000000 * 1000));
  }
}

module.exports = Gigasecond;
