pub fn raindrops(drop: i32) -> String {
    let mut res = String::new();
    if drop % 3 == 0 { res += "Pling" }
    if drop % 5 == 0 { res += "Plang" }
    if drop % 7 == 0 { res += "Plong" }
    if res.is_empty() { return drop.to_string(); }
    res
}
