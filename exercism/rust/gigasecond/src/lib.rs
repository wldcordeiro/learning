extern crate chrono;

use chrono::{UTC, DateTime, Duration};

pub fn after(date: DateTime<UTC>) -> DateTime<UTC> {
    date + Duration::seconds(1_000_000_000)
}
